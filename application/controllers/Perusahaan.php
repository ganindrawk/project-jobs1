<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Perusahaan extends CI_Controller {
		
	public function __construct()
    {
        parent::__construct();
		$this->load->library('encrypt');		
		$this->load->helper('captcha');
    }
	
	/*Halaman utama setelah login perusahaan*/
	function index()
	{
		//die($this->session->username);
		$data['users'] = $this->model_app->view_where('users',array('email'=>$this->session->username))->row_array();
		//$data['modul'] = $this->model_app->view_join_one('users','users_modul','id_session','id_umod','DESC');
		$this->template->load('perusahaan/template','perusahaan/view_home',$data);
    
	}

	/*Halaman profil perusahaan*/
	function profil_perusahaan()
	{		
		$id = $this->session->username;		
		if (isset($_POST['submit']))
		{
			$data_perusahaan = $this->model_app->view_where('profil_perusahaan',array('username'=>$this->session->username))->row_array();
			
			$path						= 'asset/foto_perusahaan/';
			$config['upload_path'] 		= $path;
            $config['allowed_types'] 	= 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] 		= '1000'; // kb
			
            $this->load->library('upload', $config);
			
			if (!is_dir($path))
			{
				mkdir('./'.$path, 0777, true);
			}
			$dir_exist = true; // flag for checking the directory exist or not
			if (!is_dir($path.'/' . $album))
			{
				mkdir('./'.$path.'/' . $album, 0777, true);
				$dir_exist = false; // dir not exist
			}
			else{
		
			}
			
            $this->upload->do_upload('f');
            $hasil=$this->upload->data();
            if ($hasil['file_name'] =='')
			{
				$data = array(  'username' 			=> $this->session->username,
								'nama_perusahaan'	=> $this->db->escape_str($this->input->post('nama_perusahaan')),
								'telp2'				=> $this->db->escape_str($this->input->post('telp2')),
								'alamat'			=> $this->db->escape_str($this->input->post('alamat')),
								'email'				=> $this->db->escape_str($this->input->post('email')),
								'provinsi'			=> $this->db->escape_str($this->input->post('provinsi')),
								'ponsel_wa'			=> $this->db->escape_str($this->input->post('ponsel_wa')),
								'kota'				=> $this->db->escape_str($this->input->post('kota')),
								'nama_direktur'		=> $this->db->escape_str($this->input->post('nama_direktur')),
								'kecamatan'			=> $this->db->escape_str($this->input->post('kecamatan')),
								'nama_manager_hrd'	=> $this->db->escape_str($this->input->post('nama_manager_hrd')),
								'kelurahan'			=> $this->db->escape_str($this->input->post('kelurahan')),
								'jumlah_karyawan'	=> $this->db->escape_str($this->input->post('jumlah_karyawan')),
								'kode_pos'			=> $this->db->escape_str($this->input->post('kode_pos')),
								'status_kantor'		=> $this->db->escape_str($this->input->post('status_kantor')),
								'siup'				=> $this->db->escape_str($this->input->post('siup')),
								'npwp'				=> $this->db->escape_str($this->input->post('npwp')),
								'sektor_usaha'		=> $this->db->escape_str($this->input->post('sektor_usaha')),
								'telp1'				=> $this->db->escape_str($this->input->post('telp1')),
								'extension'			=> $this->db->escape_str($this->input->post('extension'))
							);
            }
			else
			{
                    $data = array(  'username' 			=> $this->session->username,
								'nama_perusahaan'	=> $this->db->escape_str($this->input->post('nama_perusahaan')),
								'telp2'				=> $this->db->escape_str($this->input->post('telp2')),
								'alamat'			=> $this->db->escape_str($this->input->post('alamat')),
								'email'				=> $this->db->escape_str($this->input->post('email')),
								'provinsi'			=> $this->db->escape_str($this->input->post('provinsi')),
								'ponsel_wa'			=> $this->db->escape_str($this->input->post('ponsel_wa')),
								'kota'				=> $this->db->escape_str($this->input->post('kota')),
								'nama_direktur'		=> $this->db->escape_str($this->input->post('nama_direktur')),
								'kecamatan'			=> $this->db->escape_str($this->input->post('kecamatan')),
								'nama_manager_hrd'	=> $this->db->escape_str($this->input->post('nama_manager_hrd')),
								'kelurahan'			=> $this->db->escape_str($this->input->post('kelurahan')),
								'jumlah_karyawan'	=> $this->db->escape_str($this->input->post('jumlah_karyawan')),
								'kode_pos'			=> $this->db->escape_str($this->input->post('kode_pos')),
								'status_kantor'		=> $this->db->escape_str($this->input->post('status_kantor')),
								'siup'				=> $this->db->escape_str($this->input->post('siup')),
								'npwp'				=> $this->db->escape_str($this->input->post('npwp')),
								'sektor_usaha'		=> $this->db->escape_str($this->input->post('sektor_usaha')),
								'telp1'				=> $this->db->escape_str($this->input->post('telp1')),
								'extension'			=> $this->db->escape_str($this->input->post('extension')),
								'foto_kantor'		=> $hasil['file_name']
							);
					
				$data_user 	= array('foto'=>$hasil['file_name']);
				$where_user = array('username' => $this->session->username);
				$this->model_app->update('users', $data_user, $where_user);
            }
			
			if(!empty($data_perusahaan))
			{
				$where = array('username' => $this->session->username );
				$this->model_app->update('profil_perusahaan', $data, $where);
			}
			else{
				 $this->model_app->insert('profil_perusahaan',$data);
			}
			
            $proses = $this->model_app->view_where('profil_perusahaan', array('username' => $this->session->username ))->row_array();
			$akses = $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
			$modul = $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
			$data = array('rows' => $proses, 'record' => $modul, 'akses' => $akses, 'message' => '<div class="alert alert-danger"><center>Data Berhasil di Proses!</center></div>');

			$this->template->load('perusahaan/template','perusahaan/view_form_profil_perusahaan',$data);
		}
		else
		{
			if (isset($this->session->username) OR $this->session->level=='admin')
			{
                $proses = $this->model_app->view_where('profil_perusahaan', array('username' => $this->session->username ))->row_array();
                $akses = $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
                $modul = $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
                $data = array('rows' => $proses, 'record' => $modul, 'akses' => $akses);
    			$this->template->load('perusahaan/template','perusahaan/view_form_profil_perusahaan',$data);
            }
			else
			{
				redirect(base_url().'main');
			}
			
		}
	}

	function list_data_pelamar()
    {
          //die($this->session->username);
          $data['title'] = 'List Data Pelamar';
          $data['users'] = $this->model_app->view_where('users',array('email'=>$this->session->username))->row_array();
          //$data['modul'] = $this->model_app->view_join_one('users','users_modul','id_session','id_umod','DESC');
          $this->template->load('perusahaan/template','perusahaan/view_list_data_pelamar',$data);
    
    }
		
	function list_data_pelamar_json()
    {
		$username_perusahaan=$this->session->username;
        $record = $this->model_app->getListKirimCV('users',$username_perusahaan,'DESC',$_POST);
        $no     = 1;
        $hasil = "";
        foreach ($record as $row)
        {
            $hasil .= " <tr>
                            <td>$no</td>
                            <td>$row[nama_lengkap]</td>
                            <td>$row[jenis_kelamin] </td>
                            <td>$row[age] </td>
                            <td>$row[nilai_sekolah] </td>
                            <td>$kejuruan </td>
                            <td>$row[lama_kerja] </td>
                            <td>$row[status] </td>
                            <td>
                                <center>
                                    <a class='btn btn-success btn-xs' title='Edit Data' href='".base_url().$this->uri->segment(1)."/laporan_pdf/$row[id_informasi_cuti]'><span class='glyphicon glyphicon-print'></span></a>
                                    <!--
                                    <a class='btn btn-success btn-xs' title='Edit Data' href='".base_url().$this->uri->segment(1)."/edit_cuti/$row[id_informasi_cuti]'><span class='glyphicon glyphicon-edit'></span></a>
                                    <a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url().$this->uri->segment(1)."/delete_cuti/$row[id_informasi_cuti]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
                                    -->
                                </center>
                            </td>
                        </tr>";
            $no++;
        }
		echo json_encode(array('hasil'=>$hasil)); 
		exit;
    }

	function list_data_panggilan_tes_kerja()
    {
        $username_perusahaan=$this->session->username;
        $data['record'] = $this->model_app->getListKirimCV('users',$username_perusahaan,'DESC');
        //die(print_r($this->db->last_query()));
        $data['title'] = 'Panggilan Tes Kerja';
        $data['users'] = $this->model_app->view_where('users',array('email'=>$this->session->username))->row_array();
        //$data['modul'] = $this->model_app->view_join_one('users','users_modul','id_session','id_umod','DESC');
        $this->template->load('perusahaan/template','perusahaan/view_list_data_panggilan_tes_kerja',$data);
    
    }

	//Ganti Password
	function ganti_password(){
		
		$id = $this->session->username;
		//print_r($this->session->userdata); 

		//die("sdsd".$id);
		//$id = $this->uri->segment(3);
		
        $this->session->set_userdata('mycaptcha', $cap['word']);

		if (isset($_POST['submit']))
		{
			
			$data = array('password'=>hash("sha512", md5($this->input->post('b'))) );
            $where = array('username' => $this->input->post('a'));
            $this->model_app->update('users', $data, $where);
			echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Password Berhasil Di Ubah!!</center></div>');
			redirect($this->uri->segment(1).'/ganti_password');
		}
		else
		{			
            //if ($this->session->username==$this->uri->segment(3) OR $this->session->level=='admin'){
			if (isset($this->session->username) OR $this->session->level=='admin')
			{
                $proses = $this->model_app->edit('users', array('username' => $id))->row_array();
                $akses = $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
                $modul = $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
					
                // add captcha
                $vals = array(
					'img_path'   	=> './captcha/',
					'img_url'    	=> base_url().'captcha/',
					'font_path' 	=> './asset/Tahoma.ttf',
					'font_size'     => 17,
					'img_width'  	=> '320',
					'img_height' 	=> 33,
					'border' 		=> 0, 
					'word_length'   => 5,
					'expiration' 	=> 7200
				);
						
		        $cap 			= create_captcha($vals);
		       // $data['image'] 	= $cap['image'];
						
				$this->session->set_userdata('mycaptcha', $cap['word']);
                // end captcha
				
                $data = array('rows' => $proses, 'record' => $modul, 'akses' => $akses,'image'=>$cap['image']);
    			$this->template->load('perusahaan/template','perusahaan/view_users_ganti_password',$data);
            }
			else
			{
                redirect($this->uri->segment(1).'/ganti_password');
            }
		}
	}

  // Controller Modul List Info Loker

  function list_berita_loker(){
    //cek_session_akses('listberita',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_ordering('berita','id_berita','DESC');
        }else{
            $data['record'] = $this->model_app->view_where_ordering('berita',array('username'=>$this->session->username),'id_berita','DESC');
        }
        $data['rss'] = $this->model_utama->view_joinn('berita','users','kategori','username','id_kategori','id_berita','DESC',0,10);
        $data['iden'] = $this->model_utama->view_where('identitas',array('id_identitas' => 1))->row_array();
        //$this->load->view('administrator/rss',$data);
    $this->template->load('perusahaan/template','perusahaan/mod_berita_loker/view_berita_loker',$data);
  }


  function tambah_list_loker(){
    //cek_session_akses('listberita',$this->session->id_session);
    if (isset($_POST['submit'])){
      $config['upload_path'] = 'asset/foto_berita/';
          $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
          $config['max_size'] = '3000'; // kb
          $this->load->library('upload', $config);
          $this->upload->do_upload('k');
          $hasil=$this->upload->data();
            
            $config['source_image'] = 'asset/foto_berita/'.$hasil['file_name'];
            $config['wm_text'] = 'phpmu.com';
            $config['wm_type'] = 'text';
            $config['wm_font_path'] = './system/fonts/texb.ttf';
            $config['wm_font_size'] = '26';
            $config['wm_font_color'] = 'ffffff';
            $config['wm_vrt_alignment'] = 'middle';
            $config['wm_hor_alignment'] = 'center';
            $config['wm_padding'] = '20';
            $this->load->library('image_lib',$config);
            $this->image_lib->watermark();

            if ($this->session->level == 'kontributor'){ $status = 'N'; }else{ $status = 'Y'; }
            if ($this->input->post('j')!=''){
                $tag_seo = $this->input->post('j');
                $tag=implode(',',$tag_seo);
            }else{
                $tag = '';
            }
            if ($hasil['file_name']==''){
                    $data = array('id_kategori'=>$this->db->escape_str($this->input->post('a')),
                                    'username'=>$this->session->username,
                                    'judul'=>$this->db->escape_str($this->input->post('b')),
                                    'sub_judul'=>$this->db->escape_str($this->input->post('c')),
                                    'youtube'=>$this->db->escape_str($this->input->post('d')),
                                    'judul_seo'=>seo_title($this->input->post('b')),
                                    'headline'=>$this->db->escape_str($this->input->post('e')),
                                    'aktif'=>$this->db->escape_str($this->input->post('f')),
                                    'utama'=>$this->db->escape_str($this->input->post('g')),
                                    'isi_berita'=>$this->input->post('h'),
                                    'keterangan_gambar'=>$this->input->post('i'),
                                    'hari'=>hari_ini(date('w')),
                                    'tanggal'=>date('Y-m-d'),
                                    'jam'=>date('H:i:s'),
                                    'dibaca'=>'0',
                                    'tag'=>$tag,
                                    'status'=>$status,
																		'status_publish'=>$this->db->escape_str($this->input->post('publish'))
																		);
            }else{
                    $data = array('id_kategori'=>$this->db->escape_str($this->input->post('a')),
                                    'username'=>$this->session->username,
                                    'judul'=>$this->db->escape_str($this->input->post('b')),
                                    'sub_judul'=>$this->db->escape_str($this->input->post('c')),
                                    'youtube'=>$this->db->escape_str($this->input->post('d')),
                                    'judul_seo'=>seo_title($this->input->post('b')),
                                    'headline'=>$this->db->escape_str($this->input->post('e')),
                                    'aktif'=>$this->db->escape_str($this->input->post('f')),
                                    'utama'=>$this->db->escape_str($this->input->post('g')),
                                    'isi_berita'=>$this->input->post('h'),
                                    'keterangan_gambar'=>$this->input->post('i'),
                                    'hari'=>hari_ini(date('w')),
                                    'tanggal'=>date('Y-m-d'),
                                    'jam'=>date('H:i:s'),
                                    'gambar'=>$hasil['file_name'],
                                    'dibaca'=>'0',
                                    'tag'=>$tag,
                                    'status'=>$status,
																		'status_publish'=>$this->db->escape_str($this->input->post('publish'))
																		);
            }
            $this->model_app->insert('berita',$data);
      redirect($this->uri->segment(1).'/list_berita_loker');
    }else{
            $data['tag'] = $this->model_app->view_ordering('tag','id_tag','DESC');
            $data['record'] = $this->model_app->view_ordering('kategori','id_kategori','DESC');
      $this->template->load('perusahaan/template','perusahaan/mod_berita_loker/view_berita_loker_tambah',$data);
    }
  }

  function edit_list_loker(){
    //cek_session_akses('listberita',$this->session->id_session);
    $id = $this->uri->segment(3);
    if (isset($_POST['submit'])){
      $config['upload_path'] = 'asset/foto_berita/';
          $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
          $config['max_size'] = '3000'; // kb
          $this->load->library('upload', $config);
          $this->upload->do_upload('k');
          $hasil=$this->upload->data();

            $config['source_image'] = 'asset/foto_berita/'.$hasil['file_name'];
            $config['wm_text'] = '';
            $config['wm_type'] = 'text';
            $config['wm_font_path'] = './system/fonts/texb.ttf';
            $config['wm_font_size'] = '26';
            $config['wm_font_color'] = 'ffffff';
            $config['wm_vrt_alignment'] = 'middle';
            $config['wm_hor_alignment'] = 'center';
            $config['wm_padding'] = '20';
            $this->load->library('image_lib',$config);
            $this->image_lib->watermark();

            if ($this->session->level == 'kontributor'){ $status = 'N'; }else{ $status = 'Y'; }
            if ($this->input->post('j')!=''){
                $tag_seo = $this->input->post('j');
                $tag=implode(',',$tag_seo);
            }else{
                $tag = '';
            }
            if ($hasil['file_name']==''){
                    $data = array('id_kategori'=>$this->db->escape_str($this->input->post('a')),
                                    'username'=>$this->session->username,
                                    'judul'=>$this->db->escape_str($this->input->post('b')),
                                    'sub_judul'=>$this->db->escape_str($this->input->post('c')),
                                    'youtube'=>$this->db->escape_str($this->input->post('d')),
                                    'judul_seo'=>seo_title($this->input->post('b')),
                                    'headline'=>$this->db->escape_str($this->input->post('e')),
                                    'aktif'=>$this->db->escape_str($this->input->post('f')),
                                    'utama'=>$this->db->escape_str($this->input->post('g')),
                                    'isi_berita'=>$this->input->post('h'),
                                    'keterangan_gambar'=>$this->input->post('i'),
                                    'hari'=>hari_ini(date('w')),
                                    'tanggal'=>date('Y-m-d'),
                                    'jam'=>date('H:i:s'),
                                    'dibaca'=>'0',
                                    'tag'=>$tag,
                                    'status'=>$status,
																		'status_publish'=>$this->db->escape_str($this->input->post('publish'))
																		);
            }else{
                    $data = array('id_kategori'=>$this->db->escape_str($this->input->post('a')),
                                    'username'=>$this->session->username,
                                    'judul'=>$this->db->escape_str($this->input->post('b')),
                                    'sub_judul'=>$this->db->escape_str($this->input->post('c')),
                                    'youtube'=>$this->db->escape_str($this->input->post('d')),
                                    'judul_seo'=>seo_title($this->input->post('b')),
                                    'headline'=>$this->db->escape_str($this->input->post('e')),
                                    'aktif'=>$this->db->escape_str($this->input->post('f')),
                                    'utama'=>$this->db->escape_str($this->input->post('g')),
                                    'isi_berita'=>$this->input->post('h'),
                                    'keterangan_gambar'=>$this->input->post('i'),
                                    'hari'=>hari_ini(date('w')),
                                    'tanggal'=>date('Y-m-d'),
                                    'jam'=>date('H:i:s'),
                                    'gambar'=>$hasil['file_name'],
                                    'dibaca'=>'0',
                                    'tag'=>$tag,
                                    'status'=>$status,
																		'status_publish'=>$this->db->escape_str($this->input->post('publish'))
																		);
            }
            $where = array('id_berita' => $this->input->post('id'));
      $this->model_app->update('berita', $data, $where);
      redirect($this->uri->segment(1).'/list_berita_loker');
    }else{
      $tag = $this->model_app->view_ordering('tag','id_tag','DESC');
            $record = $this->model_app->view_ordering('kategori','id_kategori','DESC');
            if ($this->session->level=='admin'){
                 $proses = $this->model_app->edit('berita', array('id_berita' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('berita', array('id_berita' => $id, 'username' => $this->session->username))->row_array();
            }
      $data = array('rows' => $proses,'tag' => $tag,'record' => $record);
      $this->template->load('perusahaan/template','perusahaan/mod_berita_loker/view_berita_loker_edit',$data);
    }
  }

  function publish_list_loker(){
    //cek_session_admin();
    if ($this->uri->segment(4)=='Y'){
      $data = array('status'=>'N');
    }else{
      $data = array('status'=>'Y');
    }
        $where = array('id_berita' => $this->uri->segment(3));
    $this->model_app->update('berita', $data, $where);
    redirect($this->uri->segment(1).'/list_berita_loker');
  }

  function delete_list_loker(){
        //cek_session_akses('listberita',$this->session->id_session);
        if ($this->session->level=='admin'){
        $id = array('id_berita' => $this->uri->segment(3));
        }else{
            $id = array('id_berita' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
    $this->model_app->delete('berita',$id);
    redirect($this->uri->segment(1).'/list_berita_loker');
  }
  
    function form_panggil_test()
	{		
		$id = $this->session->username;		
		
		if (isset($this->session->username) OR $this->session->level=='admin')
		{
            $proses         = $this->model_app->view_where('profil_perusahaan', array('username' => $this->session->username ))->row_array();
            $akses          = $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
            $modul          = $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
            $profil_pelamar = $this->model_app->view_join_where('profil_pelamar','kirim_cv','username', array('md5(id)' => $this->uri->segment(3)),'id','DESC');
            $berita         = $this->model_app->view_join_where('berita','kirim_cv','id_berita', array('md5(id)' => $this->uri->segment(3),'berita.username' => $this->session->username),'id','DESC');
            //die(print_r($this->db->last_query()));
            $data           = array('rows' => $proses, 'record' => $modul, 'akses' => $akses, 'profil_pelamar' => $profil_pelamar[0], 'berita' => $berita[0]);
			$this->template->load('perusahaan/template','perusahaan/view_form_panggil_test',$data);
        }
		else
		{
			redirect(base_url().'main');
		}
			
		
	}

	function kirim_panggil_test()
    {
        //die(print_r($this->input->post()) );
        $id_kc              = $this->input->post('id_kc');
        $id_berita          = $this->input->post('id_berita');
        $email_pelamar      = $this->input->post('email_pelamar');
        $email_perusahaan   = $this->session->username;
        $nama_perusahaan    = $this->input->post('nama_perusahaan');
        $alamat             = $this->input->post('alamat');
        $nama_pelamar       = $this->input->post('nama_pelamar');
        $posisi_jabatan     = $this->input->post('posisi_jabatan');
        $tanggal_tes        = $this->input->post('tanggal_tes');
        $jam_mulai          = $this->input->post('jam_mulai');
        $jam_selesai        = $this->input->post('jam_selesai');
        $telp               = $this->input->post('telp');
        
        $data = array(  'id_berita' 	    => $id_berita,
                        'id_kc'             => $id_kc,
                        'nama_perusahaan'	=> $nama_perusahaan,
                        'email_perusahaan'	=> $email_perusahaan,
                        'email_pelamar'     => $email_pelamar,
                        'tanggal_tes'	    => $tanggal_tes,
                        'jam_mulai'		    => $jam_mulai,
                        'jam_selesai'		=> $jam_selesai,
                        'update_date'	    => date('Y/m/d H:i:s'),
                        'telp_hrd'          => $telp
					);
        
        
        $to 		= $this->input->post('email_pelamar');    
        $subject 	= "Panggilan Interview / Test";
        
        $message 	= " Hai ".ucfirst($nama_pelamar).",
                        <br> Anda mendapatkan Tes Panggilan Kerja di :<br><br>
                        <table>
                            <tr><td>Nama perusahaan</td><td>:</td><td>".$nama_perusahaan."</td></tr>
                            <tr><td>Alamat</td><td>:</td><td>".$alamat."</td></tr>
                            <tr><td>Posisi Jabatan</td><td>:</td><td>".$posisi_jabatan."</td></tr>
                            <tr><td>Tanggal tes</td><td>:</td><td>".$tanggal_tes."</td></tr>
                            <tr><td>Jam tes</td><td>:</td><td>".$jam_mulai." s/d ".$jam_selesai."</td></tr>
                            <tr><td>Nomor yang bisa di hubungi</td><td>:</td><td>".$telp."</td></tr>
                        </table>
                        <br><br>
                        Mohon untuk konfirmasi di nomor yang bisa di hubungi di atas.
                        Terima kasih telah bergabung dan membangun karier bersama www.bkk.smksejahtera.sch.id";   
        
        // Always set content-type when sending HTML email
        $headers  = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From: jobs@gmail.com";
        //die($message);  
        mail($to,$subject,$message, $headers);
        
        $data_kc    = array('status_kirim' => 1);
        $where_kc   = array('id' => $id_kc);
        $this->model_app->update('kirim_cv', $data_kc, $where_kc);
        
        $this->model_app->insert('detail_kirim_cv',$data);
        redirect($this->uri->segment(1).'/list_data_panggilan_tes_kerja');
    }
    
	function form_profil_pelamar()
	{		
		$username = $this->uri->segment(3);	
		$data['rata2'] = $this->uri->segment(4);
		//print_r($rata2);die;
		if (isset($this->session->username) OR $this->session->level=='admin')
		{
            $data['rows'] = $this->model_app->getProfilPelamar('users',$username,'DESC');
            $data['nilai'] = $this->model_app->getNilaiSekolahPelamar('nilai_sekolah_pelamar',$username,'DESC');
            $data['pendidikan'] = $this->model_app->getPendidikanPelamar('pendidikan',$username,'DESC');
            $data['skill'] = $this->model_app->getSkillPelamar('keterampilan_pelamar',$username,'DESC');
            $data['pengalaman'] = $this->model_app->getPengalamanPelamar('pengalaman_kerja_pelamar',$username,'DESC');
            $data['lampiran'] = $this->model_app->getLampiranPelamar('lampiran_pelamar',$username,'DESC');
            //echo '<pre>';print_r($data['rows']);
            //die('sasa');
           
			$this->template->load('perusahaan/template','perusahaan/view_form_profil_pelamar',$data);
        }
		else
		{
			redirect(base_url().'main');
		}
			
		
	}

	/*Halaman keluar*/
	function logout(){
		$this->session->sess_destroy();
		redirect('main');
	}
}

