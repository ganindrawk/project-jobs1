<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pelamar extends CI_Controller {
		
	public function __construct()
    {
        parent::__construct();
		$this->load->library('encrypt');		
		$this->load->helper('captcha');
    }
	
	/*Halaman utama setelah login pelamar*/
	function index()
	{
          $data['users'] 				= $this->model_app->view_where('users',array('email'=>$this->session->username))->row_array();
          //$data['modul'] = $this->model_app->view_join_one('users','users_modul','id_session','id_umod','DESC');
		  
		  //profil pelamar
		  $data['users_pelamar'] 		= $this->model_app->view_where('profil_pelamar',array('email'=>$this->session->username))->row_array();
          
		  //riwayat pendidikan
		  $data['data_pendidikan'] 		= $this->model_app->view_where('pendidikan',array('username'=>$this->session->username));
          
		  //keterampilan pelamar
		  $data['data_keterampilan'] 	= $this->model_app->view_where('keterampilan_pelamar',array('username'=>$this->session->username));
          
		  //pengalaman pelamar
		  $data['data_pengalaman'] 		= $this->model_app->view_where('pengalaman_kerja_pelamar',array('username'=>$this->session->username));
          
		   //nilai sekolah pelamar
		  $data['data_nilai'] 			= $this->model_app->view_where_ordering('matapelajaran','id not in (select id_mata_pelajaran from nilai_sekolah_pelamar where username = "'.$this->session->username.'")','id','ASC');
				
		   //lampiran pelamar
		  $data['data_lampiran'] 		= $this->model_app->view_where_ordering('master_lampiran','id not in (select id_master_dokumen from lampiran_pelamar where username = "'.$this->session->username.'")','id','ASC');
				
		  $this->template->load('pelamar/template','pelamar/view_home',$data);
    
	}
	
	function list_panggilan_tes()
	{
		  $username = $this->session->username;
		  $data['panggilan'] = $this->model_app->listCvTerkirim($username,'DESC');
		  $data['title'] = 'Panggilan Tes Kerja';
          $data['users'] = $this->model_app->view_where('users',array('email'=>$this->session->username))->row_array();
          //$data['modul'] = $this->model_app->view_join_one('users','users_modul','id_session','id_umod','DESC');
          $this->template->load('pelamar/template','pelamar/view_list_panggilan_tes',$data);
    
	}
	
	/*Halaman profil pelamar*/
	function profil_pelamar()
	{		
		$id = $this->session->username;		
		if (isset($_POST['submit']))
		{
			$post 						= $this->input->post();
			$data_users 				= $this->model_app->view_where('profil_pelamar',array('email'=>$this->input->post('email')))->row_array();						
			
			/*konfigirasi upload foto*/											
			$config['upload_path'] 		= 'asset/foto_user/';
            $config['allowed_types'] 	= 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] 		= '1000'; // kb
			$config['encrypt_name'] 	= TRUE;
			$config['overwrite'] 		= TRUE;
			
			$this->load->library('upload', $config);
            $this->upload->do_upload('f');
            $hasil						= $this->upload->data();
			
			if($hasil['file_name']== '')
			{
				$data = array('nama_lengkap' 		=> $this->db->escape_str($this->input->post('nama_lengkap')),
						  'nik'					=> $this->input->post('nik'),
						  'tempat_lahir'		=> $this->db->escape_str($this->input->post('tlahir')),
						  'tanggal_lahir'		=> $this->input->post('tgllahir'),
						  'jenis_kelamin'		=> $this->db->escape_str($this->input->post('jenis_kelamin')),
						  'agama'				=> $this->db->escape_str($this->input->post('agama')),
						  'alamat'				=> $this->db->escape_str($this->input->post('alamat')),
						  'email'				=> $this->db->escape_str($this->input->post('email')),
						  'username'			=> $this->db->escape_str($this->input->post('email')),
						  'hp'					=> $this->db->escape_str($this->input->post('hp')),
						  'pendidikan_akhir'	=> $this->db->escape_str($this->input->post('pendidikan_akhir')),
						  'kompetensi'			=> $this->db->escape_str($this->input->post('kompetensi')),
						  'tahun_lulus'			=> $this->input->post('tahun_lulus'),
						  'tinggi_badan'		=> $this->input->post('tinggi_badan'),
						  'berat_badan'			=> $this->input->post('berat_badan'),
						  'buta_warna'			=> $this->db->escape_str($this->input->post('buta_warna')),
						  'berkacamata'			=> $this->db->escape_str($this->input->post('berkacamata')),
						  'status_kerja'		=> $this->db->escape_str($this->input->post('status_kerja')),
						  'update_date'			=> date('Y/m/d H:i:s')
						  );
			}
			else
			{
				$data = array('nama_lengkap' 	=> $this->db->escape_str($this->input->post('nama_lengkap')),
						  'nik'					=> $this->input->post('nik'),
						  'tempat_lahir'		=> $this->db->escape_str($this->input->post('tlahir')),
						  'tanggal_lahir'		=> $this->input->post('tgllahir'),
						  'jenis_kelamin'		=> $this->db->escape_str($this->input->post('jenis_kelamin')),
						  'agama'				=> $this->db->escape_str($this->input->post('agama')),
						  'alamat'				=> $this->db->escape_str($this->input->post('alamat')),
						  'email'				=> $this->db->escape_str($this->input->post('email')),
						  'username'			=> $this->db->escape_str($this->input->post('email')),
						  'hp'					=> $this->db->escape_str($this->input->post('hp')),
						  'pendidikan_akhir'	=> $this->db->escape_str($this->input->post('pendidikan_akhir')),
						  'kompetensi'			=> $this->db->escape_str($this->input->post('kompetensi')),
						  'tahun_lulus'			=> $this->input->post('tahun_lulus'),
						  'tinggi_badan'		=> $this->input->post('tinggi_badan'),
						  'berat_badan'			=> $this->input->post('berat_badan'),
						  'buta_warna'			=> $this->db->escape_str($this->input->post('buta_warna')),
						  'berkacamata'			=> $this->db->escape_str($this->input->post('berkacamata')),
						  'status_kerja'		=> $this->db->escape_str($this->input->post('status_kerja')),
						  'update_date'			=> date('Y/m/d H:i:s'),
						  'foto'				=> $hasil['file_name']
						  );
				
				$data_user = array('foto'=>$hasil['file_name']);
				$where_user = array('username' => $this->input->post('email'));
				$this->model_app->update('users', $data_user, $where_user);
			}
						
			if(!empty($data_users))
			{
				$where = array('email' => $this->db->escape_str($this->input->post('email')) );
				$this->model_app->update('profil_pelamar', $data, $where);
			}
			else{
				 $this->model_app->insert('profil_pelamar',$data);
			}
			
			$proses 	= $this->model_app->edit('users', array('username' => $id))->row_array();
			$akses 		= $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
			$modul 		= $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
			
			$data 		= array('rows' => $proses, 'record' => $modul, 'akses' => $akses, 'data_users'=>$data_users,'message'=>'<div class="alert alert-danger"><center>Data Berhasil di Proses!</center></div>');           
			$this->template->load('pelamar/template','pelamar/view_form_profil_pelamar',$data);
				
			//redirect($this->uri->segment(1).'/profil_pelamar/');
			
		}
		else
		{
            //if ($this->session->username==$this->uri->segment(3) OR $this->session->level=='admin')
			if (isset($this->session->username) OR $this->session->level=='admin')
			{
                $proses 	= $this->model_app->edit('users', array('username' => $id))->row_array();
                $akses 		= $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
                $modul 		= $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
                $data_users = $this->model_app->view_where('profil_pelamar',array('email'=>$this->session->username))->row_array();
				$data 		= array('rows' => $proses, 'record' => $modul, 'akses' => $akses, 'data_users'=>$data_users);
				//print_r($data);
				$this->template->load('pelamar/template','pelamar/view_form_profil_pelamar',$data);
            }
			else
			{
                redirect($this->uri->segment(1).'pelamar/edit_manajemenuser');
            }
		}
	}
	
	/*Halaman riwayat pendidikan*/
	function riwayat_pendidikan()
	{		
		$id = $this->session->username;		
		if (isset($_POST['submit']) and isset($this->session->username))
		{
			$data_riwayat = $this->model_app->view_where('pendidikan',array('id'=>$this->input->post('id_riwayat_pendidikan')))->row_array();						
			$data = array(
						  'username'			=> $this->session->username,
						  'nama_sekolah'		=> $this->db->escape_str($this->input->post('nama_sekolah')),
						  'tahun_lulus'			=> $this->db->escape_str($this->input->post('tahun_lulus')),
						  'keterangan'			=> $this->db->escape_str($this->input->post('informasi_lainnya')),
						  'update_date'			=> date('Y-m-d H:i:s')
						  );						
			if(!empty($data_riwayat))
			{
				$where = array('id' => $this->db->escape_str($this->input->post('id_riwayat_pendidikan')) );
				$this->model_app->update('pendidikan', $data, $where);
			}
			else{
				 $this->model_app->insert('pendidikan',$data);
			}
			
			$proses 		= $this->model_app->edit('users', array('username' => $id))->row_array();
			$akses 			= $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
			$modul 			= $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
			$data_pelamar 	= $this->model_app->view_where('pendidikan',array('username'=>$this->session->username));
			
			$data 			= array('rows' => $proses, 'record' => $modul, 'akses' => $akses, 'data_pelamar'=>$data_pelamar,'message'=>'<div class="alert alert-danger"><center>Data Berhasil di Proses!</center></div>');           
			$this->template->load('pelamar/template','pelamar/view_riwayat_pendidikan',$data);
		}
		else
		{
			if (isset($this->session->username) OR $this->session->level=='admin')
			{
                $proses 		= $this->model_app->edit('users', array('username' => $id))->row_array();
                $akses 			= $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
                $modul 			= $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
                $data_pelamar 	= $this->model_app->view_where('pendidikan',array('username'=>$this->session->username));		
				$data 			= array('rows' => $proses, 'record' => $modul, 'akses' => $akses, 'data_pelamar'=>$data_pelamar);
				$this->template->load('pelamar/template','pelamar/view_riwayat_pendidikan',$data);
            }
			else
			{
                redirect($this->uri->segment(1).'pelamar/edit_manajemenuser');
            }
		}
	}
	
	
	function keterampilan_skill()
	{		
		$id = $this->session->username;		
		if (isset($_POST['submit']) and isset($this->session->username))
		{
			$data_riwayat = $this->model_app->view_where('keterampilan_pelamar',array('id'=>$this->input->post('id_keterampilan')))->row_array();						
			$data = array(
						  'username'			=> $this->session->username,
						  'nama_sertifikat'		=> $this->db->escape_str($this->input->post('nama_sertifikat')),
						  'nomor_sertifikat'			=> $this->db->escape_str($this->input->post('nomor_sertifikat')),
						  'tahun_sertifikat'			=> $this->db->escape_str($this->input->post('tahun_sertifikat')),
						  'update_date'			=> date('Y-m-d H:i:s')
						  );						
			if(!empty($data_riwayat))
			{
				$where = array('id' => $this->db->escape_str($this->input->post('id_keterampilan')) );
				$this->model_app->update('keterampilan_pelamar', $data, $where);
			}
			else{
				 $this->model_app->insert('keterampilan_pelamar',$data);
			}
			
			$proses 		= $this->model_app->edit('users', array('username' => $id))->row_array();
			$akses 				= $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
			$modul 				= $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
			$data_keterampilan 	= $this->model_app->view_where('keterampilan_pelamar',array('username'=>$this->session->username));
			
			$data 				= array('rows' => $proses, 'record' => $modul, 'akses' => $akses, 'data_keterampilan'=>$data_keterampilan,'message'=>'<div class="alert alert-danger"><center>Data Berhasil di Proses!</center></div>');           
			$this->template->load('pelamar/template','pelamar/view_keterampilan_skill',$data);
		}
		else
		{
			if (isset($this->session->username) OR $this->session->level=='admin')
			{
                $proses 			= $this->model_app->edit('users', array('username' => $id))->row_array();
                $akses 				= $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
                $modul 				= $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
                $data_keterampilan 	= $this->model_app->view_where('keterampilan_pelamar',array('username'=>$this->session->username));		
				$data 				= array('rows' => $proses, 'record' => $modul, 'akses' => $akses, 'data_keterampilan'=>$data_keterampilan);
				$this->template->load('pelamar/template','pelamar/view_keterampilan_skill',$data);
            }
		}
	}
	
	function pengalaman_kerja()
	{		
		$id = $this->session->username;		
		if (isset($_POST['submit']) and isset($this->session->username))
		{
			$data_riwayat = $this->model_app->view_where('pengalaman_kerja_pelamar',array('id'=>$this->input->post('id_pengalaman')))->row_array();						
			$data = array(
						  'username'			=> $this->session->username,
						  'nama_perusahaan'		=> $this->db->escape_str($this->input->post('nama_perusahaan')),
						  'bagian_kerja'		=> $this->db->escape_str($this->input->post('bagian_kerja')),
						  'lama_kerja'			=> $this->db->escape_str($this->input->post('lama_kerja')),
						  'parklaring_kerja'	=> $this->db->escape_str($this->input->post('parklaring_kerja')),
						  'deskripsi'			=> $this->db->escape_str($this->input->post('informasi_lainnya')),
						  'update_date'			=> date('Y-m-d H:i:s')
						  );						
			if(!empty($data_riwayat))
			{
				$where = array('id' => $this->db->escape_str($this->input->post('id_pengalaman')) );
				$this->model_app->update('pengalaman_kerja_pelamar', $data, $where);
			}
			else
			{
				 $this->model_app->insert('pengalaman_kerja_pelamar',$data);
			}
			
			$proses 			= $this->model_app->edit('users', array('username' => $id))->row_array();
			$akses 				= $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
			$modul 				= $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
			$data_pengalaman 	= $this->model_app->view_where('pengalaman_kerja_pelamar',array('username'=>$this->session->username));
			
			$data 				= array('rows' => $proses, 'record' => $modul, 'akses' => $akses, 'data_pengalaman'=>$data_pengalaman,'message'=>'<div class="alert alert-danger"><center>Data Berhasil di Proses!</center></div>');           
			$this->template->load('pelamar/template','pelamar/view_pengalaman_kerja',$data);
		}
		else
		{
			if (isset($this->session->username) OR $this->session->level=='admin')
			{
                $proses 			= $this->model_app->edit('users', array('username' => $id))->row_array();
                $akses 				= $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
                $modul 				= $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
                $data_pengalaman 	= $this->model_app->view_where('pengalaman_kerja_pelamar',array('username'=>$this->session->username));		
				$data 				= array('rows' => $proses, 'record' => $modul, 'akses' => $akses, 'data_pengalaman'=>$data_pengalaman);
				$this->template->load('pelamar/template','pelamar/view_pengalaman_kerja',$data);
            }
		}
	}
	
	function nilai_sekolah()
	{		
		$id = $this->session->username;		
		if (isset($_POST['submit']) and isset($this->session->username))
		{
			$data_riwayat = $this->model_app->view_where('nilai_sekolah_pelamar',array('username'=>$this->session->username,'id_mata_pelajaran'=>$this->input->post('mata_pelajaran')))->row_array();
			//die(print_r($this->db->last_query()));
			$data = array(
						  'username'			=> $this->session->username,
						  'id_mata_pelajaran'	=> $this->db->escape_str($this->input->post('mata_pelajaran')),
						  'nilai'				=> $this->db->escape_str($this->input->post('nilai')),
						  'update_date'			=> date('Y-m-d H:i:s')
						  );						
			if(!empty($data_riwayat))
			{
				$where = array('id' => $this->db->escape_str($this->input->post('id_nilai')) );
				$this->model_app->update('nilai_sekolah_pelamar', $data, $where);
			}
			else{
				 $this->model_app->insert('nilai_sekolah_pelamar',$data);
			}
			
			$proses 		= $this->model_app->edit('users', array('username' => $id))->row_array();
			$akses 			= $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
			$modul 			= $this->model_app->view_where_ordering('modul', array('publish' => 'Y', 'status' => 'user'), 'id_modul', 'DESC');
			$record 		= $this->model_app->view_join_where2('nilai_sekolah_pelamar.id,nilai_sekolah_pelamar.id_mata_pelajaran,nilai_sekolah_pelamar.nilai,matapelajaran.nama_mata_pelajaran','nilai_sekolah_pelamar','id_mata_pelajaran','matapelajaran','id',array('username'=>$this->session->username),'id_mata_pelajaran','asc');
				
			
			$data 			= array('rows' => $proses, 'record' => $modul, 'akses' => $akses, 'record'=>$record,'message'=>'<div class="alert alert-danger"><center>Data Berhasil di Proses!</center></div>');           
			$this->template->load('pelamar/template','pelamar/view_nilai_sekolah',$data);
		}
		else
		{
			if (isset($this->session->username) OR $this->session->level=='admin')
			{
				$data['record'] 		= $this->model_app->view_join_where2('nilai_sekolah_pelamar.id,nilai_sekolah_pelamar.id_mata_pelajaran,nilai_sekolah_pelamar.nilai,matapelajaran.nama_mata_pelajaran','nilai_sekolah_pelamar','id_mata_pelajaran','matapelajaran','id',array('username'=>$this->session->username),'id_mata_pelajaran','asc');
				$data['matapelajaran'] 	= $this->model_app->view_where_ordering('matapelajaran','id not in (select id_mata_pelajaran from nilai_sekolah_pelamar where username = "'.$this->session->username.'")','id','ASC');
				//die(print_r($this->db->last_query()));
				$this->template->load('pelamar/template','pelamar/view_nilai_sekolah',$data);
            }
		}
	}
	
	function lampiran_pelamar()
	{		
		$id = $this->session->username;		
		if (isset($_POST['submit']) and isset($this->session->username))
		{
			$data_riwayat = $this->model_app->view_where('lampiran_pelamar',array('username'=>$this->session->username, 'id_master_dokumen'=>$this->input->post('dokumen')))->row_array();
			//die(print_r($this->db->last_query()));
			/*konfigirasi upload foto*/
			$path						= 'asset/lampiran_pelamar/'.$this->session->username;
			$config['upload_path'] 		= $path;
            $config['allowed_types'] 	= 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] 		= '1000'; // kb
			$config['encrypt_name'] 	= TRUE;
			$config['overwrite'] 		= TRUE;
			
			$this->load->library('upload', $config);
			
			if (!is_dir($path))
			{
				mkdir('./'.$path, 0777, true);
			}
			$dir_exist = true; // flag for checking the directory exist or not
			if (!is_dir($path.'/' . $album))
			{
				mkdir('./'.$path.'/' . $album, 0777, true);
				$dir_exist = false; // dir not exist
			}
			else{
		
			}
	
            $this->upload->do_upload('f');
            $hasil						= $this->upload->data();
			
			if($hasil['file_name']== '')
			{
				$data = array(
						  'username'			=> $this->session->username,
						  'id_master_dokumen'	=> $this->db->escape_str($this->input->post('dokumen')),
						  'status'				=> $this->db->escape_str($this->input->post('status')),
						  'update_date'			=> date('Y-m-d H:i:s')
						  );	
			}
			else
			{
				$data = array(
						  'username'			=> $this->session->username,
						  'id_master_dokumen'	=> $this->db->escape_str($this->input->post('dokumen')),
						  'status'				=> $this->db->escape_str($this->input->post('status')),
						  'file_lampiran'		=> $hasil['file_name'],
						  'update_date'			=> date('Y-m-d H:i:s')
						  );	
			}
								
			if(!empty($data_riwayat))
			{
				$where = array('id' => $this->db->escape_str($this->input->post('id_lampiran_pelamar')) );
				$this->model_app->update('lampiran_pelamar', $data, $where);
			}
			else{
				 $this->model_app->insert('lampiran_pelamar',$data);
			}
			
			$data['record'] 	= $this->model_app->view_join_where2('lampiran_pelamar.id,lampiran_pelamar.id_master_dokumen,
																	  lampiran_pelamar.status,lampiran_pelamar.file_lampiran,
																	  master_lampiran.dokumen','lampiran_pelamar',
																	  'id_master_dokumen','master_lampiran','id',array('username'=>$this->session->username),'id_master_dokumen','asc');
			$data['dokumen'] 	= $this->model_app->view_where_ordering('master_lampiran','id not in (select id_master_dokumen from lampiran_pelamar where username = "'.$this->session->username.'")','id','ASC');
			$data['message']	= '<div class="alert alert-danger"><center>Data Berhasil di Proses!</center></div>';
			$this->template->load('pelamar/template','pelamar/view_upload_lampiran_pelamar',$data);
		}
		else
		{
			
			if (isset($this->session->username) OR $this->session->level=='admin')
			{
				$data['record'] 	= $this->model_app->view_join_where2('lampiran_pelamar.id,lampiran_pelamar.id_master_dokumen,lampiran_pelamar.status,lampiran_pelamar.file_lampiran,master_lampiran.dokumen','lampiran_pelamar','id_master_dokumen','master_lampiran','id',array('username'=>$this->session->username),'id_master_dokumen','asc');
				$data['dokumen'] 	= $this->model_app->view_where_ordering('master_lampiran','id not in (select id_master_dokumen from lampiran_pelamar where username = "'.$this->session->username.'")','id','ASC');
				//die("dsdad".print_r($this->db->last_query()));
				$this->template->load('pelamar/template','pelamar/view_upload_lampiran_pelamar',$data);
            }
		}
	}
	
	function hapus_pendidikan()
	{
		$id = $_POST['id_hapus_pendidikan'];
		$where = array('id' => $id );
		$this->model_app->delete('pendidikan', $where);
		//die(print_r($this->db->last_query()));
		echo json_encode(array('message'=>'Data berhasil di hapus')); 
		exit;
	}
	
	function hapus_keterampilan()
	{
		$id = $_POST['id_hapus_keterampilan'];
		$where = array('id' => $id );
		$this->model_app->delete('keterampilan_pelamar', $where);
		//die(print_r($this->db->last_query()));
		echo json_encode(array('message'=>'Data berhasil di hapus')); 
		exit;
	}
	
	function hapus_pengalaman()
	{
		$id = $_POST['id_pengalaman'];
		$where = array('id' => $id );
		$this->model_app->delete('pengalaman_kerja_pelamar', $where);
		//die(print_r($this->db->last_query()));
		echo json_encode(array('message'=>'Data berhasil di hapus')); 
		exit;
	}
	
	/*Halaman edit manajemen user*/
	function edit_manajemenuser()
	{		
		//$id = $this->uri->segment(3);
		$id = $this->session->username;
		
		if (isset($_POST['submit']))
		{
			$config['upload_path'] = 'asset/foto_user/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '1000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('f');
            $hasil=$this->upload->data();
            if ($hasil['file_name']=='' AND $this->input->post('b') ==''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'blokir'=>$this->db->escape_str($this->input->post('h')));
            }elseif ($hasil['file_name']!='' AND $this->input->post('b') ==''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'foto'=>$hasil['file_name'],
                                    'blokir'=>$this->db->escape_str($this->input->post('h')));
            }elseif ($hasil['file_name']=='' AND $this->input->post('b') !=''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'password'=>hash("sha512", md5($this->input->post('b'))),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'blokir'=>$this->db->escape_str($this->input->post('h')));
            }elseif ($hasil['file_name']!='' AND $this->input->post('b') !=''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'password'=>hash("sha512", md5($this->input->post('b'))),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'foto'=>$hasil['file_name'],
                                    'blokir'=>$this->db->escape_str($this->input->post('h')));
            }
            $where = array('username' => $this->input->post('id'));
            $this->model_app->update('users', $data, $where);

              $mod=count($this->input->post('modul'));
              $modul=$this->input->post('modul');
              for($i=0;$i<$mod;$i++){
                $datam = array('id_session'=>$this->input->post('ids'),
                              'id_modul'=>$modul[$i]);
                $this->model_app->insert('users_modul',$datam);
              }

			redirect($this->uri->segment(1).'/edit_manajemenuser/'.$this->input->post('id'));
		}
		else
		{
            //if ($this->session->username==$this->uri->segment(3) OR $this->session->level=='admin')
			if (isset($this->session->username) OR $this->session->level=='admin')
			{
                $proses = $this->model_app->edit('users', array('username' => $id))->row_array();
                $akses = $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
                $modul = $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
                $data = array('rows' => $proses, 'record' => $modul, 'akses' => $akses);
    			$this->template->load('pelamar/template','pelamar/view_profile',$data);
            }
			else
			{
                redirect($this->uri->segment(1).'pelamar/edit_manajemenuser');
            }
		}
	}

	//Ganti Password
	function ganti_password(){
		
		$id = $this->session->username;
		//print_r($this->session->userdata); 

		//die("sdsd".$id);
		//$id = $this->uri->segment(3);
		
        $this->session->set_userdata('mycaptcha', $cap['word']);

		if (isset($_POST['submit']))
		{
			
			$data = array('password'=>hash("sha512", md5($this->input->post('b'))) );
            $where = array('username' => $this->input->post('a'));
            $this->model_app->update('users', $data, $where);
			echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Password Berhasil Di Ubah!!</center></div>');
			redirect($this->uri->segment(1).'/ganti_password');
		}
		else
		{			
            //if ($this->session->username==$this->uri->segment(3) OR $this->session->level=='admin'){
			if (isset($this->session->username) OR $this->session->level=='admin')
			{
                $proses = $this->model_app->edit('users', array('username' => $id))->row_array();
                $akses = $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
                $modul = $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
					
                // add captcha
                $vals = array(
					'img_path'   	=> './captcha/',
					'img_url'    	=> base_url().'captcha/',
					'font_path' 	=> './asset/Tahoma.ttf',
					'font_size'     => 17,
					'img_width'  	=> '320',
					'img_height' 	=> 33,
					'border' 		=> 0, 
					'word_length'   => 5,
					'expiration' 	=> 7200
				);
						
		        $cap 			= create_captcha($vals);
		       // $data['image'] 	= $cap['image'];
						
				$this->session->set_userdata('mycaptcha', $cap['word']);
                // end captcha
				
                $data = array('rows' => $proses, 'record' => $modul, 'akses' => $akses,'image'=>$cap['image']);
    			$this->template->load('pelamar/template','pelamar/view_users_ganti_password',$data);
            }
			else
			{
                redirect($this->uri->segment(1).'/ganti_password');
            }
		}
	}

    function list_pengirimancv()
    {
    	//die($this->session->username);
    	$username_pelamar=$this->session->username;

    	$data['record'] = $this->model_app->getListCvPelamar('kirim_cv',$username_pelamar,'DESC');
        $data['title'] = 'List Pengiriman CV Anda';
        $data['users'] = $this->model_app->view_where('users',array('email'=>$this->session->username))->row_array();

        $this->template->load('pelamar/template','pelamar/view_list_pengiriman_cv',$data);
    
    }
	
	/*Halaman keluar*/
	function logout(){
		$this->session->sess_destroy();
		redirect('main');
	}
}

