<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
		
	public function __construct()
    {
        parent::__construct();
		$this->load->library('encrypt');		
		$this->load->helper('captcha');
		$this->load->library('encrypt');
    }
	
	/*ui login utama pelamar*/
	public function pelamar()
	{
		$data['title'] 			= title();
		$data['description'] 	= description();
		$data['keywords'] 		= keywords();
		$data['keterangan_head'] 	= "Login Pelamar Kerja";
		
		/*$vals = array(
			'img_path'   	=> './captcha/',
			'img_url'    	=> base_url().'captcha/',
			'font_path' 	=> './asset/Tahoma.ttf',
			'font_size'     => 17,
			'img_width'  	=> '320',
			'img_height' 	=> 33,
			'border' 		=> 0, 
			'word_length'   => 5,
			'expiration' 	=> 7200
		);

        $cap 			= create_captcha($vals);
        $data['image'] 	= $cap['image'];
		
        $this->session->set_userdata('mycaptcha', $cap['word']);
        */
		$this->template->load(template().'/template',template().'/content_login',$data);
		
	}
	
	/*Halaman simpan daftar pelamar*/
	function simpan_pelamar()
	{
		if (isset($_POST['submit']))
		{
			if(strtolower($this->input->post('security_code')) == strtolower($this->session->userdata('mycaptcha')))
			{   
				$data = array('username'=>$this->db->escape_str($this->input->post('email')),
							'password'=>hash("sha512", md5($this->input->post('password'))),
							'nama_lengkap'=>$this->db->escape_str($this->input->post('nama_depan'))." ".$this->db->escape_str($this->input->post('nama_belakang')),
							'email'=>$this->db->escape_str($this->input->post('email')),
							'no_telp'=>'',
							'level'=> 'user',
							'blokir'=>'N',
							'id_session'=>md5($this->input->post('email')).'-'.date('YmdHis'),
							'status_user'=>'pelamar');
				
	            $this->model_app->insert('users',$data);
				
				ini_set( 'display_errors', 1 );   
				error_reporting( E_ALL );
				
				$id 		= strtr($this->encrypt->encode($_POST['email'], '123456'), array('+' => '.', '=' => '-', '/' => '~'));
				$status 	= strtr($this->encrypt->encode('pelamar', '123456'), array('+' => '.', '=' => '-', '/' => '~'));
				
				$to 		= $_POST['email'];    
				$subject 	= "Verifikasi Pendaftaran Jobs";
				
				$message 	= "Hallo ".$_POST['nama_depan']."".$_POST['nama_belakang'].",<br>
				Terima kasih telah melakukan pendaftaran. Silakan tekan tombol di bawah untuk melakukan konfirmasi email.
				<br><p style='margin-top:26px;margin-bottom:10px;text-align:center'>
				<a href='".base_url()."login"."/confirm?id=".$id."&status=".$status."' style='color:#ffffff!important;text-decoration:none;display:inline-block;min-height:38px;line-height:38px;padding-top:5px;padding-right:16px;padding-bottom:5px;padding-left:16px;border:0;outline:0;background-color:#fd6542;font-size:15px;font-style:normal;font-weight:400;text-align:center;white-space:nowrap;border-radius:4px;width:230px;margin-bottom:20px' target='_blank'>Konfirmasi Email!</a></p>";   
				
				// Always set content-type when sending HTML email
				$headers  = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers .= "From: jobs@gmail.com";
				
				mail($to,$subject,$message, $headers);
				echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Kami Telah mengirimkan Kode Verifikasi ke email anda, Silahkan di cek di kotak Masuk / Spam </center></div>');
	            redirect($this->uri->segment(1).'/pelamar');
	        }
	        else{
	        	echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Security Code salah!</center></div>');
                redirect($this->uri->segment(1).'/daftar_pelamar');
	        }
        }
	}
	
	/*cek login pelamar*/
	public function cek()
	{
		if (isset($_POST['submit']))
		{
            //if(strtolower($this->input->post('security_code')) == strtolower($this->session->userdata('mycaptcha')))
			if ($this->input->post() )
			{
                $username 	= $this->input->post('username');
    			$password 	= hash("sha512", md5($this->input->post('password')));
    			$cek 		= $this->model_app->cek_login_pelamar_perusahaan($username,$password,'users');

    		    $row 		= $cek->row_array();
    		    $total 		= $cek->num_rows();

    			if ($total > 0)
				{
    				if($row['status_confirm'] != 1)
					{
						echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Anda belum verifikasi</center></div>');
						if($row['status_user']=='pelamar')
						{
							redirect($this->uri->segment(1).'/pelamar');
						}
						else
						{
							redirect($this->uri->segment(1).'/perusahaan');
						}
					}
					else
					{
						$this->session->set_userdata('upload_image_file_manager',true);
						$this->session->set_userdata(array('username'=>$row['username'],
													   'nama_lengkap'=>$row['nama_lengkap'],
													   'level'=>$row['level'],
													   'status_user'=>$row['status_user'],
													   'id_session'=>$row['id_session']));
						if($row['status_user']=='pelamar')
						{
							redirect('/'.$this->input->post('detailBeritaUri'));
						}
						else
						{
							redirect(base_url().'perusahaan/index');
						}
					}
					
    			}else{
                    echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Username atau Password Salah!!</center></div>');
    				redirect($this->uri->segment(1).'/pelamar');
    			}
            }
			else
			{
                echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Security Code salah!</center></div>');
                if($row['status_user']=='pelamar')
				{
					redirect($this->uri->segment(1).'/pelamar');
				}
				else
				{
					redirect($this->uri->segment(1).'/perusahaan');
				}
            }
		}
	}
	

	/*Halaman utama setelah login pelamar*/
	function home_pelamar()
	{
		  //die($this->session->username);
          $data['users'] = $this->model_app->view_where('users',array('email'=>$this->session->username))->row_array();
          //$data['modul'] = $this->model_app->view_join_one('users','users_modul','id_session','id_umod','DESC');
          $this->template->load('pelamar/template','pelamar/view_home',$data);
    
	}

	function list_panggilan_tes()
	{
		  //die($this->session->username);
		  $data['title'] = 'Panggilan Tes Kerja';
          $data['users'] = $this->model_app->view_where('users',array('email'=>$this->session->username))->row_array();
          //$data['modul'] = $this->model_app->view_join_one('users','users_modul','id_session','id_umod','DESC');
          $this->template->load('pelamar/template','pelamar/view_list_panggilan_tes',$data);
    
	}
	

	/*ui daftar pelamar*/
	public function daftar_pelamar()
	{
		$data['title'] 			= title();
		$data['description'] 	= description();
		$data['keywords'] 		= keywords();
		$vals = array(
			'img_path'   	=> './captcha/',
			'img_url'    	=> base_url().'captcha/',
			'font_path' 	=> './asset/Tahoma.ttf',
			'font_size'     => 17,
			'img_width'  	=> '320',
			'img_height' 	=> 33,
			'border' 		=> 0, 
			'word_length'   => 5,
			'expiration' 	=> 7200
		);

        $cap 			= create_captcha($vals);
        $data['image'] 	= $cap['image'];
		
        $this->session->set_userdata('mycaptcha', $cap['word']);
		$this->template->load(template().'/template',template().'/content_daftar_pelamar',$data);
	}
	
	/*Halaman verifikasi pelamar*/
	function confirm()
	{
		$getId 		= strtr($this->input->get('id'),array('.' => '+', '-' => '=', '~' => '/'));
		$getStatus 	= strtr($this->input->get('status'),array('.' => '+', '-' => '=', '~' => '/'));
		
		$id 		= $this->encrypt->decode($getId,'123456');
		$status		= $this->encrypt->decode($getStatus,'123456');
		$cek 		= $this->model_app->cek_confirm($id,'users');
		$row 		= $cek->row_array();
		$total 		= $cek->num_rows();
		$data['msg_verifikasi'] = '<div class="alert alert-danger"><center>Data Anda Tidak Terdaftar / Sudah Verifikasi</center><br><br></div>';
		if ($total > 0)
		{
			$data = array('status_confirm'=>1);
			$where = array('username' => $id);
			$this->model_app->update('users', $data, $where);
			
			$data['msg_verifikasi'] = '<div class="alert alert-danger"><center>Data Anda Telah Terverifikasi</center><br><br></div>';
		}
		
		$data['title'] 			= title();
		$data['description'] 	= description();		
		$data['keywords'] 		= keywords();
		$vals = array(
			'img_path'   	=> './captcha/',
			'img_url'    	=> base_url().'captcha/',
			'font_path' 	=> './asset/Tahoma.ttf',
			'font_size'     => 17,
			'img_width'  	=> '320',
			'img_height' 	=> 33,
			'border' 		=> 0, 
			'word_length'   => 5,
			'expiration' 	=> 7200
		);

        $cap 			= create_captcha($vals);
        $data['image'] 	= $cap['image'];
			
        $this->session->set_userdata('mycaptcha', $cap['word']);
		if($status=='pelamar')
		{			
			$this->template->load(template().'/template',template().'/content_login',$data);
		}
		elseif($status=='perusahaan')
		{			
			$this->template->load(template().'/template',template().'/content_daftar_perusahaan',$data);
		}
		else
		{
			$this->template->load(template().'/template',template().'/main',$data);
		}
	}
	
	/*ui login utama perusahaan*/
	public function perusahaan()
	{
		$data['title'] 				= title();
		$data['description'] 		= description();
		$data['keywords'] 			= keywords();
		$data['keterangan_head'] 	= "Login Perusahaan";
		
		$this->template->load(template().'/template',template().'/content_login',$data);
		
	}
	
	/*ui daftar perusahaan*/
	function daftar_perusahaan()
	{
		$data['title'] = title();
		$data['description'] = description();
		$data['keywords'] = keywords();
		$vals = array(
			'img_path'   	=> './captcha/',
			'img_url'    	=> base_url().'captcha/',
			'font_path' 	=> './asset/Tahoma.ttf',
			'font_size'     => 17,
			'img_width'  	=> '320',
			'img_height' 	=> 33,
			'border' 		=> 0, 
			'word_length'   => 5,
			'expiration' 	=> 7200
		);

        $cap 			= create_captcha($vals);
        $data['image'] 	= $cap['image'];
		
        $this->session->set_userdata('mycaptcha', $cap['word']);
		$this->template->load(template().'/template',template().'/content_daftar_perusahaan',$data);
	}
	
	/*Halaman simpan daftar perusahaan*/
	function simpan_perusahaan()
	{
		if (isset($_POST['submit']))
		{
			if(strtolower($this->input->post('security_code')) == strtolower($this->session->userdata('mycaptcha')))
			{   
				$data = array('username'=>$this->db->escape_str($this->input->post('email')),
							'password'=>hash("sha512", md5($this->input->post('password'))),
							'nama_lengkap'=>$this->db->escape_str($this->input->post('nama_depan'))." ".$this->db->escape_str($this->input->post('nama_belakang')),
							'email'=>$this->db->escape_str($this->input->post('email')),
							'no_telp'=>'',
							'level'=> 'user',
							'blokir'=>'N',
							'id_session'=>md5($this->input->post('email')).'-'.date('YmdHis'),
							'status_user'=>'perusahaan');
				
	            $this->model_app->insert('users',$data);
				
				ini_set( 'display_errors', 1 );   
				error_reporting( E_ALL );
				
				$id 		= strtr($this->encrypt->encode($_POST['email'], '123456'), array('+' => '.', '=' => '-', '/' => '~'));
				$status 	= strtr($this->encrypt->encode('perusahaan', '123456'), array('+' => '.', '=' => '-', '/' => '~'));
				
				$to 		= $_POST['email'];    
				$subject 	= "Verifikasi Pendaftaran Jobs";
				
				$message 	= "Hallo ".$_POST['nama_depan']."".$_POST['nama_belakang'].",<br>
				Terima kasih telah melakukan pendaftaran. Silakan tekan tombol di bawah untuk melakukan konfirmasi email.
				<br><p style='margin-top:26px;margin-bottom:10px;text-align:center'>
				<a href='".base_url()."login"."/confirm?id=".$id."&status=".$status."' style='color:#ffffff!important;text-decoration:none;display:inline-block;min-height:38px;line-height:38px;padding-top:5px;padding-right:16px;padding-bottom:5px;padding-left:16px;border:0;outline:0;background-color:#fd6542;font-size:15px;font-style:normal;font-weight:400;text-align:center;white-space:nowrap;border-radius:4px;width:230px;margin-bottom:20px' target='_blank'>Konfirmasi Email!</a></p>";   
				
				// Always set content-type when sending HTML email
				$headers  = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers .= "From: jobs@gmail.com";

				echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Kami Telah mengirimkan Kode Verifikasi ke email anda, Silahkan di cek di kotak Masuk / Spam </center></div>');
		
	            redirect($this->uri->segment(1).'/perusahaan');
	        }
	        else{
	        	echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Security Code salah!</center></div>');
                redirect($this->uri->segment(1).'/daftar_perusahaan');
	        }
        }
	}
	
	/*mendapatkan random password*/	
	function random_password() 
	{
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$password = array(); 
		$alpha_length = strlen($alphabet) - 1; 
		for ($i = 0; $i < 8; $i++) 
		{
			$n = rand(0, $alpha_length);
			$password[] = $alphabet[$n];
		}
		return implode($password); 
	}
	
	/*proses lupa password*/
	function lupapassword()
	{
		$email = $this->input->post('email');
		$cek = $this->model_app->cek_email($email,'users');
		$row = $cek->row_array();
		$total = $cek->num_rows();	
		if ($total > 0)
		{
			
			$newRandomPassword 	= $this->random_password();
			$password 			= hash("sha512", md5($newRandomPassword));			
			$data 				= array('password'=>$password);
			$where 				= array('username' => $email);
			$user 				= $this->model_app->view_where('users',array('email'=>$email))->row_array();
			$this->model_app->update('users', $data, $where);						
			
			$to 		= $email;    
			$subject 	= "Lupa Password - SMK SEJAHTERA";				
			$message 	= "Hallo ".$user->nama_lengkap." Anda telah meminta untuk melakukan perubahan password ,<br>
							Berikut adalah password baru anda :<br><br>
							Password : $newRandomPassword <br><br> Terima kasih.";   
							
			// Always set content-type when sending HTML email
			$headers  = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= "From: jobs@gmail.com";
				
			mail($to,$subject,$message, $headers);
			echo '<script>alert("Password telah dikirim di email terdaftar!"); window.location.replace = \'pelamar'.'\'; window.location.href = \'pelamar'.'\';</script>';
		}
		else
		{
			echo '<script>alert("Email tidak terdaftar!"); window.location.replace = \'pelamar'.'\'; window.location.href = \'pelamar'.'\';</script>';
		}
		
	}	
	
	/*Halaman keluar*/
	function logout(){
		$this->session->sess_destroy();
		redirect('main');
	}
}

