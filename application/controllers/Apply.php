<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Apply extends CI_Controller {
		
	public function __construct()
    {
        parent::__construct();
		$this->load->library('encrypt');		
		$this->load->helper('captcha');
		$this->jScript = "<script type='text/javascript'> function get(){return sessionStorage.getItem('detailberita');} </script>";
    }
	
	/*Halaman utama setelah login pelamar*/
	function index()
	{
		$lampiran_pelamar	 	= $this->model_app->view_where_ordering_limit('lampiran_pelamar',array('username' => $this->session->username),'update_date','DESC',0,1)->row_array();
		$berita					= $this->model_app->view_join_where('berita','profil_perusahaan','username', array('judul_seo' => $this->uri->segment(3)),'id_berita','DESC');
		$data_riwayat_cv		= $this->model_app->view_where('kirim_cv',array('username'=>$this->session->username, 'id_berita'=>$berita[0]['id_berita']));
		$data['users'] 			= $this->model_app->view_where('users',array('email'=>$this->session->username))->row_array();
		$data['profil_pelamar'] = $this->model_app->view_where('profil_pelamar',array('email'=>$this->session->username))->row_array();
		$data['berita'] 		= $berita;
		$data['jml_riwayat_cv'] = $data_riwayat_cv->result_id->num_rows;
		$data['riwayat_cv'] 	= $data_riwayat_cv->row_array();
		
		if(!empty($lampiran_pelamar['update_date']))
		{
		  $data['lampiran_pelamar_last_date'] = '- '.$lampiran_pelamar['update_date'];
		}
		else
		{
		  $data['lampiran_pelamar_last_date'] = '- Tidak Ada Data Terupload';
		}
		  
		
		$this->template->load('application/template','application/view_home',$data);
	}
	
	function kirim_cv()
	{
		$post 		= $this->input->post();
		$id_berita  = $post['id_berita'];
		$pitch  	= $post['pitch'];
		$username   = $this->session->username;
		$data_riwayat = $this->model_app->view_where('kirim_cv',array('username'=>$username, 'id_berita'=>$id_berita))->row_array();
			
		$data = array(
						  'username'		=> $username,
						  'id_berita'		=> $id_berita,
						  'pitch'			=> $pitch,
						  'update_date'		=> date('Y-m-d')
						  );
		if($id_berita == '')
		{
			echo json_encode(array('message'=>'Data gagal di simpan, error data id '));
			exit;
		}
		if(empty($data_riwayat))
		{
			$this->model_app->insert('kirim_cv',$data);
			echo json_encode(array('message'=>'Data berhasil di simpan')); 
		}
		else
		{
			echo json_encode(array('message'=>'Data gagal di proses, sudah ada sebelumnya')); 
		}
		
		exit;
    
	}
	
	/*Halaman keluar*/
	function logout(){
		$this->session->sess_destroy();
		redirect('main');
	}
}

