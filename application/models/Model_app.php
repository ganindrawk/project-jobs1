<?php 
class Model_app extends CI_model{
    public function view($table){
        return $this->db->get($table);
    }

    public function view_where_ordering_limit($table,$data,$order,$ordering,$baris,$dari){
        $this->db->where($data);
        $this->db->order_by($order,$ordering);
        $this->db->limit($dari, $baris);
        return $this->db->get($table);
    }
    
    public function insert($table,$data){
        return $this->db->insert($table, $data);
    }

    public function edit($table, $data){
        return $this->db->get_where($table, $data);
    }
 
    public function update($table, $data, $where){
        return $this->db->update($table, $data, $where); 
    }

    public function delete($table, $where){
        return $this->db->delete($table, $where);
    }

    public function view_where($table,$data){
        $this->db->where($data);
        return $this->db->get($table);
    }

    public function view_ordering_limit($table,$order,$ordering,$baris,$dari){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by($order,$ordering);
        $this->db->limit($dari, $baris);
        return $this->db->get()->result_array();
    }
    
    public function view_ordering($table,$order,$ordering){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    public function view_where_ordering($table,$data,$order,$ordering){
        $this->db->where($data);
        $this->db->order_by($order,$ordering);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function view_join_one($table1,$table2,$field,$order,$ordering){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field.'='.$table2.'.'.$field);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }

    public function view_join_where($table1,$table2,$field,$where,$order,$ordering){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field.'='.$table2.'.'.$field);
        $this->db->where($where);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }
    
    public function view_join_where2($filed_select, $table1,$field1,$table2,$field2,$where,$order,$ordering){
        $this->db->select($filed_select);
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field1.'='.$table2.'.'.$field2);
        $this->db->where($where);
        $this->db->order_by($order,$ordering);
        return $this->db->get()->result_array();
    }
    
    function umenu_akses($link,$id){
        return $this->db->query("SELECT * FROM modul,users_modul WHERE modul.id_modul=users_modul.id_modul AND users_modul.id_session='$id' AND modul.link='$link'")->num_rows();
    }

    public function cek_login($username,$password,$table){
        return $this->db->query("SELECT * FROM $table where username='".$this->db->escape_str($username)."' AND password='".$this->db->escape_str($password)."' AND blokir='N'");
    }
    
    public function cek_login_pelamar_perusahaan($username,$password,$table){
        return $this->db->query("SELECT * FROM $table where username='".$this->db->escape_str($username)."' AND password='".$this->db->escape_str($password)."' and status_user in ('pelamar','perusahaan')");
    }

    public function cek_confirm($username,$table){
        return $this->db->query("SELECT * FROM $table where username='".$this->db->escape_str($username)."' and status_confirm != 1");
    }

    public function getProfil($username,$tipeUser,$table){
        return $this->db->query("SELECT * FROM users where username='".$this->db->escape_str($username)."' and status_user = 'pelamar'")->result();
    }
    
    public function cek_email($username,$table){
        return $this->db->query("SELECT * FROM $table where username='".$this->db->escape_str($username)."'");
    }

    public function getListCvPelamar($table,$username_pelamar,$sort){
        //print_r($username);die;
        $result = $this->db->query(
            "select
                kc.update_date,
                b.judul,
                ppr.nama_perusahaan
            from
                kirim_cv kc
            left join berita b on b.id_berita=kc.id_berita
            left join profil_perusahaan ppr on ppr.username = b.username
            where
                kc.username = '$username_pelamar'
            ")->result_array();
        //echo '<pre>';print_r($result);die;
        return $result;
    }

    public function listCvTerkirim($username,$sort){
        //print_r($username);die;
        $result = $this->db->query(
            "SELECT
                dkc.*, 
                ppr.alamat,
                kc.status_kirim
            FROM
                detail_kirim_cv dkc
            LEFT JOIN profil_perusahaan ppr on dkc.email_perusahaan = ppr.username
            LEFT JOIN kirim_cv kc on dkc.email_pelamar = kc.username
            WHERE
                dkc.email_pelamar = '$username'
            ")->result_array();
       // echo '<pre>';print_r($result);die;
        return $result;
    }

    public function getListKirimCV($table,$username_perusahaan,$sort,$array = null){
        //print_r($username);die;
        //die(print_r($array));
        $thnLulus           = "";
        $usia               = ""; 
        $jenisKelamin       = "";
        $nilaiSekolah       = "";
        $kejuruan           = "";
        $pengalamanKerja    = "";
        
        if(isset($array['thnLulus']) and !empty($array['thnLulus']))
        {
            $thnLulus           = " and al.tahun_lulus = '".$array['thnLulus']."'";
        }
        
        if(isset($array['usia']) and !empty($array['usia']))
        {
            $usia               = " and al.age = '".$array['usia']."'";    
        }
        if(isset($array['jenisKelamin']) and !empty($array['jenisKelamin']))
        {
            $jenisKelamin       = " and al.jenis_kelamin = '".$array['jenisKelamin']."'";
        }
        if(isset($array['nilaiSekolah']) and !empty($array['nilaiSekolah']))
        {
            $nilaiSekolah       = " and al.nilai_sekolah = '".$array['nilaiSekolah']."'";
        }
        if(isset($array['kejuruan']) and !empty($array['kejuruan']))
        {
            $kejuruan           = $array['kejuruan'];
        }
        if(isset($array['pengalamanKerja']) and !empty($array['pengalamanKerja']))
        {
            $pengalamanKerja    = " and al.lama_kerja = '".$array['pengalamanKerja']."'";
        }
        
        $result = $this->db->query(
            "select * from (select 
                pp.nama_lengkap,
                pp.username,
                pp.jenis_kelamin,
                pp.tanggal_lahir,

                (
                    select avg(nsp.nilai) from nilai_sekolah_pelamar nsp
                    where nsp.username = pp.username
                ) as nilai_sekolah,
                b.judul,
                YEAR(CURRENT_DATE() ) - YEAR(pp.tanggal_lahir ) - (RIGHT(CURRENT_DATE, 5) < RIGHT(pp.tanggal_lahir , 5)) as age,
                (SELECT pkp.lama_kerja
                   from pengalaman_kerja_pelamar pkp
                  where pkp.username = pp.username ) as lama_kerja,
                  md5(kc.id) as id_kc,
                case when kc.status_kirim = 1 then 'Sudah panggilan tes' else 'Belum dipanggil tes' end status,
                kc.status_kirim as status_kirim,
                b.username as username_perusahaan,
                pp.tahun_lulus
            from
                profil_pelamar pp 
            left join kirim_cv kc on pp.username=kc.username
            Left join berita b on b.id_berita = kc.id_berita ) al
            where
                al.username_perusahaan = '$username_perusahaan' $thnLulus $usia $jenisKelamin $nilaiSekolah $kejuruan $pengalamanKerja
            ")->result_array();
        //echo '<pre>';print_r($result);die;
        return $result;
    }

    public function getProfilPelamar($table,$username,$sort){
        //print_r($username);die;
        $result = $this->db->query(
            "select 
                u.username,
                u.nama_lengkap,
                pp.*,

                (
                    select avg(nsp.nilai) from nilai_sekolah_pelamar nsp
                    where nsp.username = u.username
                ) as nilai_sekolah,
                b.judul
            from
                users u
            left join profil_pelamar pp on u.username = pp.username
            left join kirim_cv kc on u.username=kc.username
            Left join berita b on b.id_berita = kc.id_berita 
            where
                u.username = '$username'
            ")->result_array();
        // echo $this->db->last_query();die;
        // echo '<pre>';print_r($result);die;
        return $result;
    }

    public function getNilaiSekolahPelamar($table,$username,$sort){
        //print_r($username);die;
        $result = $this->db->query(
            "SELECT 
                mp.nama_mata_pelajaran,
                nsp.nilai
            FROM
                $table nsp
                left join matapelajaran mp on mp.id = nsp.id_mata_pelajaran
            WHERE
                nsp.username = '$username'
            ")->result_array();
        return $result;
    }

    public function getPendidikanPelamar($table,$username,$sort){
        //print_r($username);die;
        $result = $this->db->query(
            "SELECT p.* FROM $table p WHERE
                p.username = '$username'
            ")->result_array();
        // echo $this->db->last_query();die;
        // echo '<pre>';print_r($result);die;
        return $result;
    }

    public function getSkillPelamar($table,$username,$sort){
        //print_r($username);die;
        $result = $this->db->query(
            "SELECT s.* FROM $table s WHERE
                s.username = '$username'
            ")->result_array();
        // echo $this->db->last_query();die;
        // echo '<pre>';print_r($result);die;
        return $result;
    }

    public function getPengalamanPelamar($table,$username,$sort){
        //print_r($username);die;
        $result = $this->db->query(
            "SELECT p.* FROM $table p WHERE
                p.username = '$username'
            ")->result_array();
        // echo $this->db->last_query();die;
        // echo '<pre>';print_r($result);die;
        return $result;
    }

    public function getLampiranPelamar($table,$username,$sort){
        //print_r($username);die;
        $result = $this->db->query(
            "SELECT
                lp.*,
                ml.* 
            FROM
                lampiran_pelamar lp
                LEFT JOIN master_lampiran ml ON ml.id = lp.id_master_dokumen 
            WHERE
                lp.username = '$username'
            ")->result_array();
        // echo $this->db->last_query();die;
        // echo '<pre>';print_r($result);die;
        return $result;
    }
    
    function grafik_kunjungan(){
        return $this->db->query("SELECT count(*) as jumlah, tanggal FROM statistik GROUP BY tanggal ORDER BY tanggal DESC LIMIT 10");
    }

    function kategori_populer($limit){
        return $this->db->query("SELECT * FROM (SELECT a.*, b.jum_dibaca FROM
                                    (SELECT * FROM kategori) as a left join
                                    (SELECT id_kategori, sum(dibaca) as jum_dibaca FROM berita GROUP BY id_kategori) as b on a.id_kategori=b.id_kategori) as c 
                                        where c.aktif='Y' ORDER BY c.jum_dibaca DESC LIMIT $limit");
    }
}