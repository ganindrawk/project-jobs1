# Created by : Rio Firmansyah Eka Saputra

## Pemasangan Library Esign

### Memasang library
1. Buat folder dengan nama 'Esign' di libraries
2. Clone/Download file didalam git ini
3. untuk clone bisa menggunakan perintah "git clone http://10.15.34.82/rio/esign-library.git"
4. lalu extract file hasil download, lalu copy semua file didalam folder esign-library
5. lalu paste didalam folder 'Esign' yang sudah dibuat sebelumnya di libraries CI
6. Buat file dengan nama 'esign_lib.php' didalam folder libraries CI.
7. Copy paste kode dibawah ini kedalam file 'esign_lib.php'

    ```
    <?php
    if (!defined('BASEPATH')) {
        exit('No direct script access allowed');
    }

    require_once dirname(__FILE__) . '\Esign\Esign.php';
    class esign_lib extends \Esign\Esign
    {
        public function __construct()
        {
            parent::__construct();
        }
    }
    ```
8. kemudian buat function di controller CI, lalu pada bagian construct panggil library dengan perintah berikut 
```
$this->load->library(array('esign_lib', 'session'));
```

9. lalu untuk pengetesannya, buat function index, lalu panggil perintah berikut : 
```
echo $this->esign_lib->index();
```


## Penggunaan API Esign

### Registrasi User
1. Variabel tipe TEXT dan harus di sebutkan dan di post pada form view
 - 1. *nik* (mandatory)
 - 2. *nama* (mandatory)
 - 3. *nip* (mandatory)
 - 4. *email* (mandatory)
 - 5. *jabatan*
 - 6. *nomor_telepon* (mandatory)
 - 7. *unit_kerja*
 - 8. *kota* (mandatory)
 - 9. *provinsi*  (mandatory)

2. Variabel yang bertipe FILE dan harus disebutkan pada form view, (*sebaiknya dokumen yang di upload bertipe PDF*)
 - 1. *ktp* (mandatory)
 - 2. *surat_rekomendasi* (mandatory)
 - 3. *sk_pengangkatan* (mandatory)
 - 4. *image_ttd* (mandatory)

3. Lalu panggil kode berikut didalam controller untuk Registrasi User
```
$this->esign_lib->registrasiUser();
```

4. untuk contoh hasil response seperti berikut :
```
success : 
{
    "message": "Pendaftaran berhasil"
}
```
```
error :
{
    "error": "NIK harus diisi"
}
```


### Send Sign Request
1. Variabel yang harus disebutkan dan di post pada form view
 - 1. *penandatangan* (mandatory)
 - 2. *tujuan*
 - 3. *perihal*
 - 4. *info*
 - 5. *jenis_dokumen* 
 - 6. *nomor*
 - 7. *tampilan* (di isikan dengan **visible** dan **invisible**)
 - 8. *image* (di isikan **true** dan **false**, bila **true** maka gambar tampil, bila **false** maka gambar tidak tampil)
 - 9. *linkQR* (di isikan dengan alamat download dokumen)
 - 10. *halaman* (di isikan hanya **pertama** dan **terakhir**)
 - 11. *yAxis* (di isikan dengan angka dan satuan milimeter)
 - 12. *xAxis* (di isikan dengan angka dan satuan milimeter)
 - 13. *width* (di isikan dengan angka dan satuan milimeter)
 - 14. *height* (di isikan dengan angka dan satuan milimeter)

2. Variabel yang bertipe FILE dan harus disebutkan pada form view (Dokumen upload bertipe PDF)
 - 1. *file* (mandatory)

3. Lalu panggil kode berikut didalam controller untuk send sign request
```
$this->esign_lib->sendSignRequest();
```


4. Untuk hasil response sebagai berikut :
```
{
    "message": "Permintaan tanda tangan telah disampaikan kepada user. Silahkan untuk mengecek status tanda tangan",
    "id_signed": "10d6b831f3a34a09aab272ed940d388a"
}
```


### Sign Dokumen
1. Variabel yang harus disebutkan dan di post pada form view
 - 1. *passphrase* (di isikan dengan passphare yang dimasukan pada saat pendaftaran)(mandatory)
 - 2. *id_signed* (didapatkan pada hasil respon send request)(mandatory)

2. Lalu panggil kode berikut didalam controller untuk Sign Dokumen
```
$this->esign_lib->signDokumen();
```

3. Untuk hasil response sebagai berikut :
```
{
    "waktu": "111 ms",
    "message": "Proses berhasil"
}
```

### Download Dokumen
1. Variabel yang harus disebutkan dan di post pada form view
 - 1. *id_signed* (didapatkan pada hasil respon send request)(mandatory)

2. Lalu panggil kode berikut didalam controller untuk Download Dokumen
```
$this->teslib->downloadDok('Di isi id_signed');
```
3. Untuk hasil response berupa file yang awalnya di send sign request


### Sign Dokumen & Download
1. Variabel yang harus disebutkan dan di post pada form view
 - 1. *passphrase* (di isikan dengan passphare yang dimasukan pada saat pendaftaran)(mandatory)
 - 2. *id_signed* (didapatkan pada hasil respon send request)(mandatory)

2. Lalu panggil kode berikut didalam controller untuk Download Dokumen
```
$this->esign_lib->signDokumenDownload();
```
3. Untuk hasil response berupa file yang awalnya di send sign request

