   
    <div class="col-md-12 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-book"></i></span>
            <div class="info-box-content">
                <span>PEMBERITAHUAN KEAMANAN YANG PENTING<br>
                      Berhati-hatilah terhadap iklan yang mengharuskan Anda mengeluarkan biaya untuk melamar atau memproses lamaran, atau yang terdengar terlalu muluk-muluk.
                      Baca Panduan Mencari Kerja Secara Aman untuk info lebih lanjut
                      Sebelum Anda melamar, silakan baca peraturan pelamaran dan wawancara
                </span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- /.col -->
   
    <section class="col-lg-12 connectedSortable">
        <div class='box'>
                <div class="pull-left image" style="padding-left:20px;padding-top:20px">
                    <img src="<?=base_url()?>/asset/foto_user/<?=$profil_pelamar['foto']?>" class="img-circle" alt="User Image" width="80" height="80">
                </div>
                    
                <div class="box-body">
                    <h3 class="candidate-name" id="candidate_name"> <strong> <?=ucfirst($profil_pelamar['nama_lengkap'])?> </strong> </h3>
                    <ul class="list-inline">
                        
                        <li class="candidate-info" id="candidate_email">
                            <span class="icon-envelope icon"></span>
                             <?=$profil_pelamar['email']?>
                        </li>
                        <li class="candidate-info" id="candidate_handphone_no">
                            <span class="icon-phone icon"></span>
                            <?=$profil_pelamar['hp']?>
                        </li>
                    </ul>
                    <h5 class="resume-action" id="resume_action">
                        <a class="hidden-xs" id="resume_edit" href="#" onclick="window.open('<?=base_url()?>pelamar/profil_pelamar','_blank','resizable=1,scrollbars=1,width=1050,height=600')">Edit</a>
                    </h5>
                </div>
                <hr class="separate-line"></hr>
                <div class="media-body" style="padding-left:20px;">
                    <h5 class="candidate-name" > Lampiran Dokumen Pelamar
                        <span class="text-muted"> <?=$lampiran_pelamar_last_date?></span>
                    </h5> 
                    <h5 class="resume-action">
                        <a class="hidden-xs" id="uploaded_resume_edit" href="#" onclick="window.open('<?=base_url()?>pelamar/lampiran_pelamar','_blank','resizable=1,scrollbars=1,width=1050,height=600');">Edit</a>                                                  
                    </h5>
                </div>
                <hr class="separate-line"></hr>
                <div class="media-body" style="padding-left:20px;">
                    <h3 class="candidate-name" id="candidate_name"> <strong> Deskripsi </strong> </h3>
                    
                    <h5 class="candidate-name" >
                        <span class="text-muted"> <?=$berita[0]['isi_berita']?></span>
                    </h5> 
                    
                </div>
                <hr class="separate-line"></hr>
                
                <div class="media-body" style="padding-left:20px;">
                    <h3 class="candidate-name" id="candidate_name"> <strong> Make your Pitch! (Recommended) </strong> </h3>
                    <input type="hidden" value="<?=$berita[0]['id_berita']?>" id="id_berita">
                    <h5 class="candidate-name" >
                        <textarea class="form-control" id="pitch" name="pitch" placeholder="Beri tahu pemberi kerja mengapa Anda paling cocok untuk pekerjaan ini. Soroti keterampilan khusus dan cara Anda berkontribusi. Hindari nada umum, misalnya saya bertanggung jawab." maxlength="300" index="1" rows="20" onkeyup="countChar(this);" onkeypress="if(event.keyCode==13){return false;}" spellcheck="false" style="margin: 0px 60.2539px 0px 0px; width: 1230px; height: 398px;"><?=$riwayat_cv['pitch'];?></textarea>
                    </h5> 
                    
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-9" style="padding-botton:200">
                        <?php
                        if($jml_riwayat_cv==0)
                        {
                            echo '<input class="btn btn-primary" id="btn_save" type="submit" name="submit" value="Submit Application" style="height:70px;" >&nbsp;&nbsp;';
                        }
                        else
                        {
                            echo '<input class="btn btn-primary" type="button" value="Anda Sudah Pernah Kirim CV" style="height:70px;" >&nbsp;&nbsp;';
                        }
                        ?>
                        
                    </div>
                </div>
                &nbsp;
        </div>
    </section><!-- /.Left col -->

    <section class="col-lg-5 connectedSortable">
        <?php include "grafik.php"; ?>
    </section><!-- right col -->
    
    <script type="text/javascript">
        $(function () {
            $('#btn_save').on('click', function () {
                var pitch       = document.getElementById("pitch").value;
                var id_berita   = document.getElementById("id_berita").value;
               
                let ask ="Anda yakin proses kirim";
                //return false;
                if(confirm(ask)){
                    $.ajax({
                        type:"POST",
                        url: '<?=base_url()?>apply/kirim_cv',
                        data: {
                            pitch: pitch,
                            id_berita: id_berita
                        },
                        dataType : 'json',
                        success:function(responsedata){
                            alert(responsedata.message);
                            window.location.href = '<?=base_url();?>';
                        }
                    });
                }
            });
        });
    </script>
