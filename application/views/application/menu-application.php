        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <?php
                        $usr = $this->model_app->view_where('users', array('username'=> $this->session->username))->row_array();
                        if (empty($berita[0]['foto_kantor']))
                        {
                            $foto = 'blank.png';
                        }
                        else
                        {
                            $foto = $berita[0]['foto_kantor'];
                        }
                    ?>
                    <img src="<?php echo base_url(); ?>/asset/foto_perusahaan/<?php echo $foto; ?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <?php
                        echo "<p>".$berita[0]['judul']."</p>";
                        echo "<p>".$berita[0]['nama_perusahaan']."</p>";
                    ?>                    
                </div>
                <div class="sidebar-menu">
                    
                </div>
            </div>

        <ul class="sidebar-menu">
            <li class="header" style='color:#fff; text-transform:uppercase;'>
                <i class="fa fa-map-marker" style="font-size:24px;color:red"></i>
                <?php echo $berita[0]['alamat'];?>
                
            </li>
        </ul>
        </section>