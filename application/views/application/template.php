<?php 
if ($this->session->level=='')
{
    redirect(base_url());
}
else
{
?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title>WELCOME ADMINISTRATOR</title>
            <meta name="author" content="phpmu.com">
            <link rel="shortcut icon" href="<?php echo base_url(); ?>asset/images/<?php echo favicon(); ?>" />
            <!-- Tell the browser to be responsive to screen width -->
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <!-- Bootstrap 3.3.5 -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bootstrap/css/bootstrap.min.css">
            <!-- Font Awesome -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
            <!-- Ionicons -->
            <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
            <!-- DataTables -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>/asset/admin/plugins/datatables/dataTables.bootstrap.css">
            <!-- Theme style -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/AdminLTE.min.css">
            <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/style.css">
            <!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/skins/_all-skins.min.css">
            <!-- iCheck -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/plugins/iCheck/flat/blue.css">
            <!-- Morris chart -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/plugins/morris/morris.css">
            <!-- jvectormap -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
            <!-- Date Picker -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/plugins/datepicker/datepicker3.css">
            <!-- Daterange picker -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/plugins/daterangepicker/daterangepicker-bs3.css">
            <style type="text/css">
            .files{
                position:absolute;
                z-index:2;
                top:0; left:0;
                filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
                opacity:0;
                background-color:transparent;
                color:transparent;
            }
            
            .nowrap {
                white-space: nowrap ;
            }
            /*
            .resume-summary {
                position: relative;
                margin-bottom: 10px
            }
            
            .resume-summary h3 {
                font-size: 18px;
                font-weight: 700;
                margin-bottom: 5px;
                color: #1c3f94
            }
            
            .resume-summary .list-inline {
                color: #666
            }
            
            @media (max-width:767px) {
                .resume-summary .list-inline>li {
                    display: block;
                    line-height: 1.8
                }
                .resume-summary .list-inline>li [class*="icon-"],
                .resume-summary .list-inline>li [class^="icon-"] {
                    min-width: 20px;
                    text-align: center
                }
            }
            
            @media (min-width:768px) {
                .resume-summary .list-inline>li+li:before {
                    content: "\0399";
                    padding-right: 15px
                }
            }
            
            .resume-summary .media img {
                width: 46px;
                height: 46px
            }
            
            .resume-summary {
                position: relative;
                margin-bottom: 10px
            }
            
            @media only screen and (max-width:767px) {
                .resume-summary {
                    padding-top: 18px
                }
            }
            
            .resume-summary .media:first-child {
                margin-top: 0
            }
            
            .resume-summary .media-left,
            .resume-summary .media-right,
            .resume-summary .media-body {
                display: table-cell;
                vertical-align: top
            }
            
            .resume-summary .resume-photo {
                max-width: 45px;
                max-height: 45px
            }
            
            .resume-summary .media-body .candidate-name {
                font-size: 18px;
                margin-bottom: 5px;
                color: #1c3f94;
                word-break: break-word;
                margin-left: 7px
            }
            
            .resume-summary .list-inline {
                color: #666;
                padding-left: 0;
                list-style: none;
                margin-left: -5px
            }
            
            @media (min-width:768px) {
                .resume-summary .list-inline>li+li:before {
                    content: "\0399";
                    padding-right: 15px
                }
            }
            
            .resume-summary .list-inline .icon {
                font-size: 1.4em
            }
            
            .resume-summary .resume-action {
                position: absolute;
                top: 0;
                right: 0;
                margin-top: 0
            }
            
            .resume-summary .separate-line {
                margin-top: 0;
                margin-bottom: 0
            }
            
            .resume-summary .text-muted {
                color: #777
            }
            */
            </style>
            
            <script type="text/javascript" src="<?php echo base_url(); ?>/asset/admin/plugins/jQuery/jquery-1.12.3.min.js"></script>
            <script src="<?php echo base_url(''); ?>asset/ckeditor/ckeditor.js"></script>
            <style type="text/css">.checkbox-scroll { border:1px solid #ccc; width:100%; height: 114px; padding-left:8px; overflow-y: scroll; }</style>
            <!--<link rel="stylesheet" href="https://almsaeedstudio.com/themes/AdminLTE/plugins/pace/pace.min.css">-->
            
        </head>
        <body class="hold-transition skin-blue sidebar-mini">
            <div class="wrapper">
                
                        
                <aside class="main-sidebar">
                    <?php include "menu-application.php"; ?>
                </aside>
                        
                <div class="content-wrapper">                    
                    
                    <section class="content">
                        <?php echo $contents; ?>
                    </section>
                    
                    <div style='clear:both'></div>
                </div><!-- /.content-wrapper -->
                <footer class="main-footer">
                    <?php include "footer.php"; ?>
                </footer>
            </div><!-- ./wrapper -->
        
            <!-- jQuery 2.1.4 -->
            <script src="<?php echo base_url(); ?>asset/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
            
            <!-- jQuery UI 1.11.4 -->
            <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
            
            <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
            <script>$.widget.bridge('uibutton', $.ui.button);</script>
            <script src="https://code.highcharts.com/highcharts.js"></script>
            <script src="https://code.highcharts.com/modules/data.js"></script>
            <script src="https://code.highcharts.com/modules/exporting.js"></script>
            
            <!-- Bootstrap 3.3.5 -->
            <script src="<?php echo base_url(); ?>asset/admin/bootstrap/js/bootstrap.min.js"></script>
            <!--<script src="https://almsaeedstudio.com/themes/AdminLTE/plugins/pace/pace.min.js"></script>-->
            <script src=https://cdnjs.cloudflare.com/ajax/libs/pace/1.2.4/pace.min.js"></script>
            
            <!-- DataTables -->
            <script src="<?php echo base_url(); ?>asset/admin/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="<?php echo base_url(); ?>asset/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
            
            <!-- Morris.js charts -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
            <script src="<?php echo base_url(); ?>asset/admin/plugins/morris/morris.min.js"></script>
            
            <!-- Sparkline -->
            <script src="<?php echo base_url(); ?>asset/admin/plugins/sparkline/jquery.sparkline.min.js"></script>
            
            <!-- jvectormap -->
            <script src="<?php echo base_url(); ?>asset/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
            <script src="<?php echo base_url(); ?>asset/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
            
            <!-- jQuery Knob Chart -->
            <script src="<?php echo base_url(); ?>asset/admin/plugins/knob/jquery.knob.js"></script>
            
            <!-- daterangepicker -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
            <script src="<?php echo base_url(); ?>asset/admin/plugins/daterangepicker/daterangepicker.js"></script>
            
            <!-- datepicker -->
            <script src="<?php echo base_url(); ?>asset/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
            <!-- Slimscroll -->
            <script src="<?php echo base_url(); ?>asset/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
            <!-- FastClick -->
            <script src="<?php echo base_url(); ?>asset/admin/plugins/fastclick/fastclick.min.js"></script>
            <!-- AdminLTE App -->
            <script src="<?php echo base_url(); ?>asset/admin/dist/js/app.min.js"></script>
            <script src="<?php echo base_url(); ?>asset/admin/dist/js/jquery.nestable.js"></script>
            
            <script>
                $('#rangepicker').daterangepicker();
                $('.datepicker').datepicker();
                $(function () { 
                    $("#example1").DataTable();
                    $('#example2').DataTable({
                        "paging": true,
                        "lengthChange": false,
                        "searching": false,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false
                    });
                });
                
                $(".datepicker2").datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    todayHighlight: true,
                });
                
               
                    $('#datetimepicker1').datepicker({
                        format: 'yyyy-mm-dd',
                    autoclose: true,
                    todayHighlight: true,
                    });
               
            </script>
    
            <script>
                /*CKEDITOR.replace('editor1' ,{
                  filebrowserImageBrowseUrl : '<?php echo base_url('asset/kcfinder'); ?>'
                });
                */
            </script>
            <script type="text/javascript">
            // To make Pace works on Ajax calls
            //$(document).ajaxStart(function() { Pace.restart(); });
              $('.ajax').click(function(){
                  $.ajax({url: '#', success: function(result){
                      $('.ajax-content').html('<hr>Ajax Request Completed !');
                  }});
              });
                    
              var url = window.location;
              // for sidebar menu entirely but not cover treeview
              $('ul.sidebar-menu a').filter(function() {
                return this.href == url;
              }).parent().addClass('active');
          
              // for treeview
              $('ul.treeview-menu a').filter(function() {
                return this.href == url;
              }).closest('.treeview').addClass('active');
            </script>
        </body>
    </html>
<?php } ?>
