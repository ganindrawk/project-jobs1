<style>
  th {

  } 
  td.profile {
    background-color: grey;
    color: white;
  }
</style>


        <div class='col-md-12'>
            <div class='box box-info'>
                <div class='box-header with-border'>
                    <h3 class='box-title'>Profile Perusahaan</h3>
                </div>
                <div class='box-body'>
                <?php
                if(isset($message))
                {
                    echo $message;
                }
                $attributes = array('class'=>'form-horizontal','role'=>'form');
                echo form_open_multipart($this->uri->segment(1).'/profil_perusahaan',$attributes); 
                if ($rows['foto_perusahaan']=='')
                {
                    $foto = 'users.gif';
                }
                else
                {
                    $foto = $rows['foto_perusahaan'];
                }
                ?> 
                    <div class='col-md-12'>
                        <table class='table table-condensed table-bordered'>
                            <tbody>
                                <tr>
                                    <th width='120px' scope='row'></th>
                                    <td>
                                        <input type='hidden' class='form-control' name='username' value='<?=$rows['username']?>' readonly='on'>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Nama Perusahaan</th>
                                    <td>
                                        <input type="text" class="form-control" name="nama_perusahaan" value="<?=$rows['nama_perusahaan']?>" required>
                                    </td>
                                    <th scope="row">Telpon 2</th>
                                    <td>
                                        <input type="text" class="form-control" name="telp2" value="<?=$rows['telp2']?>">
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Alamat Kantor*</th>
                                    <td>
                                        <input type="text" class="form-control" name="alamat" value="<?=$rows['alamat']?>" required>
                                    </td>
                                    <th scope="row">Email*</th>
                                    <td>
                                        <input type="email" class="form-control" name="email" value="<?=$rows['email']?>" required>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Provinsi</th>
                                    <td>
                                        <input type="text" class="form-control" name="provinsi" value="<?=$rows['provinsi']?>" required>
                                    </td>
                                    <th scope="row">Ponsel WA*</th>
                                    <td>
                                        <input type="text" class="form-control" name="ponsel_wa" value="<?=$rows['ponsel_wa']?>">
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Kota/Kabupaten</th>
                                    <td>
                                        <input type="text" class="form-control" name="kota" value="<?=$rows['kota']?>" required>
                                    </td>
                                    <th scope="row">Nama Direktur/Pimpinan</th>
                                    <td>
                                        <input type="text" class="form-control" name="nama_direktur" value="<?=$rows['nama_direktur']?>">
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Kecamatan</th>
                                    <td>
                                        <input type="text" class="form-control" name="kecamatan" value="<?=$rows['kecamatan']?>" required>
                                    </td>
                                    <th scope="row">Nama Manager HRD</th>
                                    <td><input type="text" class="form-control" name="nama_manager_hrd" value="<?=$rows['nama_manager_hrd']?>"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Kelurahan</th>
                                    <td>
                                        <input type="text" class="form-control" name="kelurahan" value="<?=$rows['kelurahan']?>" required>
                                    </td>
                                    <th scope="row">Jumlah Karyawan</th>
                                    <td>
                                        <input type="text" class="form-control" name="jumlah_karyawan" value="<?=$rows['jumlah_karyawan']?>">
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Kode Pos</th>
                                    <td>
                                        <input type="text" class="form-control" name="kode_pos" value="<?=$rows['kode_pos']?>" required>
                                    </td>
                                    <th scope="row">Status Kantor Saat Ini</th>
                                    <td>
                                        <input type="text" class="form-control" name="status_kantor" value="<?=$rows['status_kantor']?>" required>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Nomor SIUP</th>
                                    <td>
                                        <input type="text" class="form-control" name="siup" value="<?=$rows['siup']?>" required>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">NPWP*</th>
                                    <td>
                                        <input type="text" class="form-control" name="npwp" value="<?=$rows['npwp']?>" required>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Sektor Usaha*</th>
                                    <td>
                                        <input type="text" class="form-control" name="sektor_usaha" value="<?=$rows['sektor_usaha']?>" required>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Telpon 1</th>
                                    <td>
                                        <input type="text" class="form-control" name="telp1" value="<?=$rows['telp1']?>" required>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Extention</th>
                                    <td>
                                        <input type="number" class="form-control" name="extension" value="<?=$rows['extension']?>">
                                    </td>
                                </tr>
                                <tr>
                                    <th scope='row'>Foto Kantor Tampak Depan</th>
                                    <td>
                                        <input type='file' class='form-control' name='f'><hr style='margin:5px'>
                                        <?php
                                        if ($rows['foto_kantor'] != '')
                                        {
                                        ?>
                                            <i style='color:red'>Foto Saat ini : </i><a target='_BLANK' href='<?=base_url()?>asset/foto_perusahaan/<?=$rows['foto_kantor']?>'><?=$rows['foto_kantor']?></a>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                    
                    <div class='box-footer'>
                        <button type='submit' name='submit' class='btn btn-info'>Simpan</button>
                    </div>
                <?php
                echo form_close();
                ?>
                    
                </div>
            </div>
        </div>
            
    <script type="text/javascript">


        function getArrData(el){
            var arr_data = [];
            $(el).each(function(i,obj){
                var elchild = $(obj).children();                
                var arr_child = [];
                $(elchild).each(function(j,objchl){                                            
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            if(typeof newValue !== undefined && newValue !== false) {  
                                arr_child.push(newValue);
                            }
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();
                            arr_child.push(newValue);                                                      
                        }

                        
                    }                         
                 });                
                arr_data.push(arr_child);
            });
            return arr_data;
        }

        function getArrTableExisting(element){
            var arr_data = [];            
             $(element).each(function(i,obj){                
                console.log(obj);
                var id = $(obj).attr("id").split("-")[1];                          
                var elchild = $(obj).children();                    
                var arr_child = [];
                $(elchild).each(function(j,objchl){                        
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            arr_child.push(newValue);
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();                                
                            arr_child.push(newValue);
                        }                        
                    }                         
                 });

                arr_data.push({id: id, data: arr_child});
            });    
            return arr_data;
        }


        function fnClickAddRow(){
            var eltblbody =$(".contains-body-pendidikan").text();  
            var contain_body_pendidikan = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX").last();
            
            var html = '<tr class="gradeX tambahan tambahan-new" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Nama Sekolah" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="tahun_lulus" class="form-control allowNumberWithDotsOnly" value="0"/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pendidikan==""){
                $('.contains-body-pendidikan').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumber('.gradeX.tambahan');
        }

         function fnClickAddRowSkill(){
            var eltblbody =$(".contains-body-skill").text();  
            var contain_body_skill = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_skill").last();
            
            var html = '<tr class="gradeX_skill tambahan tambahan-new-skill" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Ketrampilan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Sertifikat" class="form-control "/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_skill==""){
                $('.contains-body-skill').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberSkill('.gradeX_skill.tambahan');
        }

         function fnClickAddRowPengalaman(){
            var eltblbody =$(".contains-body-pengalaman").text();  
            var contain_body_pengalaman = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_pengalaman").last();
            
            var html = '<tr class="gradeX_pengalaman tambahan tambahan-new-pengalaman" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Perusahaan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Bagian Kerja" class="form-control "/></td>';
                html = html + '<td><input type="text" placeholder="Lama Kerja" class="form-control "/></td>';  
                html = html + '<td><input type="text" placeholder="Packlaring Kerja" class="form-control "/></td>';                    
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pengalaman==""){
                $('.contains-body-pengalaman').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberPengalaman('.gradeX_pengalaman.tambahan');
        }
    

        function onDelete(elm){              
            if (!confirm('Apakah mau dihapus?')){                
                return;
            }
            $(".btn").attr("disabled",true);
            var el_add_row = $(".new");
            if (!el_add_row){
                $(".btn-submit").hide();
            }
            var el = $(elm).parent().parent();                        
            var arr_id = $(el).attr("id");
            var is_exist = false;
            if (arr_id){
                is_exist = true;
                var arr_id = $(el).attr("id").split("-");
            }
            if (is_exist){
                $.ajax({
                    type: "POST",
                    data: {id:arr_id[1]},
                    url: "delete",
                    success: function(msg){                    
                        var json = JSON.parse(msg);
                        if (json.response=="SUKSES"){
                            $(el).remove();  
                            $(".btn").attr("disabled",false);
                        }
                    }
                }); 
            }else{
                $(el).remove();  
                $(".btn").attr("disabled",false);
                $(".btn-submit").attr("disabled",true);
            }
            reSortNumber(".tambahan");                                        
        }

        


        function reSortNumber(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberSkill(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberPengalaman(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function isExistIds(ids, val){
            var is_exist = false;                
            if (ids.includes(val)) {                    
                is_exist = true;
            }            
            return is_exist;
        }
</script>