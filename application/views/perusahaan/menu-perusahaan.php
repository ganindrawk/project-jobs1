        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
            <?php $usr = $this->model_app->view_where('users', array('username'=> $this->session->username))->row_array();
                  if (trim($usr['foto'])==''){ $foto = 'blank.png'; }else{ $foto = $usr['foto']; } ?>
            <img src="<?php echo base_url(); ?>/asset/foto_perusahaan/<?php echo $foto; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <?php echo "<p>$usr[nama_lengkap]</p>"; ?>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header" style='color:#fff; text-transform:uppercase; border-bottom:2px solid #00c0ef'>MENU <span class='uppercase'><?php echo $this->session->level; ?></span></li>
            <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/index"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                          
            
            <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/profil_perusahaan/"><i class="fa fa-edit"></i> <span>Profile Perusahaan</span></a></li>

            <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/list_data_pelamar/"><i class="fa fa-edit"></i> <span>Data Pelamar</span></a></li>
            
            <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/list_berita_loker/"><i class="fa fa-edit"></i> <span>Berita Loker</span></a></li>

             <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/list_data_panggilan_tes_kerja/"><i class="fa fa-edit"></i> <span>Data Panggilan Tes Kerja</span></a></li>
            
            <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/ganti_password"><i class="fa fa-edit"></i> <span>Ganti Password</span></a></li>
            <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/logout"><i class="fa fa-power-off"></i> <span>Logout</span></a></li>
          </ul>
        </section>