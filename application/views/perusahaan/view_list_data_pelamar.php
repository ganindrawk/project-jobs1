<style>
  th {

  } 
  td.profile {
    background-color: grey;
    color: white;
  }
</style>


    <div class='col-md-12'>
        <div class='box box-info'>
            <div class='box-header with-border'>
                <h3 class='box-title'>List Data Pelamar</h3>
                <p>Terdapat .... data pelamar kerja yang dapat Anda seleksi sebagai calon pekerja di perusahaan Anda, silahkan lakukan pencarian data melalui kolom yang tersedia.</p>
            </div>
            <div class='box-body'>
                 
                    <div class='col-md-12'>
                        <table class='table table-condensed table-bordered'>
                            <tbody>
    
                                <tr>
                                    <th width='220px' scope='row'>Search By Tahun Lulus</th>
                                    <td>
                                        <input type='number' class='form-control' name='thnLulus' id='thnLulus' onKeyPress='if(this.value.length==4)return false;'>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope='row'>Search By Usia</th>
                                    <td>
                                       <input type='number' class='form-control' name='usia' id='usia' onKeyPress='if(this.value.length==3)return false;'>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope='row'>Search By Jenis Kelamin</th>
                                    <td>
                                        <select name='jenisKelamin' id='jenisKelamin' class='form-control' required>
                                            <option value='' selected>- Pilih Kategori -</option>
                                            <option value='pria'>Pria</option>
                                            <option value='wanita'>Wanita</option>
                                        </select>

                                    </td>
                                </tr>  

                                 <tr>
                                    <th scope='row'>Search By Nilai Sekolah Tertinggi</th>
                                    <td>
                                        <input type='text' class='form-control' name='nilaiSekolah' id='nilaiSekolah'>
                                    </td>
                                </tr>       

                                 <tr>
                                    <th scope='row'>Search By Kejuruan</th>
                                    <td>
                                        <input type='text' class='form-control' name='kejuruan' id='kejuruan'>
                                    </td>
                                </tr>       

                                 <tr>
                                    <th scope='row'>Search By Pengalaman Kerja</th>
                                    <td>
                                        <input type='text' class='form-control' name='pengalamanKerja' id='pengalamanKerja'>
                                    </td>
                                </tr>                                         
                               
                            </tbody>
                        </table>
                    </div>
                      
                    <div class='box-footer'>
                        <button type='submit' name='submit' id='submit' class='btn btn-info'>Search</button>
                    </div>
            </div>
        </div>
    </div>

    <!-- Result dari Search -->
    <div class='col-md-12'>
        <div class='box box-info'>
            <div class='box-header with-border'>
                <h3 class='box-title'>Hasil Pencarian Data Pelamar</h3>
             
            </div>
            <div class='box-body'>
                 
                    <div class='col-md-12'>
                       <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style='width:20px'>No</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Usia</th>
                        <th>Nilai Sekolah</th>
                        <th>Kejuruan</th>
                        <th>Pengalaman Kerja</th>
                        <th>Status</th>
                        <th style='width:75px'>Action</th>
                      </tr>
                    </thead>
                    <tbody id="content"></tbody>
                </table>
                    </div>
        
            </div>
        </div>
    </div>
    
<script type="text/javascript">
    $( document ).ready(function() {
        content();
    });
    function content(){
        $.ajax({
            type:"POST",
            url: '<?=base_url()?>perusahaan/list_data_pelamar_json',
            dataType : 'json',
            success:function(responsedata){
                document.getElementById("content").innerHTML = responsedata.hasil;
            }
        });
    }
    
    document.getElementById("submit").addEventListener('click',function (){
        var thnLulus            = document.getElementById("thnLulus").value;
        var usia                = document.getElementById("usia").value;
        var jenisKelamin        = document.getElementById("jenisKelamin").value;
        var nilaiSekolah        = document.getElementById("nilaiSekolah").value;
        var kejuruan            = document.getElementById("kejuruan").value;
        var pengalamanKerja     = document.getElementById("pengalamanKerja").value;
        
        $.ajax({
            type:"POST",
            url: '<?=base_url()?>perusahaan/list_data_pelamar_json',
            data: {
                thnLulus: thnLulus,
                usia:usia,
                jenisKelamin:jenisKelamin,
                nilaiSekolah:nilaiSekolah,
                kejuruan:kejuruan,
                pengalamanKerja:pengalamanKerja
            },
            dataType : 'json',
            success:function(responsedata){
                document.getElementById("content").innerHTML = responsedata.hasil;
            }
        });
    });
    
</script>