            <div class="col-xs-12">  
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Daftar Nama Pelamar untuk Pemanggilan Tes Kerja di Perusahaan Anda</h3>
                  <!-- <a class='pull-right btn btn-primary btn-sm' href='<?php echo base_url().$this->uri->segment(1); ?>/tambah_cuti'>Tambahkan Data</a> -->
                </div><!-- /.box-header -->
                <div class="box-body">

                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th style='width:20px'>No</th>
                            <th>Nama</th>
                            <th>Posisi Lamaran</th>
                            <th>Jenis Kelamin</th>
                            <th>Usia</th>
                            <th>Nilai Sekolah</th>
                            <th>Kejuruan</th>
                            <th>Pengalaman Kerja</th>
                            <th>Status</th>
                            <th style='width:75px'>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                             
                            foreach ($record as $row)
                            {
                                echo "  <tr>
                                            <td>$no</td>
                                            <td>$row[nama_lengkap]</td>
                                            <td>$row[judul]</td>
                                            <td>$row[jenis_kelamin]</td>
                                            <td>$row[age]</td>
                                            <td>$row[nilai_sekolah]</td>
                                            <td></td>
                                            <td>$row[lama_kerja] Tahun</td>
                                            <td>$row[status]</td>
                                            <td>
                                                <center>
                                                <a class='btn btn-success btn-xs' title='Lihat' href='".base_url().$this->uri->segment(1)."/form_profil_pelamar/$row[username]/$row[nilai_sekolah]'><span class='glyphicon glyphicon-eye-open'></span></a>
                                                ";
                                                if($row['status_kirim'] == 0)
                                                {
                                                    echo "<a class='btn btn-success btn-xs' title='Panggil Tes' href='".base_url().$this->uri->segment(1)."/form_panggil_test/".$row['id_kc']."'><span class='glyphicon glyphicon-earphone'></span></a>";
                                                }
                                                
                                        echo "    </center>
                                                </td>
                                        </tr>";
                                $no++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
