<style>
  th {

  } 
  td.profile {
    background-color: grey;
    color: white;
  }
</style>


        <div class='col-md-12'>
            <div class='box box-info'>
                <div class='box-header with-border'>
                    <h3 class='box-title'>Profil Pelamar</h3>
                </div>
                <div class='box-body'>
                <?php
                if(isset($message))
                {
                    echo $message;
                }
                $attributes = array('class'=>'form-horizontal','role'=>'form');
                echo form_open_multipart($this->uri->segment(1).'/#',$attributes); 
                $username = $this->uri->segment(3);
                ?> 
                    <div class='col-md-12'>
                        <table class='table table-condensed table-bordered'>
                            <tbody>
                                <tr>
                                    <th width='120px' scope='row'></th>
                                    <td>
                                        <input type='hidden' class='form-control' name='username' value='<?=$rows['username']?>' readonly='on'>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Nama</th>
                                    <td>
                                        <input type="text" class="form-control" value="<?=$rows[0]['nama_lengkap']?>" readonly='on'>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <th scope="row">NIK</th>
                                    <td>
                                        <input type="text" class="form-control" name="alamat" value="<?=$rows[0]['nik']?>" readonly='on'>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Tempat Lahir</th>
                                    <td>
                                        <input type="text" class="form-control" value="<?=$rows[0]['tempat_lahir']?>" readonly='on'>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Tanggal Lahir</th>
                                    <td>
                                         <div class='input-group date'>
                                        <input type="text" class="form-control" value="<?=$rows[0]['tanggal_lahir']?>" readonly='on'>
                                         <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Jenis Kelamin</th>
                                    <td>
                                    <input type="text" class="form-control" value="<?=$rows[0]['jenis_kelamin']?>" readonly='on'>
                                    </div>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">Agama</th>
                                    <td>
                                        <input type="text" class="form-control"  value="<?=$rows[0]['agama']?>" readonly='on'>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <th scope="row">Alamat</th>
                                    <td>
                                        <input type="text" class="form-control" value="<?=$rows[0]['alamat']?>" readonly='on'>
                                    </td>
                                </tr>

                                 <tr>
                                    <th scope="row">Email</th>
                                    <td>
                                        <input type="text" class="form-control" value="<?=$rows[0]['email']?>" readonly='on'>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">HP / WA</th>
                                    <td>
                                        <input type="text" class="form-control" value="<?=$rows[0]['hp']?>" readonly='on'>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">Tinggi Badan</th>
                                    <td>
                                        <input type="text" class="form-control" value="<?=$rows[0]['tinggi_badan']?>" readonly='on'>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">Berat Badan</th>
                                    <td>
                                        <input type="text" class="form-control" value="<?=$rows[0]['berat_badan']?>" readonly='on'>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">Buta Warna</th>
                                    <td>
                                        <input type="text" class="form-control" value="<?=$rows[0]['buta_warna']?>" readonly='on'>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">Berkacamata</th>
                                    <td>
                                        <input type="text" class="form-control" value="<?=$rows[0]['berkacamata']?>" readonly='on'>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">Status Bekerja</th>
                                    <td>
                                        <input type="text" class="form-control" value="<?=$rows[0]['status_kerja']?>" readonly='on'>
                                    </td>
                                </tr>

                                <tr>
                                    <th colspan="2" scope="row">Riwayat Pendidikan</th>
                                   
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    <table class="table table-bordered">
                                        <th class="bg-primary" scope="row">No</th>
                                        <th class="bg-primary" scope="row">Nama Sekolah</th>
                                        <th  class="bg-primary"scope="row">Tahun Lulus</th>
                                    </td>
                                </tr> 
                                <?php
                                        //print_r($nilai as $value);die;
                                        $no='1';
                                        foreach ($pendidikan as  $value) {
                                            echo "
                                                <tr>
                                                    <td scope=row>$no</td>
                                                    <td scope=row>$value[nama_sekolah]</td>
                                                    <td scope=row>$value[tahun_lulus]</td>
                                                </tr>
                                                ";
                                            $no++;
                                        }   
                                        ?>
                                </table>
                                
                                <tr>
                                    <th colspan="2" scope="row">Ketrampilan /  Skill</th>
                                   
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    <table class="table table-bordered">
                                        <th class="bg-primary" scope="row">No</th>
                                        <th class="bg-primary" scope="row">Ketrampilan /  Skill</th>
                                        <th class="bg-primary" scope="row">Sertifikat</th>
                                    </td>
                                </tr>
                                 <?php
                                        //print_r($nilai as $value);die;
                                        $no='1';
                                        foreach ($skill as  $value) {
                                            echo "
                                                <tr>
                                                    <td scope=row>$no</td>
                                                    <td scope=row>$value[nama_sertifikat]</td>
                                                    <td scope=row>$value[nomor_sertifikat]</td>
                                                </tr>
                                                ";
                                            $no++;
                                        }   
                                        ?>
                                </table>

                                <tr>
                                    <th colspan="2" scope="row">Pengalaman Kerja</th>
                                   
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    <table class="table table-bordered">
                                        <th class="bg-primary" scope="row">No</th>
                                        <th class="bg-primary" scope="row">Perusahaan</th>
                                        <th class="bg-primary" scope="row">Bagian Kerja</th>
                                        <th class="bg-primary" scope="row">Lama Kerja</th>
                                        <th class="bg-primary" scope="row">Packlaring Kerja</th>
                                    </td>
                                </tr>
                                <?php
                                        //print_r($nilai as $value);die;
                                        $no='1';
                                        foreach ($pengalaman as  $value) {
                                            echo "
                                                <tr>
                                                    <td scope=row>$no</td>
                                                    <td scope=row>$value[nama_perusahaan]</td>
                                                    <td scope=row>$value[bagian_kerja]</td>
                                                    <td scope=row>$value[lama_kerja]</td>
                                                    <td scope=row>$value[parklaring_kerja]</td>

                                                </tr>
                                                ";
                                            $no++;
                                        }   
                                        ?>
                                </table>

                                <tr>
                                    <th colspan="2" scope="row">Nilai Sekolah</th>
                                   
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    <table class="table table-bordered">
                                        <th class="bg-primary" scope="row">No</th>
                                        <th class="bg-primary" scope="row">Mata Pelajaran</th>
                                        <th class="bg-primary" scope="row">Nilai</th>
                                        <?php
                                        //print_r($nilai as $value);die;
                                        $no='1';
                                        foreach ($nilai as  $value) {
                                            echo "
                                                <tr>
                                                    <td scope=row>$no</td>
                                                    <td scope=row>$value[nama_mata_pelajaran]</td>
                                                    <td scope=row>$value[nilai]</td>
                                                </tr>
                                                ";
                                            $no++;
                                        }   
                                      echo "                                             <tr>                                       <td colspan=2 align=center><b>Rata-rata</b>
                                                  </td>
                                                  <td><b>$rata2</b>
                                                  </td>
                                                </tr>
                                            ";
                                        ?>
                                    </table>
                                    </td>
                                </tr>

                                <tr>
                                    <th colspan="2" scope="row">Upload Lampiran Dokumen Pelamar</th>
                                   
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    <table class="table table-bordered">
                                        <tr>
                                        <th class="bg-primary" scope="row">No</th>
                                        <th class="bg-primary" scope="row">Mata Pelajaran</th>
                                        <th class="bg-primary" scope="row">Status</th>
                                         <th class="bg-primary" scope="row">Aksi</th>
                                        </tr>
                                        <?php
                                        //print_r($nilai as $value);die;
                                        $no='1';
                                        foreach ($lampiran as  $value) {
                                            echo "
                                                <tr>
                                                    <td scope=row>$no</td>
                                                    <td scope=row>$value[dokumen]</td>";
                                               
                                            if($value[status]=='ada'){
                                                echo "<td scope=row>Ada</td>";
                                                echo "<td scope=row>
                                                        <a href='".base_url()."asset/lampiran_pelamar/$username/$value[file_lampiran]' target=_blank>Lihat</a>
                                                     </td>";
                                            }
                                            else{
                                                 echo "<td scope=row><a hrTidak Ada</td>";
                                                echo "<td scope=row></td>";
                                            }
                                            echo "</tr>";
                                            $no++;
                                        }   
                                        ?>
                                    </table>
                                </td>
                            </tr>                       
                                
                            </tbody>
                        </table>
                    </div>
                    
                    <div class='box-footer'>
                        <button type='submit' name='submit' class='btn btn-info'>Panggil Tes</button>
                    </div>
                <?php
                echo form_close();
                ?>
                    
                </div>
            </div>
        </div>
            
    <script type="text/javascript">


        function getArrData(el){
            var arr_data = [];
            $(el).each(function(i,obj){
                var elchild = $(obj).children();                
                var arr_child = [];
                $(elchild).each(function(j,objchl){                                            
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            if(typeof newValue !== undefined && newValue !== false) {  
                                arr_child.push(newValue);
                            }
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();
                            arr_child.push(newValue);                                                      
                        }

                        
                    }                         
                 });                
                arr_data.push(arr_child);
            });
            return arr_data;
        }

        function getArrTableExisting(element){
            var arr_data = [];            
             $(element).each(function(i,obj){                
                console.log(obj);
                var id = $(obj).attr("id").split("-")[1];                          
                var elchild = $(obj).children();                    
                var arr_child = [];
                $(elchild).each(function(j,objchl){                        
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            arr_child.push(newValue);
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();                                
                            arr_child.push(newValue);
                        }                        
                    }                         
                 });

                arr_data.push({id: id, data: arr_child});
            });    
            return arr_data;
        }


        function fnClickAddRow(){
            var eltblbody =$(".contains-body-pendidikan").text();  
            var contain_body_pendidikan = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX").last();
            
            var html = '<tr class="gradeX tambahan tambahan-new" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Nama Sekolah" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="tahun_lulus" class="form-control allowNumberWithDotsOnly" value="0"/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pendidikan==""){
                $('.contains-body-pendidikan').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumber('.gradeX.tambahan');
        }

         function fnClickAddRowSkill(){
            var eltblbody =$(".contains-body-skill").text();  
            var contain_body_skill = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_skill").last();
            
            var html = '<tr class="gradeX_skill tambahan tambahan-new-skill" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Ketrampilan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Sertifikat" class="form-control "/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_skill==""){
                $('.contains-body-skill').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberSkill('.gradeX_skill.tambahan');
        }

         function fnClickAddRowPengalaman(){
            var eltblbody =$(".contains-body-pengalaman").text();  
            var contain_body_pengalaman = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_pengalaman").last();
            
            var html = '<tr class="gradeX_pengalaman tambahan tambahan-new-pengalaman" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Perusahaan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Bagian Kerja" class="form-control "/></td>';
                html = html + '<td><input type="text" placeholder="Lama Kerja" class="form-control "/></td>';  
                html = html + '<td><input type="text" placeholder="Packlaring Kerja" class="form-control "/></td>';                    
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pengalaman==""){
                $('.contains-body-pengalaman').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberPengalaman('.gradeX_pengalaman.tambahan');
        }
    

        function onDelete(elm){              
            if (!confirm('Apakah mau dihapus?')){                
                return;
            }
            $(".btn").attr("disabled",true);
            var el_add_row = $(".new");
            if (!el_add_row){
                $(".btn-submit").hide();
            }
            var el = $(elm).parent().parent();                        
            var arr_id = $(el).attr("id");
            var is_exist = false;
            if (arr_id){
                is_exist = true;
                var arr_id = $(el).attr("id").split("-");
            }
            if (is_exist){
                $.ajax({
                    type: "POST",
                    data: {id:arr_id[1]},
                    url: "delete",
                    success: function(msg){                    
                        var json = JSON.parse(msg);
                        if (json.response=="SUKSES"){
                            $(el).remove();  
                            $(".btn").attr("disabled",false);
                        }
                    }
                }); 
            }else{
                $(el).remove();  
                $(".btn").attr("disabled",false);
                $(".btn-submit").attr("disabled",true);
            }
            reSortNumber(".tambahan");                                        
        }

        


        function reSortNumber(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberSkill(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberPengalaman(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function isExistIds(ids, val){
            var is_exist = false;                
            if (ids.includes(val)) {                    
                is_exist = true;
            }            
            return is_exist;
        }
</script>