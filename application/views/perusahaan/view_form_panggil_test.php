<style>
  th {

  } 
  td.profile {
    background-color: grey;
    color: white;
  }
</style>


        <div class='col-md-12'>
            <div class='box box-info'>
                <div class='box-header with-border'>
                    <h3 class='box-title'>Form Panggilan Test Kerja</h3>
                </div>
                <div class='box-body'>
                <?php
                if(isset($message))
                {
                    echo $message;
                }
                $attributes = array('class'=>'form-horizontal','role'=>'form');
                echo form_open_multipart($this->uri->segment(1).'/kirim_panggil_test',$attributes); 
                $nama_pelamar = str_replace("%20"," ",$this->uri->segment(3));
                ?> 
                    <div class='col-md-12'>
                        <table class='table table-condensed table-bordered'>
                            <tbody>
                                <tr>
                                    <th width='120px' scope='row'></th>
                                    <td>
                                        <input type='hidden' class='form-control' name='email_pelamar' value='<?=$profil_pelamar['username']?>' readonly='on'>
                                        <input type='hidden' class='form-control' name='id_kc' value='<?=$profil_pelamar['id']?>' readonly='on'>
                                        <input type='hidden' class='form-control' name='id_berita' value='<?=$berita['id_berita']?>' readonly='on'>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Nama Perusahaan</th>
                                    <td>
                                        <input type="text" class="form-control" name="nama_perusahaan" value="<?=$rows['nama_perusahaan']?>" readonly='on'>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <th scope="row">Alamat Kantor*</th>
                                    <td>
                                        <input type="text" class="form-control" name="alamat" value="<?=$rows['alamat']?>" readonly='on'>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Nama Pelamar</th>
                                    <td>
                                        <input type="text" class="form-control" name="nama_pelamar" value="<?=ucfirst($profil_pelamar['nama_lengkap'])?>" readonly='on'>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Posisi Jabatan</th>
                                    <td>
                                        <input type="text" class="form-control" name="posisi_jabatan" value="<?=$berita['judul']?>" readonly='on'>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Tanggal Tes</th>
                                    <td>
                                         <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' class="form-control" name='tanggal_tes' value='<?=$data_users['tanggal_tes']?>' readonly  required/>
                                        <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Waktu Tes</th>
                                    <td>
                                       <div class="input-group clockpicker">
                                            <input type="time" class="form-control" name="jam_mulai" value="">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div> s/d 
                                         <div class="input-group clockpicker">
                                            <input type="time" class="form-control" name="jam_selesai" value="">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">HP Manager HRD</th>
                                    <td>
                                        <input type="text" class="form-control" name="telp"">
                                    </td>
                                </tr>
                             
                                
                            </tbody>
                        </table>
                    </div>
                    
                    <div class='box-footer'>
                        <button type='submit' name='submit' class='btn btn-info'>Kirim Pesan</button>
                    </div>
                <?php
                echo form_close();
                ?>
                    
                </div>
            </div>
        </div>
            
    <script type="text/javascript">


        function getArrData(el){
            var arr_data = [];
            $(el).each(function(i,obj){
                var elchild = $(obj).children();                
                var arr_child = [];
                $(elchild).each(function(j,objchl){                                            
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            if(typeof newValue !== undefined && newValue !== false) {  
                                arr_child.push(newValue);
                            }
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();
                            arr_child.push(newValue);                                                      
                        }

                        
                    }                         
                 });                
                arr_data.push(arr_child);
            });
            return arr_data;
        }

        function getArrTableExisting(element){
            var arr_data = [];            
             $(element).each(function(i,obj){                
                console.log(obj);
                var id = $(obj).attr("id").split("-")[1];                          
                var elchild = $(obj).children();                    
                var arr_child = [];
                $(elchild).each(function(j,objchl){                        
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            arr_child.push(newValue);
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();                                
                            arr_child.push(newValue);
                        }                        
                    }                         
                 });

                arr_data.push({id: id, data: arr_child});
            });    
            return arr_data;
        }


        function fnClickAddRow(){
            var eltblbody =$(".contains-body-pendidikan").text();  
            var contain_body_pendidikan = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX").last();
            
            var html = '<tr class="gradeX tambahan tambahan-new" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Nama Sekolah" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="tahun_lulus" class="form-control allowNumberWithDotsOnly" value="0"/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pendidikan==""){
                $('.contains-body-pendidikan').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumber('.gradeX.tambahan');
        }

         function fnClickAddRowSkill(){
            var eltblbody =$(".contains-body-skill").text();  
            var contain_body_skill = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_skill").last();
            
            var html = '<tr class="gradeX_skill tambahan tambahan-new-skill" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Ketrampilan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Sertifikat" class="form-control "/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_skill==""){
                $('.contains-body-skill').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberSkill('.gradeX_skill.tambahan');
        }

         function fnClickAddRowPengalaman(){
            var eltblbody =$(".contains-body-pengalaman").text();  
            var contain_body_pengalaman = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_pengalaman").last();
            
            var html = '<tr class="gradeX_pengalaman tambahan tambahan-new-pengalaman" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Perusahaan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Bagian Kerja" class="form-control "/></td>';
                html = html + '<td><input type="text" placeholder="Lama Kerja" class="form-control "/></td>';  
                html = html + '<td><input type="text" placeholder="Packlaring Kerja" class="form-control "/></td>';                    
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pengalaman==""){
                $('.contains-body-pengalaman').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberPengalaman('.gradeX_pengalaman.tambahan');
        }
    

        function onDelete(elm){              
            if (!confirm('Apakah mau dihapus?')){                
                return;
            }
            $(".btn").attr("disabled",true);
            var el_add_row = $(".new");
            if (!el_add_row){
                $(".btn-submit").hide();
            }
            var el = $(elm).parent().parent();                        
            var arr_id = $(el).attr("id");
            var is_exist = false;
            if (arr_id){
                is_exist = true;
                var arr_id = $(el).attr("id").split("-");
            }
            if (is_exist){
                $.ajax({
                    type: "POST",
                    data: {id:arr_id[1]},
                    url: "delete",
                    success: function(msg){                    
                        var json = JSON.parse(msg);
                        if (json.response=="SUKSES"){
                            $(el).remove();  
                            $(".btn").attr("disabled",false);
                        }
                    }
                }); 
            }else{
                $(el).remove();  
                $(".btn").attr("disabled",false);
                $(".btn-submit").attr("disabled",true);
            }
            reSortNumber(".tambahan");                                        
        }

        


        function reSortNumber(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberSkill(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberPengalaman(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function isExistIds(ids, val){
            var is_exist = false;                
            if (ids.includes(val)) {                    
                is_exist = true;
            }            
            return is_exist;
        }
</script>