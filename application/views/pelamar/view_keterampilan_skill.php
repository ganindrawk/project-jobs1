<style>
  th {

  } 
  td.profile {
    background-color: grey;
    color: white;
  }
</style>


    <div class='col-md-12'>
        <div class='box box-info'>
            <div class='box-header with-border'>
              <h3 class='box-title'>Data Keterampilan / Skill</h3>
            </div>
            <div class='box-body'>
                <?php
                if(isset($message))
                {
                    echo $message;
                }          
                ?>
                
                <div class="panel panel-default resume-panel" id="panel" style="min-height: 338px;">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-xs-7">
                                                <div class="row">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-xs-5 text-right" id="tambah_resume">
                                                <a class="" id="add_education" href="javascript:;" title="Add Education" onclick="return showhide();">
                                                        <i class="fa fa-plus-circle"></i>&nbsp;&nbsp;&nbsp;
                                                        <span class="add-detail" id="lbl_edu_add">Tambah</span></a>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="resume-content-form c-hide" id="resume_content_form" style="display:none;">
                                    <?php
                                    $attributes = array('class'=>'form-horizontal','role'=>'form','id'=>'frmEducation');
                                    echo form_open_multipart($this->uri->segment(1).'/keterampilan_skill',$attributes); 
                                    ?>
                                         <div class="c-hide" id="eduFrm0">
                                            <div class="form-group ">
                                                <label class="col-sm-3 control-label custom-control-label required" for="nama_sekolah" aria-required="true">Nama Keterampilan</label>
                                                <div class="col-sm-7">
                                                    <input type="hidden" name="id_keterampilan" value="" id="id_keterampilan">
                                                    <input class="form-control" id="nama_sertifikat" name="nama_sertifikat" type="text" value="" maxlength="100" autocomplete="off" required>
                                                 </div>
                                            </div>
                                            
                                            <div class="form-group ">
                                                <label class="col-sm-3 control-label custom-control-label required" for="nama_sekolah" aria-required="true">Nomor Sertifikat</label>
                                                <div class="col-sm-7">
                                                    <input class="form-control" id="nomor_sertifikat" name="nomor_sertifikat" type="text" value="" maxlength="100" autocomplete="off" required>
                                                 </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label custom-control-label required " aria-required="true">Tahun</label>
                                                <div class="col-sm-7">
                                                    <div class="row">
                                                        <div class="col-sm-4 col-xs-6">
                                                            <input class="form-control" id="tahun_sertifikat" name="tahun_sertifikat" type="number"  onKeyPress="if(this.value.length==4) return false;" required>
                                                        </div>                                                        
                                                        <div class="col-xs-12 error-container"></div>
                                                    </div>
                                                </div>
                                            </div>
                                                                                        
                                                                                    
                                        </div>
                            
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <input class="btn btn-primary" id="btn_save" type="submit" name="submit" value="Save">&nbsp;&nbsp;
                                                <input class="btn btn-default" id="btn_cancel" type="button" name="btn_cancel" value="Cancel" onclick="return showhide();">
                                           </div>
                                        </div>
                                        </form>
                                    <?php
                                        echo form_close();
                                    ?>
                                </div>
                                
                                <?php
                                $no=0;
                                foreach($data_keterampilan->result_array() as $data)
                                {
                                ?>
                                    <div class="form-horizontal resume-section-content">
                                        <div class="form-group resume-detail-group" id="edu_display0">
                                            <label class="col-sm-3  custom-control-label visible-sm visible-md visible-lg c-text-left" style="text-align: right;" id="lbl_edu_gra_date0"> <?=$data['tahun_sertifikat']?></label>
                                            <div class="col-sm-9">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <div class="row">
                                                            <input type="hidden" value="<?=$data['nama_sertifikat']?>" name="nama_sertifikat_<?=$no?>"  id="nama_sertifikat_<?=$no?>">
                                                            <input type="hidden" value="<?=$data['nomor_sertifikat']?>" name="nomor_sertifikat_<?=$no?>"  id="nomor_sertifikat_<?=$no?>">
                                                            <input type="hidden" value="<?=$data['tahun_sertifikat']?>" name="tahun_sertifikat_<?=$no?>"  id="tahun_sertifikat_<?=$no?>">
                                                            <input type="hidden" value="<?=$data['id']?>" name="id_<?=$no?>"  id="id_<?=$no?>">
                                                            
                                                            <div class="col-xs-4"><span class="resume-title" id="lbl_edu_institute_name0"><?=$data['nomor_sertifikat']?></span></div>
                                                            <div class="col-xs-4 text-left">
                                                                <a id="edu_edit_link0" href="#" title="Edit" onClick="edit('<?=$no?>')"><span class="icon-edit icon-style"></span>Edit</a>
                                                                <a class="hidden-xs" id="edu_delete_link0" href="#" data-confirm="Are you sure you want to delete this education record?" title="Delete" onClick="hapus('<?=$data['id']?>')">&nbsp;&nbsp;<span class="icon-trash-bin icon-style"></span>Hapus</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <span class="resume-sub-title" id="lbl_edu_qualification_name0"> <?=$data['nama_sertifikat']?> </span>
                                                    </div>
                                                                        
                                                </div>
                                                
                                            </div>
                                        </div>                        
                                    </div>
                                <?php
                                    $no++;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <script type="text/javascript">
        //filter only numeric
        $( document ).ready(function() {
                //document.getElementById("resume_content_form").style.display = "none";
               
        });
        
        function edit(no)
        {            
            var div = document.getElementById("resume_content_form");
            var div_tambah = document.getElementById("tambah_resume"); 
            div.style.display = "block";
            div_tambah.style.display = "none";
            
            var nama_sertifikat   = document.getElementById("nama_sertifikat_"+no).value;
            var nomor_sertifikat  = document.getElementById("nomor_sertifikat_"+no).value;
            var tahun_sertifikat  = document.getElementById("tahun_sertifikat_"+no).value;
            var id_keterampilan   = document.getElementById("id_"+no).value;
                       
            document.getElementById("id_keterampilan").value  = id_keterampilan;
            document.getElementById("nama_sertifikat").value  = nama_sertifikat;
            document.getElementById("nomor_sertifikat").value = nomor_sertifikat;
            document.getElementById("tahun_sertifikat").value = tahun_sertifikat;
        }
        
        function hapus(id)
        {            
            var result = confirm("Apakah yakin ingin menghapus data ini ?");
            if (result)
            {
                $.ajax({
                    url: 'hapus_keterampilan',
                    type: "POST",
                     dataType: "json",
                    data: {id_hapus_keterampilan:id},
                    success: function(data) {
                        alert(data.message);
                        window.location.href = 'keterampilan_skill';
                    }            
                });
            }
            
        }
        
        function showhide() 
        {  
            var div = document.getElementById("resume_content_form");
            var div_tambah = document.getElementById("tambah_resume"); 
            if (div.style.display !== "none") {  
                div.style.display = "none";
                div_tambah.style.display = "block";
                document.getElementById("frmEducation").reset();

            }  
            else {  
                div.style.display = "block";
                div_tambah.style.display = "none"; 
            }  
        }  


        function validate(evt) {
            var theEvent = evt || window.event;
          
            // Handle paste
            if (theEvent.type === 'paste')
            {
                key = event.clipboardData.getData('text/plain');
            } else
            {
            // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if( !regex.test(key) )
            {
              theEvent.returnValue = false;
              if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function getArrData(el){
            var arr_data = [];
            $(el).each(function(i,obj){
                var elchild = $(obj).children();                
                var arr_child = [];
                $(elchild).each(function(j,objchl){                                            
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            if(typeof newValue !== undefined && newValue !== false) {  
                                arr_child.push(newValue);
                            }
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();
                            arr_child.push(newValue);                                                      
                        }

                        
                    }                         
                 });                
                arr_data.push(arr_child);
            });
            return arr_data;
        }

        function getArrTableExisting(element){
            var arr_data = [];            
             $(element).each(function(i,obj){                
                console.log(obj);
                var id = $(obj).attr("id").split("-")[1];                          
                var elchild = $(obj).children();                    
                var arr_child = [];
                $(elchild).each(function(j,objchl){                        
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            arr_child.push(newValue);
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();                                
                            arr_child.push(newValue);
                        }                        
                    }                         
                 });

                arr_data.push({id: id, data: arr_child});
            });    
            return arr_data;
        }


        function fnClickAddRow(){
            var eltblbody =$(".contains-body-pendidikan").text();  
            var contain_body_pendidikan = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX").last();
            
            var html = '<tr class="gradeX tambahan tambahan-new" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Nama Sekolah" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="tahun_lulus" class="form-control allowNumberWithDotsOnly" value="0"/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pendidikan==""){
                $('.contains-body-pendidikan').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumber('.gradeX.tambahan');
        }

         function fnClickAddRowSkill(){
            var eltblbody =$(".contains-body-skill").text();  
            var contain_body_skill = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_skill").last();
            
            var html = '<tr class="gradeX_skill tambahan tambahan-new-skill" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Ketrampilan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Sertifikat" class="form-control "/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_skill==""){
                $('.contains-body-skill').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberSkill('.gradeX_skill.tambahan');
        }

         function fnClickAddRowPengalaman(){
            var eltblbody =$(".contains-body-pengalaman").text();  
            var contain_body_pengalaman = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_pengalaman").last();
            
            var html = '<tr class="gradeX_pengalaman tambahan tambahan-new-pengalaman" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Perusahaan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Bagian Kerja" class="form-control "/></td>';
                html = html + '<td><input type="text" placeholder="Lama Kerja" class="form-control "/></td>';  
                html = html + '<td><input type="text" placeholder="Packlaring Kerja" class="form-control "/></td>';                    
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pengalaman==""){
                $('.contains-body-pengalaman').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberPengalaman('.gradeX_pengalaman.tambahan');
        }
    

        function onDelete(elm){              
            if (!confirm('Apakah mau dihapus?')){                
                return;
            }
            $(".btn").attr("disabled",true);
            var el_add_row = $(".new");
            if (!el_add_row){
                $(".btn-submit").hide();
            }
            var el = $(elm).parent().parent();                        
            var arr_id = $(el).attr("id");
            var is_exist = false;
            if (arr_id){
                is_exist = true;
                var arr_id = $(el).attr("id").split("-");
            }
            if (is_exist){
                $.ajax({
                    type: "POST",
                    data: {id:arr_id[1]},
                    url: "delete",
                    success: function(msg){                    
                        var json = JSON.parse(msg);
                        if (json.response=="SUKSES"){
                            $(el).remove();  
                            $(".btn").attr("disabled",false);
                        }
                    }
                }); 
            }else{
                $(el).remove();  
                $(".btn").attr("disabled",false);
                $(".btn-submit").attr("disabled",true);
            }
            reSortNumber(".tambahan");                                        
        }

        


        function reSortNumber(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberSkill(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberPengalaman(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function isExistIds(ids, val){
            var is_exist = false;                
            if (ids.includes(val)) {                    
                is_exist = true;
            }            
            return is_exist;
        }
    </script>