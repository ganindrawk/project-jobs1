     <div class="col-xs-12" id="resume_content_form" style="display:none;">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Tambah Nilai Mata Pelajaran</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="resume-content-form c-hide">
                    <?php
                    $attributes = array('class'=>'form-horizontal','role'=>'form','id'=>'frmEducation', 'novalidate' => 'novalidate');
                    echo form_open_multipart($this->uri->segment(1).'/nilai_sekolah',$attributes); 
                    ?>
                    <div class="c-hide" id="eduFrm0">
                        <div class="form-group ">
                            <label class="col-sm-3 control-label custom-control-label required" for="nama_sekolah" aria-required="true">Mata Pelajaran</label>
                            <div class="col-sm-7">
                                <input type="hidden" name="id_nilai" value="" id="id_nilai">
                                <div id="mp"> </div>
                                <select name="mata_pelajaran" id="mata_pelajaran"  class="dform-control" required>
                                    <?php
                                    foreach($matapelajaran as $data_mp)
                                    {
                                        echo "<option value='".$data_mp['id']."'> ".$data_mp['nama_mata_pelajaran']."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                                                                    
                        <div class="form-group ">
                            <label class="col-sm-3 control-label custom-control-label required" for="nilai" aria-required="true">Nilai</label>
                            <div class="col-sm-7">
                                <input class="form-control" id="nilai" name="nilai" type="number" value="" maxlength="100" autocomplete="off" step="0.01" required>
                             </div>
                        </div>
                            
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <input class="btn btn-primary" id="btn_save" type="submit" name="submit" value="Save">&nbsp;&nbsp;
                                <input class="btn btn-default" id="btn_cancel" type="button" name="btn_cancel" value="Cancel" onclick="return showhide();">
                           </div>
                        </div>
                        <?php
                            echo form_close();
                        ?>
                    </div>
                </div>
            </div>
        </div>
     </div>
    
    <div class="col-xs-12">  
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Nilai Mata Pelajaran</h3>
              <a class='pull-right btn btn-primary btn-sm' href='javascript:;'  id="add_nilai" onclick="return showhide();">Tambahkan Data</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style='width:20px'>No</th>
                            <th>Mata Pelajaran</th>
                            <th>Nilai</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $no     = 1;
                    $nilai  = 0;
                    foreach ($record as $row)
                    {
                        echo    '   <div class="col-xs-12" style="display: none;">
                                        <input type="text" value="'.$row['id'].'" name="id_'.$no.'"  id="id_'.$no.'">
                                        <input type="text" value="'.$row['id_mata_pelajaran'].'" name="id_mata_pelajaran_'.$no.'"  id="id_mata_pelajaran_'.$no.'">
                                        <input type="text" value="'.$row['nilai'].'" name="nilai_'.$no.'"  id="nilai_'.$no.'">
                                        <input type="text" value="'.$row['nama_mata_pelajaran'].'" name="nama_mata_pelajaran_'.$no.'"  id="nama_mata_pelajaran_'.$no.'">
                                    </div>
                                ';
                        echo "<tr><td>$no</td>
                                  <td>$row[nama_mata_pelajaran]</td>
                                  <td>$row[nilai]</td>
                                  <td>
                                      <center>
                                          <a class='btn btn-success btn-xs' title='Edit Data' href='#' id='edu_edit_link0'  onClick='edit($no)'>
                                              <span class='glyphicon glyphicon-edit'></span>
                                          </a>
                                      </center>
                                  </td>
                              </tr>";
                            $no++;
                            $nilai = $nilai + $row['nilai'];
                    }
                    $rata=number_format($nilai/($no-1),2);
                    ?>
                    </tbody>
                    <thead>
                        <tr>
                            <th style='width:20px; text-align:center;' colspan='2'>Nilai Rata Rata</th>
                            <th><?=$rata?></th>
                            <th></th>
                        </tr>
                    </thead>
              </table>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
    
        function showhide() 
        {      
            var div = document.getElementById("resume_content_form");
            var div_tambah = document.getElementById("add_nilai");
            var element =  document.getElementById('mata_pelajaran_baru');
            if (typeof(element) != 'undefined' && element != null)
            {
                document.getElementById("mp").innerHTML = ' ';
                document.getElementById("mata_pelajaran_baru").style.display = "block";            
                document.getElementById("mata_pelajaran_baru").setAttribute("name","mata_pelajaran");
                document.getElementById("mata_pelajaran_baru").setAttribute("id","mata_pelajaran");
            }
                        
            if (div.style.display !== "none")
            {  
                div.style.display = "none";
                div_tambah.style.display = "block";
                document.getElementById("frmEducation").reset();
            }  
            else
            {  
                div.style.display = "block";
                div_tambah.style.display = "none"; 
            }  
        }
        
        function edit(no)
        {            
            var div                     = document.getElementById("resume_content_form");
            var div_tambah              = document.getElementById("add_nilai");
            
            div.style.display           = "block";
            div_tambah.style.display    = "none";
            
            var nama_mata_pelajaran     = ((document.getElementById("nama_mata_pelajaran_"+no)||{}).value)||"";
            var nilai                   = ((document.getElementById("nilai_"+no)||{}).value)||"";
            var id_mata_pelajaran       = ((document.getElementById("id_mata_pelajaran_"+no)||{}).value)||"";
            var id_nilai                = ((document.getElementById("id_"+no)||{}).value)||"";
            
            document.getElementById("mata_pelajaran").style.display = "none";
            document.getElementById("mata_pelajaran").setAttribute("name","mata_pelajaran_baru");
            document.getElementById("mata_pelajaran").setAttribute("id","mata_pelajaran_baru");
            
            document.getElementById("mp").innerHTML = '<input class="form-control" id="mata_pelajaran" name="mata_pelajaran" type="hidden" value="'+id_mata_pelajaran+'" ><input class="form-control" id="nama_mata_pelajaran" name="nama_mata_pelajaran" type="text" value="'+nama_mata_pelajaran+'" readonly>';
            document.getElementById("id_nilai").value           = id_nilai;
            document.getElementById("nilai").value              = nilai;
            //document.getElementById("mata_pelajaran").value     = nama_mata_pelajaran;            
        }
        
    </script>