<style>
  th {

  } 
  td.profile {
    background-color: grey;
    color: white;
  }
</style>


    <div class='col-md-12'>
        <div class='box box-info'>
            <div class='box-header with-border'>
              <h3 class='box-title'>Data Profile</h3>
            </div>
            <div class='box-body'>
                <?php
                if(isset($message))
                {
                    echo $message;
                }
                
                $attributes = array('class'=>'form-horizontal','role'=>'form','id'=>'formProfil');
                echo form_open_multipart($this->uri->segment(1).'/profil_pelamar',$attributes); 
                if ($rows['foto']==''){ $foto = 'users.gif'; }else{ $foto = $rows['foto']; }
                ?>
                <div class='col-md-12'>
                    <table class='table table-condensed table-bordered'>
                        <tbody>
                            <input type='hidden' name='id' value='<?=$rows['username']?>' >
                            <input type='hidden' name='ids' value='<?=$rows['id_session']?>' >
                            <tr>
                                <th width='120px' scope='row'>Nama Lengkap</th>
                                <td>
                                    <input type='text' class='form-control' name='nama_lengkap' value='<?=$rows['nama_lengkap']?>' readonly>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>NIK</th>
                                <td>
                                    <input type='text' class='form-control' name='nik' value='<?=$data_users['nik']?>' onkeypress='validate(event);'  maxlength = "16" required>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>Tempat Lahir</th>
                                <td>
                                    <input type='text' class='form-control' name='tlahir' value='<?=$data_users['tempat_lahir']?>'  maxlength = "255" required>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>Tanggal Lahir</th>
                                <td>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' class="form-control" name='tgllahir' value='<?=$data_users['tanggal_lahir']?>' readonly  required/>
                                        <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </td>
                            </tr>      
                            <tr>
                                <th scope='row'>Jenis Kelamin</th>
                                <td>
                                    <select name="jenis_kelamin" class='form-control'  required >
                                        <option value="pria" <?=($data_users['jenis_kelamin']=='pria') ? 'selected' : '' ?> >Pria</option>
                                        <option value="wanita" <?=($data_users['jenis_kelamin']=='wanita') ? 'selected' : '' ?> >Wanita</option>
                                    </select>
                                </td>
                            </tr>     
                         
                            <tr>
                                <th scope='row'>Agama</th>
                                <td>
                                    <input type='text' class='form-control' name='agama' value='<?=$data_users['agama']?>'>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>Alamat</th>
                                <td>
                                    <textarea class='form-control' name='alamat' required><?=$data_users['alamat']?></textarea>
                                </td>
                            </tr>
                                                                
                            <tr>
                                <th scope='row'>Email</th>
                                <td>
                                    <input type='email' class='form-control' name='email' value='<?=$rows['email']?>' readonly>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>No HP/WA</th>
                                <td>
                                    <input type='number' class='form-control' name='hp' value='<?=$data_users['hp']?>' onkeypress='validate(event);' required>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>Pendidikan Akhir</th>
                                <td>
                                    <select name="pendidikan_akhir" class='form-control'  required>
                                        <option value="sd" <?=($data_users['pendidikan_akhir']=='sd') ? 'selected' : '' ?> >SD</option>
                                        <option value="smp" <?=($data_users['pendidikan_akhir']=='smp') ? 'selected' : '' ?> >SMP</option>
                                        <option value="sma" <?=($data_users['pendidikan_akhir']=='sma') ? 'selected' : '' ?> >SMA</option>
                                         <option value="smk" <?=($data_users['pendidikan_akhir']=='smk') ? 'selected' : '' ?> >SMK</option>
                                        <option value="d3" <?=($data_users['pendidikan_akhir']=='d3') ? 'selected' : '' ?> >D3</option>
                                        <option value="s1" <?=($data_users['pendidikan_akhir']=='s1') ? 'selected' : '' ?> >S1</option>
                                    </select>
                                </td>
                            </tr>
                            
                            <tr>
                                <th scope='row'>Kompetensi</th>
                                <td>
                                    <input type='text' class='form-control' name='kompetensi' value='<?=$data_users['kompetensi']?>' maxlength = "255" required>
                                </td>
                            </tr>
                            
                            <tr>
                                <th scope='row'>Tahun Lulus</th>
                                <td>
                                    <input type='number' class='form-control' name='tahun_lulus' value='<?=$data_users['tahun_lulus']?>' onkeypress='validate(event);' maxlength = "4" required>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>Tinggi Badan</th>
                                <td>
                                    <input type='number' class='form-control' name='tinggi_badan' value='<?=$data_users['tinggi_badan']?>' onkeypress='validate(event);' maxlength = "4" required>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>Berat Badan</th>
                                <td>
                                    <input type='number' class='form-control' name='berat_badan' value='<?=$data_users['berat_badan']?>' onkeypress='validate(event);' maxlength = "4" required>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>Buta Warna</th>
                                <td>
                                    <select name="buta_warna" class='form-control'  required >
                                        <option value="ya" <?=($data_users['buta_warna']=='ya') ? 'selected' : '' ?> >Ya</option>
                                        <option value="tidak" <?=($data_users['buta_warna']=='tidak') ? 'selected' : '' ?> >Tidak</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>Berkacamata</th>
                                <td>
                                    <select name="berkacamata" class='form-control'  required >
                                        <option value="ya" <?=($data_users['berkacamata']=='ya') ? 'selected' : '' ?> >Ya</option>
                                        <option value="tidak" <?=($data_users['berkacamata']=='tidak') ? 'selected' : '' ?> >Tidak</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>Status Bekerja</th>
                                <td>
                                    <select name="status_kerja" class='form-control'  required >
                                        <option value="sudah_bekerja" <?=($data_users['status_kerja']=='sudah_bekerja') ? 'selected' : '' ?> >Sudah Bekerja</option>
                                        <option value="belum_bekerja" <?=($data_users['status_kerja']=='belum_bekerja') ? 'selected' : '' ?> >Belum Bekerja</option>
                                        <option value="kuliah" <?=($data_users['status_kerja']=='ya') ? 'kuliah' : '' ?> >Kuliah</option>
                                    </select>
                                       
                                </td>
                            </tr>
                            
                            <tr>
                                <th scope='row'>Foto (max 1MB)</th>
                                <td>
                                    <input type='file' class='form-control' name='f'><hr style='margin:5px'>
                                    <?php
                                    if ($data_users['foto'] != '')
                                    {
                                    ?>
                                        <i style='color:red'>Foto Saat ini : </i>
                                        
                                        <a target='_BLANK' href='<?=base_url()?>asset/foto_user/<?=$data_users['foto']?>'>
                                            <?=$data_users['foto']?>
                                            </a>
                                    <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <?php
                               
                                    if($data_users['foto'] != ''){
                                        echo "<img style='width:100px' src='".base_url()."asset/foto_user/$data_users[foto]' alt='data_users[foto]' />";
                                    }
                                    else{
                                        echo '<div style="color:red">Foto belum ada, Silahkan ditambahkan</div> ';
                                    }

                                    ?>
                              </td>
                            </tr>
                            <!--
                            <tr>
                                <th scope='row'>Riwayat Pendidikan</th>
                                <td>
                                    <button type='button' class='btn btn-primary btn-tambah' onclick='fnClickAddRow()''>Tambah Data</button>
                                    <br><br>
                                </td>
                            </tr>                                                            
                            <tr>message
                                <td class='profile'>No</td>
                                <td class='profile'>Nama Sekolah</td>
                                <td class='profile'>Tahun Lulus</td>                        
                                <td class='profile'>Aksi</td>   
                            </tr>        
                            <tbody class='contains-body-pendidikan'></tbody>      
                            <tr>
                                <th scope='row'>Ketrampilan Skill</th>
                                <td>
                                    <button type='button' class='btn btn-primary btn-tambah' onclick='fnClickAddRowSkill()''>Tambah Data</button>
                                    <br><br>
                                </td>
                            </tr>                                                       
                            <tr>
                                <td class='profile'>No</td>
                                <td class='profile'>Ketrampilan/Skill</td>
                                <td class='profile'>Sertifikat</td>                        
                                <td class='profile'>Aksi</td>   
                            </tr>      
                            <tbody class='contains-body-skill'></tbody>      
                            <tr>
                                <th scope='row'>Pengalaman Kerja</th>
                                <td>
                                    <button type='button' class='btn btn-primary btn-tambah' onclick='fnClickAddRowPengalaman();'>Tambah Data</button>
                                    <br><br>
                                </td>
                            </tr>
                            <tr>
                                <td class='profile'>No</td>
                                <td class='profile'>Perusahaan</td>
                                <td class='profile'>Bagian Kerja</td>                        
                                <td class='profile'>Lama Kerja</td>
                                <td class='profile'>Packlaring Kerja</td>
                                <td class='profile'>Aksi</td>     
                            </tr>      
                            <tbody class='contains-body-pengalaman'></tbody>   
                            <tr>
                                <th scope='row'>Nilai Sekolah</th>
                                <td>
                                                            
                                    <tr>
                                        <td class='profile'>No</td>
                                        <td class='profile'>Mata Pelajaran</td>
                                        <td class='profile'>Nilai</td>         
                                    </tr>
                                    <tr>
                                        <td>1</td><td>Bahasa Indonesia</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                         <td>2</td><td>Bahasa Inggris</td>
                                         <td></td>
                                    </tr>
                                    <tr>
                                         <td>3</td><td>Matematika</td>
                                         <td></td>
                                    </tr>
                                    <tr>
                                         <td>4</td><td>Produktif</td>
                                         <td></td>
                                    </tr> 
                                    <tr>
                                        <td colspan=2>Rata-rata</td>
                                        <td></td>
                                    </tr>    
                                </td>
                            </tr>
                            
                            <tr>
                                <th scope='row'>Upload Lampiran Dokumen Pelamar</th>
                                <td>                 
                                    <tr>
                                        <td class='profile'>No</td>
                                        <td class='profile'>Mata Pelajaran</td>
                                        <td class='profile'>Status</td> 
                                        <td class='profile'>Aksi</td>           
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Ijazah Akhir</td>
                                        <td></td>
                                        <td>Lihat | Update</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>SKHUN</td>
                                        <td></td>
                                        <td>Lihat | Update</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Sertifikat Kompetensi</td>
                                        <td></td>
                                        <td>Lihat | Update</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Sertifikat PSG/Magang</td>
                                        <td></td>
                                        <td>Lihat | Update</td>
                                    </tr>                 
                                </td>
                            </tr>
                            -->
                                                            
                        </tbody>
                    </table>
                </div>
                  
                <div class='box-footer'>
                    <button type='submit' name='submit' class='btn btn-info'>Update</button>
                    <a href='".base_url().$this->uri->segment(1)."/profil_pelamar'>
                        <button type='button' class='btn btn-default pull-right'>Cancel</button>
                    </a>
                </div>
                
                <?php
                echo form_close();
                ?>
            </div>
        </div>
    </div>
    
    
    <script type="text/javascript">
        //filter only numeric
        function validate(evt) {
            var theEvent = evt || window.event;
          
            // Handle paste
            if (theEvent.type === 'paste')
            {
                key = event.clipboardData.getData('text/plain');
            } else
            {
            // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if( !regex.test(key) )
            {
              theEvent.returnValue = false;
              if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function getArrData(el){
            var arr_data = [];
            $(el).each(function(i,obj){
                var elchild = $(obj).children();                
                var arr_child = [];
                $(elchild).each(function(j,objchl){                                            
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            if(typeof newValue !== undefined && newValue !== false) {  
                                arr_child.push(newValue);
                            }
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();
                            arr_child.push(newValue);                                                      
                        }

                        
                    }                         
                 });                
                arr_data.push(arr_child);
            });
            return arr_data;
        }

        function getArrTableExisting(element){
            var arr_data = [];            
             $(element).each(function(i,obj){                
                console.log(obj);
                var id = $(obj).attr("id").split("-")[1];                          
                var elchild = $(obj).children();                    
                var arr_child = [];
                $(elchild).each(function(j,objchl){                        
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            arr_child.push(newValue);
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();                                
                            arr_child.push(newValue);
                        }                        
                    }                         
                 });

                arr_data.push({id: id, data: arr_child});
            });    
            return arr_data;
        }


        function fnClickAddRow(){
            var eltblbody =$(".contains-body-pendidikan").text();  
            var contain_body_pendidikan = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX").last();
            
            var html = '<tr class="gradeX tambahan tambahan-new" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Nama Sekolah" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="tahun_lulus" class="form-control allowNumberWithDotsOnly" value="0"/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pendidikan==""){
                $('.contains-body-pendidikan').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumber('.gradeX.tambahan');
        }

         function fnClickAddRowSkill(){
            var eltblbody =$(".contains-body-skill").text();  
            var contain_body_skill = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_skill").last();
            
            var html = '<tr class="gradeX_skill tambahan tambahan-new-skill" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Ketrampilan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Sertifikat" class="form-control "/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_skill==""){
                $('.contains-body-skill').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberSkill('.gradeX_skill.tambahan');
        }

         function fnClickAddRowPengalaman(){
            var eltblbody =$(".contains-body-pengalaman").text();  
            var contain_body_pengalaman = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_pengalaman").last();
            
            var html = '<tr class="gradeX_pengalaman tambahan tambahan-new-pengalaman" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Perusahaan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Bagian Kerja" class="form-control "/></td>';
                html = html + '<td><input type="text" placeholder="Lama Kerja" class="form-control "/></td>';  
                html = html + '<td><input type="text" placeholder="Packlaring Kerja" class="form-control "/></td>';                    
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pengalaman==""){
                $('.contains-body-pengalaman').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberPengalaman('.gradeX_pengalaman.tambahan');
        }
    

        function onDelete(elm){              
            if (!confirm('Apakah mau dihapus?')){                
                return;
            }
            $(".btn").attr("disabled",true);
            var el_add_row = $(".new");
            if (!el_add_row){
                $(".btn-submit").hide();
            }
            var el = $(elm).parent().parent();                        
            var arr_id = $(el).attr("id");
            var is_exist = false;
            if (arr_id){
                is_exist = true;
                var arr_id = $(el).attr("id").split("-");
            }
            if (is_exist){
                $.ajax({
                    type: "POST",
                    data: {id:arr_id[1]},
                    url: "delete",
                    success: function(msg){                    
                        var json = JSON.parse(msg);
                        if (json.response=="SUKSES"){
                            $(el).remove();  
                            $(".btn").attr("disabled",false);
                        }
                    }
                }); 
            }else{
                $(el).remove();  
                $(".btn").attr("disabled",false);
                $(".btn-submit").attr("disabled",true);
            }
            reSortNumber(".tambahan");                                        
        }

        


        function reSortNumber(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberSkill(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberPengalaman(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function isExistIds(ids, val){
            var is_exist = false;                
            if (ids.includes(val)) {                    
                is_exist = true;
            }            
            return is_exist;
        }
    </script>