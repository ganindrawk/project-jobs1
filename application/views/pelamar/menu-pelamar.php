        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
            <?php $usr = $this->model_app->view_where('users', array('username'=> $this->session->username))->row_array();
                  if (trim($usr['foto'])==''){ $foto = 'blank.png'; }else{ $foto = $usr['foto']; } ?>
            <img src="<?php echo base_url(); ?>/asset/foto_user/<?php echo $foto; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <?php echo "<p>".$this->session->nama_lengkap."</p>"; ?>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header" style='color:#fff; text-transform:uppercase; border-bottom:2px solid #00c0ef'>MENU <span class='uppercase'><?php echo $this->session->level; ?></span></li>
            <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/index"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                          
            
            <li  class="treeview">
                <a href="#"><i class="fa fa-edit"></i> <span>Profile</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class='fa fa-circle-o'></i> Lihat Profil Saya</a></li>
                    <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/profil_pelamar"><i class='fa fa-circle-o'></i> Profil Pelamar</a></li>
                    <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/riwayat_pendidikan"><i class='fa fa-circle-o'></i> Riwayat Pendidikan</a></li>
                    <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/keterampilan_skill"><i class='fa fa-circle-o'></i> Keterampilan/Skill</a></li>
                    <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/pengalaman_kerja"><i class='fa fa-circle-o'></i> Pengalaman Kerja</a></li>
                    <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/nilai_sekolah"><i class='fa fa-circle-o'></i> Nilai Sekolah</a></li>
                    <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/lampiran_pelamar"><i class='fa fa-circle-o'></i> Lampiran Dokumen Pelamar</a></li>
                </ul>
            </li>

            <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/list_panggilan_tes/"><i class="fa fa-edit"></i> <span>Panggilan Tes Kerja</span></a></li>
            
            <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/list_pengirimancv/<?php echo $this->session->username; ?>"><i class="fa fa-edit"></i> <span>Pengiriman CV</span></a></li>
            
            <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/ganti_password"><i class="fa fa-edit"></i> <span>Ganti Password</span></a></li>
            <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/logout"><i class="fa fa-power-off"></i> <span>Logout</span></a></li>
          </ul>
        </section>