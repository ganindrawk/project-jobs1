            <div class="col-xs-12">  
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">List Panggilan Tes Kerja</h3>
                  <!-- <a class='pull-right btn btn-primary btn-sm' href='<?php echo base_url().$this->uri->segment(1); ?>/tambah_cuti'>Tambahkan Data</a> -->
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style='width:20px'>No</th>
                        <th>Nama Perusahaan</th>
                        <th>Alamat</th>
                        <th>Tanggal Tes</th>
                        <th>Waktu Tes</th>
                        <th>Status</th>
                        <th style='width:75px'>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                  <?php 
                    $no = 1;
       
                    foreach ($panggilan as $row){

                    $tgl_test = tgl_indo($row['tanggal_tes']);
             
                    echo "<tr><td>$no</td>
                              <td>$row[nama_perusahaan]</td>
                              <td>$row[alamat]</td>
                              <td>$tgl_test</td>
                              <td>Jam $row[jam_mulai] WIB - s/d Jam $row[jam_selesai] WIB</td>";
                              if($row[status_kirim]==1){
                                  echo "<td>Panggilan Tes(Belum Tes)</td>";
                              }
                              else if($row[status_kirim]==2){
                                  echo "<td>Siap di tes</td>";
                              }
                              else if($row[status_kirim]==3){
                                  echo "<td>Tidak Diterima</td>";
                              }
                              else if($row[status_kirim]==4){
                                  echo "<td>Diterima</td>";
                              }
                              else {
                                   echo "<td>Menunggu</td>";
                              }
                              echo "
                              <td><center>
                               <a class='btn btn-success btn-xs' title='Cetak' href='".base_url().$this->uri->segment(1)."/'><span class='glyphicon glyphicon-print'></span></a>
                               
                                <a class='btn btn-success btn-xs' title='Konfirmasi' href='".base_url().$this->uri->segment(1)."/'><span class='glyphicon glyphicon-edit'></span></a>
                              </center></td>
                          </tr>";
                      $no++;
                    }
                  ?>
                  </tbody>
                </table>
              </div>