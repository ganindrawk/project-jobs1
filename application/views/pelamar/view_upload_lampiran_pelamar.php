<style>
  th {

  } 
  td.profile {
    background-color: grey;
    color: white;
  }
</style>


    <div class='col-md-12'>
        <div class='box box-info'>
            <div class='box-header with-border'>
              <h3 class='box-title'>Upload Lampiran Dokumen Pelamar</h3>
            </div>
            <div class='box-body'>
                <?php
                if(isset($message))
                {
                    echo $message;
                }          
                ?>
                
                <div class="panel panel-default resume-panel" id="panel" style="min-height: 338px;">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-xs-7">
                                                <div class="row">
                                                    
                                                </div>
                                            </div>
                                            <!--
                                            <div class="col-xs-5 text-right" id="tambah_resume">
                                                <a class="" id="add_education" href="javascript:;" title="Add Education" onclick="return showhide();">
                                                        <i class="fa fa-plus-circle"></i>&nbsp;&nbsp;&nbsp;
                                                        <span class="add-detail" id="lbl_edu_add">Tambah</span>
                                                </a>
                                                
                                            </div>
                                            -->
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="resume-content-form c-hide" id="resume_content_form" style="display:none;">
                                    <?php
                                    $attributes = array('class'=>'form-horizontal','role'=>'form','id'=>'frmEducation');
                                    echo form_open_multipart($this->uri->segment(1).'/lampiran_pelamar',$attributes); 
                                    ?>
                                         <div class="c-hide" id="eduFrm0">
                                            
                                            
                                            <div class="form-group ">
                                                <label class="col-sm-3 control-label custom-control-label required" for="nama_sekolah" aria-required="true">Nama Dokumen</label>
                                                <div class="col-sm-7">
                                                    <input type="hidden" name="id_lampiran_pelamar" value="" id="id_lampiran_pelamar">
                                                    <div id="mp"> </div>
                                                    <select name="dokumen" id="dokumen"  class="dform-control" required>
                                                        <?php
                                                        foreach($dokumen as $data)
                                                        {
                                                            echo "<option value='".$data['id']."'> ".$data['dokumen']."</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label custom-control-label required " aria-required="true">Status</label>
                                                <div class="col-sm-7">
                                                    <div class="row">
                                                        <div class="col-sm-4 col-xs-6">
                                                            <select name="status" id="status"  class="dform-control" required onchange="showUpload(this.value);">
                                                                <option value="ada">Ada</option>
                                                                <option value="tidak_ada">Tidak Ada</option>
                                                            </select>
                                                        </div>                                                        
                                                        <div class="col-xs-12 error-container"></div>
                                                    </div>
                                                </div>
                                            </div>
                                                                                        
                                            <div class="form-group" id="upload_lampiran">
                                                <label class="col-sm-3 control-label custom-control-label">File (max 1MB) </label>
                                                <div class="col-sm-9">
                                                    <input type='file' class='form-control' name='f'><hr style='margin:5px'>
                                                    gif|jpg|png|JPG|JPEG
                                                    <div id="foto_saat_ini">
                                                        
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                            
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <input class="btn btn-primary" id="btn_save" type="submit" name="submit" value="Save">&nbsp;&nbsp;
                                                <input class="btn btn-default" id="btn_cancel" type="button" name="btn_cancel" value="Cancel" onclick="return showhide();">
                                           </div>
                                        </div>
                                        </form>
                                    <?php
                                        echo form_close();
                                    ?>
                                </div>
                                
                                <div class="col-xs-12">  
                                    <div class="box">
                                        <div class="box-header">
                                          <h3 class="box-title">Data Lampiran Pelamar</h3>
                                          <a class='pull-right btn btn-primary btn-sm' href='#'  id='tambah' onclick='return showhide();'>Tambahkan Data</a>
                                        </div><!-- /.box-header -->
                                        <div class="box-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th style='width:20px'>No</th>
                                                        <th>Mata Pelajaran</th>
                                                        <th>Status</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                $no     = 1;
                                                foreach ($record as $row)
                                                {
                                                    echo    '   <div class="col-xs-12" style="display: none;">
                                                                    <input type="text" value="'.$row['id'].'" name="id_'.$no.'"  id="id_'.$no.'">
                                                                    <input type="text" value="'.$row['id_master_dokumen'].'" name="id_master_dokumen_'.$no.'"  id="id_master_dokumen_'.$no.'">
                                                                    <input type="text" value="'.$row['status'].'" name="status_'.$no.'"  id="status_'.$no.'">
                                                                    <input type="text" value="'.$row['dokumen'].'" name="dokumen_'.$no.'"  id="dokumen_'.$no.'">
                                                                    <input type="text" value="'.$row['file_lampiran'].'" name="file_lampiran_'.$no.'"  id="file_lampiran_'.$no.'">
                                                                </div>
                                                            ';
                                                    if(empty($row['status']))
                                                    {
                                                        $status = '-';
                                                    }
                                                    else
                                                    {
                                                        $status = $row['status'];
                                                    }
                                                    echo '<tr><td>'.$no.'</td>
                                                              <td>'.$row['dokumen'].'</td>
                                                              <td>'.$status.'</td>
                                                              <td>
                                                                  <center>';
                                                    if($row['status']=='ada')
                                                    {
                                                        echo '        <a class="btn btn-success btn-xs" title="Edit Data" href="#" onClick="lihat(\''.$row['file_lampiran'].'\')">
                                                                          Lihat</span>
                                                                      </a> 
                                                                      | ';
                                                    }
                                                    
                                                    echo '            <a class="btn btn-success btn-xs" title="Edit Data" href="#" id="edu_edit_link0"  onClick="edit('.$no.')">
                                                                          Update</span>
                                                                      </a>
                                                                  </center>
                                                              </td>
                                                          </tr>';
                                                        $no++;
                                                }
                                                ?>
                                                </tbody>
                                                
                                          </table>
                                        </div>
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <script type="text/javascript">
        //filter only numeric
        $( document ).ready(function() {
                //document.getElementById("resume_content_form").style.display = "none";
               
        });
        
        
        function lihat(file)
        {
            var url = '<?=base_url()?>/asset/lampiran_pelamar/<?=$this->session->username?>/'+file;
            window.open(url, '_blank').focus();
        }
        
        function showUpload(val)
        {
            var div = document.getElementById("upload_lampiran");
            if (val=='ada')
            {
                div.style.display = "block";
            }
            else
            {
                div.style.display = "none";
            }
        }
        
        function edit(no)
        {            
            var div = document.getElementById("resume_content_form");
            var div_tambah = document.getElementById("tambah"); 
            div.style.display = "block";
            div_tambah.style.display = "none";
            
            var file_lampiran       = document.getElementById("file_lampiran_"+no).value;
            var dokumen             = document.getElementById("dokumen_"+no).value;
            var status              = document.getElementById("status_"+no).value;
            var id_master_dokumen   = document.getElementById("id_master_dokumen_"+no).value;
            var id                  = document.getElementById("id_"+no).value;
            
            document.getElementById("foto_saat_ini").style.display = "block";           
            document.getElementById("status").value                 = status;
            document.getElementById("id_lampiran_pelamar").value    = id;
            if (status=='ada')
            {
                document.getElementById("foto_saat_ini").innerHTML      = "<i style='color:red'>Foto Saat ini : </i><a target='_BLANK' href='<?=base_url()?>asset/lampiran_pelamar/<?=$this->session->username?>/"+file_lampiran+"'>Lihat</a>";
                document.getElementById("upload_lampiran").style.display = "block";
            }
            else
            {
                document.getElementById("foto_saat_ini").innerHTML      = "";
                document.getElementById("upload_lampiran").style.display = "none";
            }
            
            
            document.getElementById("dokumen").style.display = "none";
            document.getElementById("dokumen").setAttribute("name","dokumen_baru");
            document.getElementById("dokumen").setAttribute("id","dokumen_baru");
            
            document.getElementById("mp").innerHTML = '<input class="form-control" id="dokumen" name="dokumen" type="hidden" value="'+id_master_dokumen+'" ><input class="form-control" id="nama_dokumen" name="nama_dokumen" type="text" value="'+dokumen+'" readonly>';
            
            
        }
        
        function hapus(id)
        {            
            var result = confirm("Apakah yakin ingin menghapus data ini ?");
            if (result)
            {
                $.ajax({
                    url: 'hapus_pendidikan',
                    type: "POST",
                     dataType: "json",
                    data: {id_hapus_pendidikan:id},
                    success: function(data) {
                        alert(data.message);
                        window.location.href = 'riwayat_pendidikan';
                    }            
                });
            }
            
        }
        
        function showhide() 
        {  
            var div = document.getElementById("resume_content_form");
            var div_tambah = document.getElementById("tambah");
                        
            var element =  document.getElementById('dokumen_baru');
            if (typeof(element) != 'undefined' && element != null)
            {
                document.getElementById("mp").innerHTML                 = ' ';
                document.getElementById("dokumen_baru").style.display   = "block";            
                document.getElementById("dokumen_baru").setAttribute("name","dokumen");
                document.getElementById("dokumen_baru").setAttribute("id","dokumen");
            }
            
            document.getElementById("foto_saat_ini").style.display = "none";
            document.getElementById("upload_lampiran").style.display = "block";
            
            if (div.style.display !== "none") {  
                div.style.display = "none";
                div_tambah.style.display = "block";
                document.getElementById("frmEducation").reset();

            }  
            else {  
                div.style.display = "block";
                div_tambah.style.display = "none"; 
            }  
        }  


        function validate(evt) {
            var theEvent = evt || window.event;
          
            // Handle paste
            if (theEvent.type === 'paste')
            {
                key = event.clipboardData.getData('text/plain');
            } else
            {
            // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if( !regex.test(key) )
            {
              theEvent.returnValue = false;
              if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function getArrData(el){
            var arr_data = [];
            $(el).each(function(i,obj){
                var elchild = $(obj).children();                
                var arr_child = [];
                $(elchild).each(function(j,objchl){                                            
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            if(typeof newValue !== undefined && newValue !== false) {  
                                arr_child.push(newValue);
                            }
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();
                            arr_child.push(newValue);                                                      
                        }

                        
                    }                         
                 });                
                arr_data.push(arr_child);
            });
            return arr_data;
        }

        function getArrTableExisting(element){
            var arr_data = [];            
             $(element).each(function(i,obj){                
                console.log(obj);
                var id = $(obj).attr("id").split("-")[1];                          
                var elchild = $(obj).children();                    
                var arr_child = [];
                $(elchild).each(function(j,objchl){                        
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            arr_child.push(newValue);
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();                                
                            arr_child.push(newValue);
                        }                        
                    }                         
                 });

                arr_data.push({id: id, data: arr_child});
            });    
            return arr_data;
        }


        function fnClickAddRow(){
            var eltblbody =$(".contains-body-pendidikan").text();  
            var contain_body_pendidikan = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX").last();
            
            var html = '<tr class="gradeX tambahan tambahan-new" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Nama Sekolah" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="tahun_lulus" class="form-control allowNumberWithDotsOnly" value="0"/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pendidikan==""){
                $('.contains-body-pendidikan').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumber('.gradeX.tambahan');
        }

         function fnClickAddRowSkill(){
            var eltblbody =$(".contains-body-skill").text();  
            var contain_body_skill = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_skill").last();
            
            var html = '<tr class="gradeX_skill tambahan tambahan-new-skill" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Ketrampilan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Sertifikat" class="form-control "/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_skill==""){
                $('.contains-body-skill').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberSkill('.gradeX_skill.tambahan');
        }

         function fnClickAddRowPengalaman(){
            var eltblbody =$(".contains-body-pengalaman").text();  
            var contain_body_pengalaman = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_pengalaman").last();
            
            var html = '<tr class="gradeX_pengalaman tambahan tambahan-new-pengalaman" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Perusahaan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Bagian Kerja" class="form-control "/></td>';
                html = html + '<td><input type="text" placeholder="Lama Kerja" class="form-control "/></td>';  
                html = html + '<td><input type="text" placeholder="Packlaring Kerja" class="form-control "/></td>';                    
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pengalaman==""){
                $('.contains-body-pengalaman').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberPengalaman('.gradeX_pengalaman.tambahan');
        }
    

        function onDelete(elm){              
            if (!confirm('Apakah mau dihapus?')){                
                return;
            }
            $(".btn").attr("disabled",true);
            var el_add_row = $(".new");
            if (!el_add_row){
                $(".btn-submit").hide();
            }
            var el = $(elm).parent().parent();                        
            var arr_id = $(el).attr("id");
            var is_exist = false;
            if (arr_id){
                is_exist = true;
                var arr_id = $(el).attr("id").split("-");
            }
            if (is_exist){
                $.ajax({
                    type: "POST",
                    data: {id:arr_id[1]},
                    url: "delete",
                    success: function(msg){                    
                        var json = JSON.parse(msg);
                        if (json.response=="SUKSES"){
                            $(el).remove();  
                            $(".btn").attr("disabled",false);
                        }
                    }
                }); 
            }else{
                $(el).remove();  
                $(".btn").attr("disabled",false);
                $(".btn-submit").attr("disabled",true);
            }
            reSortNumber(".tambahan");                                        
        }

        


        function reSortNumber(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberSkill(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberPengalaman(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function isExistIds(ids, val){
            var is_exist = false;                
            if (ids.includes(val)) {                    
                is_exist = true;
            }            
            return is_exist;
        }
    </script>