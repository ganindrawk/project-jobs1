<style>
  th {

  } 
  td.profile {
    background-color: grey;
    color: white;
  }
</style>

<?php 
    echo "<div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'>Data Profile</h3>
                </div>
              <div class='box-body'>";
              $attributes = array('class'=>'form-horizontal','role'=>'form');
              echo form_open_multipart($this->uri->segment(1).'/edit_manajemenuser',$attributes); 
              if ($rows['foto']==''){ $foto = 'users.gif'; }else{ $foto = $rows['foto']; }
          echo "<div class='col-md-12'>
                  <table class='table table-condensed table-bordered'>
                  <tbody>
                    <input type='hidden' name='id' value='$rows[username]'>
                    <input type='hidden' name='ids' value='$rows[id_session]'>
                    <tr><th width='120px' scope='row'>Username</th>   <td><input type='text' class='form-control' name='a' value='$rows[username]' readonly='on'></td></tr>
                    <tr><th scope='row'>Password</th>                 <td><input type='password' class='form-control' name='b' onkeyup=\"nospaces(this)\"></td></tr>
                    <tr><th scope='row'>Nama Lengkap</th>             <td><input type='text' class='form-control' name='c' value='$rows[nama_lengkap]'></td></tr>
                    <tr><th scope='row'>NIK</th>             <td><input type='text' class='form-control' name='nik' value='$rows[nik]'></td></tr>
                    <tr><th scope='row'>Tempat Lahir</th>             <td><input type='text' class='form-control' name='tlahir' value='$rows[tlahir]'></td></tr>
                    <tr><th scope='row'>Tanggal Lahir</th>             <td><input type='text' class='form-control' name='tgllahir' value='$rows[tgllahir]'></td></tr>";

                    echo "<tr><th scope='row'>Jenis Kelamin</th><td>"; 

                    echo "<input type='radio' name='jenis_kelamin' checked> Pria &nbsp; ";
                    echo "<input type='radio' name='jenis_kelamin'>&nbsp; Wanita"; 
                    echo "</td></tr>";

                   echo "
                    <tr><th scope='row'>Agama</th><td><input type='text' class='form-control' name='agama' value='$rows[agama]'></td></tr>
                     <tr><th scope='row'>Alamat</th><td><textarea class='form-control' name='alamat' value='$rows[textarea]'></textarea></td></tr>

                    <tr><th scope='row'>Email</th><td><input type='email' class='form-control' name='d' value='$rows[email]'></td></tr>
                    <tr><th scope='row'>No HP/WA</th>                  <td><input type='number' class='form-control' name='e' value='$rows[no_telp]'></td></tr>
                    <tr><th scope='row'>Pendidikan Akhir</th><td><input type='text' class='form-control' name='pendidikan_akhir' value='$rows[pendidikan_akhir]'></td></tr>
                    <tr><th scope='row'>Tahun Lulus</th><td><input type='text' class='form-control' name='pendidikan_akhir' value='$rows[pendidikan_akhir]'></td></tr>
                    <tr><th scope='row'>Tinggi Badan</th><td><input type='text' class='form-control' name='tinggi_badan' value='$rows[tinggi_badan]'></td></tr>
                    <tr><th scope='row'>Berat Badan</th><td><input type='text' class='form-control' name='berat_badan' value='$rows[berat_badan]'></td></tr>";
                     
                    
                    echo "<tr><th scope='row'>Buta Warna</th><td>"; 

                    echo "<input type='radio' name='buta_warna' checked> Ya &nbsp; ";
                    echo "<input type='radio' name='buta_warna'>&nbsp; Tidak"; 
                    echo "</td></tr>";

                    echo "<tr><th scope='row'>Berkacamata</th><td>"; 

                    echo "<input type='radio' name='berkacamata' checked> Ya &nbsp; ";
                    echo "<input type='radio' name='berkacamata'>&nbsp; Tidak"; 
                    echo "</td></tr>";

                    echo "<tr><th scope='row'>Status Bekerja</th><td>"; 
                    
                    echo "<input type='radio' name='status_kerja' checked> Sudah Bekerja &nbsp;"; 
                    echo "<input type='radio' name='status_kerja'> Belum Bekerja &nbsp;"; 
                    echo "<input type='radio' name='status_kerja'> Kuliah&nbsp;";

                    echo "</td></tr>";
                    // Riwayat Pendidikan
                    echo "<tr><th scope='row'>Riwayat Pendidikan</th>";
                    echo "<td>";
                    echo "<button type='button' class='btn btn-primary btn-tambah' onclick='fnClickAddRow()''>Tambah Data</button><br><br>";
                    echo "                                
                    <tr>
                      <td class='profile'>No</td>
                      <td class='profile'>Nama Sekolah</td>
                      <td class='profile'>Tahun Lulus</td>                        
                      <td class='profile'>Aksi</td>   
                    </tr>

                    <tbody class='contains-body-pendidikan'>
                    </tbody>                                          
                   ";
                  echo "</td>";
                  echo "</tr>";
                  // end Riwayat Pendidikan
                  // Ketrampilan Skill
                  echo "<tr><th scope='row'>Ketrampilan Skill</th>";
                  echo "<td>";
                  echo "<button type='button' class='btn btn-primary btn-tambah' onclick='fnClickAddRowSkill()''>Tambah Data</button><br><br>";
                  echo "                                
                    <tr>
                      <td class='profile'>No</td>
                      <td class='profile'>Ketrampilan/Skill</td>
                      <td class='profile'>Sertifikat</td>                        
                      <td class='profile'>Aksi</td>   
                    </tr>

                    <tbody class='contains-body-skill'>
                    </tbody>                                          
                   ";
                  echo "</td>";
                  echo "</tr>";
                  // end Ketrampilan Skill

                  // Pengalaman Kerja
                  echo "<tr><th scope='row'>Pengalaman Kerja</th>";
                  echo "<td>";
                  echo "<button type='button' class='btn btn-primary btn-tambah' onclick='fnClickAddRowPengalaman()''>Tambah Data</button><br><br>";
                  echo "                                
                    <tr>
                      <td class='profile'>No</td>
                      <td class='profile'>Perusahaan</td>
                      <td class='profile'>Bagian Kerja</td>                        
                      <td class='profile'>Lama Kerja</td>
                      <td class='profile'>Packlaring Kerja</td>
                      <td class='profile'>Aksi</td>     
                    </tr>

                    <tbody class='contains-body-pengalaman'>
                    </tbody>                                          
                   ";
                  echo "</td>";
                  echo "</tr>";
                  // end Pengalaman Kerja

                  // Nilai Sekolah
                  echo "<tr><th scope='row'>Nilai Sekolah</th>";
                  echo "<td>";
                  echo "                                
                    <tr>
                      <td class='profile'>No</td>
                      <td class='profile'>Mata Pelajaran</td>
                      <td class='profile'>Nilai</td>         
                    </tr>
                    <tr>
                        <td>1</td><td>Bahasa Indonesia</td><td></td>
                    </tr>
                    <tr>
                         <td>2</td><td>Bahasa Inggris</td><td></td>
                    </tr>
                    <tr>
                         <td>3</td><td>Matematika</td><td></td>
                    </tr>
                    <tr>
                         <td>4</td><td>Produktif</td><td></td>
                    </tr> 
                    <tr>
                        <td colspan=2>Rata-rata</td><td></td>
                    </tr>                                                        
                   ";
                  echo "</td>";
                  echo "</tr>";
                  // end Nilai Sekolah

                  // Upload Lampiran Dokumen Pelamar
                  echo "<tr><th scope='row'>Upload Lampiran Dokumen Pelamar</th>";
                  echo "<td>";
                  echo "                                
                    <tr>
                      <td class='profile'>No</td>
                      <td class='profile'>Mata Pelajaran</td>
                      <td class='profile'>Status</td> 
                      <td class='profile'>Aksi</td>           
                    </tr>
                    <tr>
                        <td>1</td><td>Ijazah Akhir</td><td></td><td>Lihat | Update</td>
                    </tr>
                    <tr>
                         <td>2</td><td>SKHUN</td><td></td><td>Lihat | Update</td>
                    </tr>
                    <tr>
                         <td>3</td><td>Sertifikat Kompetensi</td><td></td><td>Lihat | Update</td>
                    </tr>
                    <tr>
                         <td>4</td><td>Sertifikat PSG/Magang</td><td></td><td>Lihat | Update</td>
                    </tr>                                                             
                   ";
                  echo "</td>";
                  echo "</tr>";
                  // end Upload Lampiran Dokumen Pelamar



                    echo "
                    <tr><th scope='row'>Ganti Foto</th>                     <td><input type='file' class='form-control' name='f'><hr style='margin:5px'>";

                    if ($rows['foto'] != ''){ echo "<i style='color:red'>Foto Saat ini : </i><a target='_BLANK' href='".base_url()."asset/foto_user/$rows[foto]'>$rows[foto]</a>"; } echo "</td></tr></td></tr>";
                    if ($this->session->level == 'admin'){
                      echo "<tr><th scope='row'>Blokir</th><td>"; if ($rows['blokir']=='Y'){ echo "<input type='radio' name='h' value='Y' checked> Ya &nbsp; <input type='radio' name='h' value='N'> Tidak"; }else{ echo "<input type='radio' name='h' value='Y'> Ya &nbsp; <input type='radio' name='h' value='N' checked> Tidak"; } echo "</td></tr>
                            <tr><th scope='row'>Tambah Akses</th>                    <td><div class='checkbox-scroll'>";
                            foreach ($record as $row){
                              echo "<span style='display:block'><input name='modul[]' type='checkbox' value='$row[id_modul]' /> $row[nama_modul]</span> ";
                                                                               }
                      echo "</div></td></tr>
                      <tr><th scope='row'>Hak Akses</th><td><div class='checkbox-scroll'>";
                            foreach ($akses as $ro){
                              echo "<span style='display:block'><a class='text-danger' href='".base_url()."administrator/delete_akses/$ro[id_umod]/".$this->uri->segment(3)."'><span class='glyphicon glyphicon-remove'></span></a> $ro[nama_modul]</span> ";
                                                                               }
                      echo "</div></td></tr>";
                    }
                    echo '<tr><td></tr>';
                  echo "</tbody>
                  </table></div>
              
              <div class='box-footer'>
                    <button type='submit' name='submit' class='btn btn-info'>Update</button>
                    <a href='".base_url().$this->uri->segment(1)."/manajemenuser'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
                    
                  </div>
            </div></div></div>";
            echo form_close();
            ?>
<script type="text/javascript">


        function getArrData(el){
            var arr_data = [];
            $(el).each(function(i,obj){
                var elchild = $(obj).children();                
                var arr_child = [];
                $(elchild).each(function(j,objchl){                                            
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            if(typeof newValue !== undefined && newValue !== false) {  
                                arr_child.push(newValue);
                            }
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();
                            arr_child.push(newValue);                                                      
                        }

                        
                    }                         
                 });                
                arr_data.push(arr_child);
            });
            return arr_data;
        }

        function getArrTableExisting(element){
            var arr_data = [];            
             $(element).each(function(i,obj){                
                console.log(obj);
                var id = $(obj).attr("id").split("-")[1];                          
                var elchild = $(obj).children();                    
                var arr_child = [];
                $(elchild).each(function(j,objchl){                        
                    var newValue = "";
                    if ($(objchl).children()){
                        var inputearea = $(objchl).find("textarea");
                        if ($(inputearea).hasClass("form-control")){
                            newValue  = $(inputearea).val();
                            arr_child.push(newValue);
                        }

                        var inpute2 = $(objchl).find("select");
                        if ($(inpute2).hasClass("form-control")){
                            newValue  = $(inpute2).val();                                
                            arr_child.push(newValue);
                        }

                        var inputel = $(objchl).find("input");
                        if ($(inputel).hasClass("form-control")){
                            newValue  = $(inputel).val();                                
                            arr_child.push(newValue);
                        }                        
                    }                         
                 });

                arr_data.push({id: id, data: arr_child});
            });    
            return arr_data;
        }


        function fnClickAddRow(){
            var eltblbody =$(".contains-body-pendidikan").text();  
            var contain_body_pendidikan = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX").last();
            
            var html = '<tr class="gradeX tambahan tambahan-new" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Nama Sekolah" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="tahun_lulus" class="form-control allowNumberWithDotsOnly" value="0"/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pendidikan==""){
                $('.contains-body-pendidikan').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumber('.gradeX.tambahan');
        }

         function fnClickAddRowSkill(){
            var eltblbody =$(".contains-body-skill").text();  
            var contain_body_skill = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_skill").last();
            
            var html = '<tr class="gradeX_skill tambahan tambahan-new-skill" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Ketrampilan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Sertifikat" class="form-control "/></td>';                
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_skill==""){
                $('.contains-body-skill').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberSkill('.gradeX_skill.tambahan');
        }

         function fnClickAddRowPengalaman(){
            var eltblbody =$(".contains-body-pengalaman").text();  
            var contain_body_pengalaman = eltblbody.replace(/\s+/g, '');            
            var el = $(".gradeX_pengalaman").last();
            
            var html = '<tr class="gradeX_pengalaman tambahan tambahan-new-pengalaman" id-kegiatan="" type-kegiatan="">';
                html = html + '<td class="no"></td>';
                html = html + '<td><input type="text" placeholder="Perusahaan Skill" class="form-control" value=""/></td>';
                html = html + '<td><input type="text" placeholder="Bagian Kerja" class="form-control "/></td>';
                html = html + '<td><input type="text" placeholder="Lama Kerja" class="form-control "/></td>';  
                html = html + '<td><input type="text" placeholder="Packlaring Kerja" class="form-control "/></td>';                    
                html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
                html = html + '</tr>';
                        
            if (contain_body_pengalaman==""){
                $('.contains-body-pengalaman').html(html);
            }else{
                $(html).insertAfter(el);
            }

            //$("input,select").css("border-color","red");

             reSortNumberPengalaman('.gradeX_pengalaman.tambahan');
        }
    

        function onDelete(elm){              
            if (!confirm('Apakah mau dihapus?')){                
                return;
            }
            $(".btn").attr("disabled",true);
            var el_add_row = $(".new");
            if (!el_add_row){
                $(".btn-submit").hide();
            }
            var el = $(elm).parent().parent();                        
            var arr_id = $(el).attr("id");
            var is_exist = false;
            if (arr_id){
                is_exist = true;
                var arr_id = $(el).attr("id").split("-");
            }
            if (is_exist){
                $.ajax({
                    type: "POST",
                    data: {id:arr_id[1]},
                    url: "delete",
                    success: function(msg){                    
                        var json = JSON.parse(msg);
                        if (json.response=="SUKSES"){
                            $(el).remove();  
                            $(".btn").attr("disabled",false);
                        }
                    }
                }); 
            }else{
                $(el).remove();  
                $(".btn").attr("disabled",false);
                $(".btn-submit").attr("disabled",true);
            }
            reSortNumber(".tambahan");                                        
        }

        


        function reSortNumber(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberSkill(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function reSortNumberPengalaman(element){
            var number = 1;
            $(element).each(function(i,obj){
                var elchild = $(obj).children();
                $(elchild).each(function(j,objchl){
                    if ($(objchl).hasClass("no")){ //no pada <tr> 
                        $(objchl).text(number++);
                    }
                });
            });
        }

        function isExistIds(ids, val){
            var is_exist = false;                
            if (ids.includes(val)) {                    
                is_exist = true;
            }            
            return is_exist;
        }
</script>