<style>
  th {

  } 
  td.profile {
    background-color: grey;
    color: white;
  }
</style>


    <div class='col-md-12'>
        <div class='box box-info'>
            <div class='box-header with-border'>
                <h3 class='box-title'>Ganti Password</h3>
            </div>
            <div class='box-body'>
                <?php
                echo $this->session->flashdata('message');					
            
                $attributes = array('class'=>'form-horizontal','role'=>'form');
                echo form_open_multipart($this->uri->segment(1).'/ganti_password',$attributes);
                ?>
                 
                    <div class='col-md-12'>
                        <table class='table table-condensed table-bordered'>
                            <tbody>
                                <input type='hidden' name='id' value='<?=$rows['username']?>'>
                                <input type='hidden' name='ids' value='<?=$rows['id_session']?>'>
                                <tr>
                                    <th width='120px' scope='row'>Username</th>
                                    <td>
                                        <input type='text' class='form-control' name='a' value='<?=$rows['username']?>' readonly='on'>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope='row'>Password Baru</th>
                                    <td>
                                        <input type='password' class='form-control' name='b' onkeyup="nospaces(this);" onChange="onChange();" required>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope='row'>Konfirmasi</th>
                                    <td>
                                        <input type='password' class='form-control' name='c' onkeyup="nospaces(this);" onChange="onChange();" required>
                                        <span id='message'></span>
                                    </td>
                                </tr>                                    
                                <tr>
                                    <th scope='row'>CAPTCHA</th>
                                    <td> <?=$image?> </td>
                                </tr>
                                <tr>
                                    <th scope='row'>Masukkan Kode Captcha</th>
                                    <td><input type='text' class='form-control' name='security_code' placeholder='Security Code' required></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                      
                    <div class='box-footer'>
                        <button type='submit' name='submit' class='btn btn-info'>Update</button>
                        <a href='<?=base_url()?>login/home_pelamar'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
                    </div>
                <?php
                echo form_close();
                ?>
            </div>
        </div>
    </div>
    
<script type="text/javascript">
    function onChange() {
        const password = document.querySelector('input[name=b]');
        const confirm = document.querySelector('input[name=c]');
        if (confirm.value == password.value) {
            confirm.setCustomValidity('');
        }
        else
        {
            confirm.setCustomValidity('Passwords do not match');
        }
    }
    
    /*  
    function getArrData()
    {
        var arr_data = [];
        $(el).each(function(i,obj){
            var elchild = $(obj).children();                
            var arr_child = [];
            $(elchild).each(function(j,objchl){                                            
                var newValue = "";
                if ($(objchl).children()){
                    var inputearea = $(objchl).find("textarea");
                    if ($(inputearea).hasClass("form-control")){
                        newValue  = $(inputearea).val();
                        arr_child.push(newValue);
                    }

                    var inpute2 = $(objchl).find("select");
                    if ($(inpute2).hasClass("form-control")){
                        newValue  = $(inpute2).val();                                
                        if(typeof newValue !== undefined && newValue !== false) {  
                            arr_child.push(newValue);
                        }
                    }

                    var inputel = $(objchl).find("input");
                    if ($(inputel).hasClass("form-control")){
                        newValue  = $(inputel).val();
                        arr_child.push(newValue);                                                      
                    }

                    
                }                         
             });                
            arr_data.push(arr_child);
        });
        return arr_data;
    }

    function getArrTableExisting(element){
        var arr_data = [];            
         $(element).each(function(i,obj){                
            console.log(obj);
            var id = $(obj).attr("id").split("-")[1];                          
            var elchild = $(obj).children();                    
            var arr_child = [];
            $(elchild).each(function(j,objchl){                        
                var newValue = "";
                if ($(objchl).children()){
                    var inputearea = $(objchl).find("textarea");
                    if ($(inputearea).hasClass("form-control")){
                        newValue  = $(inputearea).val();
                        arr_child.push(newValue);
                    }

                    var inpute2 = $(objchl).find("select");
                    if ($(inpute2).hasClass("form-control")){
                        newValue  = $(inpute2).val();                                
                        arr_child.push(newValue);
                    }

                    var inputel = $(objchl).find("input");
                    if ($(inputel).hasClass("form-control")){
                        newValue  = $(inputel).val();                                
                        arr_child.push(newValue);
                    }                        
                }                         
             });

            arr_data.push({id: id, data: arr_child});
        });    
        return arr_data;
    }

    
    function fnClickAddRow(){
        var eltblbody =$(".contains-body-pendidikan").text();  
        var contain_body_pendidikan = eltblbody.replace(/\s+/g, '');            
        var el = $(".gradeX").last();
        
        var html = '<tr class="gradeX tambahan tambahan-new" id-kegiatan="" type-kegiatan="">';
            html = html + '<td class="no"></td>';
            html = html + '<td><input type="text" placeholder="Nama Sekolah" class="form-control" value=""/></td>';
            html = html + '<td><input type="text" placeholder="tahun_lulus" class="form-control allowNumberWithDotsOnly" value="0"/></td>';                
            html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
            html = html + '</tr>';
                    
        if (contain_body_pendidikan==""){
            $('.contains-body-pendidikan').html(html);
        }else{
            $(html).insertAfter(el);
        }

        //$("input,select").css("border-color","red");

         reSortNumber('.gradeX.tambahan');
    }

     function fnClickAddRowSkill(){
        var eltblbody =$(".contains-body-skill").text();  
        var contain_body_skill = eltblbody.replace(/\s+/g, '');            
        var el = $(".gradeX_skill").last();
        
        var html = '<tr class="gradeX_skill tambahan tambahan-new-skill" id-kegiatan="" type-kegiatan="">';
            html = html + '<td class="no"></td>';
            html = html + '<td><input type="text" placeholder="Ketrampilan Skill" class="form-control" value=""/></td>';
            html = html + '<td><input type="text" placeholder="Sertifikat" class="form-control "/></td>';                
            html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
            html = html + '</tr>';
                    
        if (contain_body_skill==""){
            $('.contains-body-skill').html(html);
        }else{
            $(html).insertAfter(el);
        }

        //$("input,select").css("border-color","red");

         reSortNumberSkill('.gradeX_skill.tambahan');
    }

    function fnClickAddRowPengalaman(){
       var eltblbody =$(".contains-body-pengalaman").text();  
       var contain_body_pengalaman = eltblbody.replace(/\s+/g, '');            
       var el = $(".gradeX_pengalaman").last();
       
       var html = '<tr class="gradeX_pengalaman tambahan tambahan-new-pengalaman" id-kegiatan="" type-kegiatan="">';
           html = html + '<td class="no"></td>';
           html = html + '<td><input type="text" placeholder="Perusahaan Skill" class="form-control" value=""/></td>';
           html = html + '<td><input type="text" placeholder="Bagian Kerja" class="form-control "/></td>';
           html = html + '<td><input type="text" placeholder="Lama Kerja" class="form-control "/></td>';  
           html = html + '<td><input type="text" placeholder="Packlaring Kerja" class="form-control "/></td>';                    
           html = html + '<td class="action"><button href="javascript:void(0)" class="btn btn-primary btn-delete" onclick="onDelete(this)">Delete</button></td>';
           html = html + '</tr>';
                   
       if (contain_body_pengalaman==""){
           $('.contains-body-pengalaman').html(html);
       }else{
           $(html).insertAfter(el);
       }

       //$("input,select").css("border-color","red");

        reSortNumberPengalaman('.gradeX_pengalaman.tambahan');
   }
   

    function onDelete(elm){              
        if (!confirm('Apakah mau dihapus?')){                
            return;
        }
        $(".btn").attr("disabled",true);
        var el_add_row = $(".new");
        if (!el_add_row){
            $(".btn-submit").hide();
        }
        var el = $(elm).parent().parent();                        
        var arr_id = $(el).attr("id");
        var is_exist = false;
        if (arr_id){
            is_exist = true;
            var arr_id = $(el).attr("id").split("-");
        }
        if (is_exist){
            $.ajax({
                type: "POST",
                data: {id:arr_id[1]},
                url: "delete",
                success: function(msg){                    
                    var json = JSON.parse(msg);
                    if (json.response=="SUKSES"){
                        $(el).remove();  
                        $(".btn").attr("disabled",false);
                    }
                }
            }); 
        }else{
            $(el).remove();  
            $(".btn").attr("disabled",false);
            $(".btn-submit").attr("disabled",true);
        }
        reSortNumber(".tambahan");                                        
    }

    


    function reSortNumber(element){
        var number = 1;
        $(element).each(function(i,obj){
            var elchild = $(obj).children();
            $(elchild).each(function(j,objchl){
                if ($(objchl).hasClass("no")){ //no pada <tr> 
                    $(objchl).text(number++);
                }
            });
        });
    }

    function reSortNumberSkill(element){
        var number = 1;
        $(element).each(function(i,obj){
            var elchild = $(obj).children();
            $(elchild).each(function(j,objchl){
                if ($(objchl).hasClass("no")){ //no pada <tr> 
                    $(objchl).text(number++);
                }
            });
        });
    }

    function reSortNumberPengalaman(element){
        var number = 1;
        $(element).each(function(i,obj){
            var elchild = $(obj).children();
            $(elchild).each(function(j,objchl){
                if ($(objchl).hasClass("no")){ //no pada <tr> 
                    $(objchl).text(number++);
                }
            });
        });
    }

    function isExistIds(ids, val){
        var is_exist = false;                
        if (ids.includes(val)) {                    
            is_exist = true;
        }            
        return is_exist;
    }
    */
</script>